#if MAX_HOR_ORDER>-1 
OUTPUT_FUNCS_INT1(0)%F0=>CALC_OUTPUT_FUNC_INT1_0_F0 
OUTPUT_FUNCS_INT1(0)%F2=>CALC_OUTPUT_FUNC_INT1_0_F2 
OUTPUT_FUNCS_INT1(0)%F4=>CALC_OUTPUT_FUNC_INT1_0_F4 

OUTPUT_FUNCS_INT1(0)%D1F0=>CALC_OUTPUT_FUNC_INT1_0_D1F0 
OUTPUT_FUNCS_INT1(0)%D1F2=>CALC_OUTPUT_FUNC_INT1_0_D1F2 
OUTPUT_FUNCS_INT1(0)%D1F4=>CALC_OUTPUT_FUNC_INT1_0_D1F4 

OUTPUT_FUNCS_INT1(0)%D2F0=>CALC_OUTPUT_FUNC_INT1_0_D2F0 
OUTPUT_FUNCS_INT1(0)%D2F2=>CALC_OUTPUT_FUNC_INT1_0_D2F2 
OUTPUT_FUNCS_INT1(0)%D2F4=>CALC_OUTPUT_FUNC_INT1_0_D2F4 

IF (Np==0) RETURN 
#endif 

#if MAX_HOR_ORDER>0 
OUTPUT_FUNCS_INT1(1)%F0=>CALC_OUTPUT_FUNC_INT1_1_F0 
OUTPUT_FUNCS_INT1(1)%F2=>CALC_OUTPUT_FUNC_INT1_1_F2 
OUTPUT_FUNCS_INT1(1)%F4=>CALC_OUTPUT_FUNC_INT1_1_F4 

OUTPUT_FUNCS_INT1(1)%D1F0=>CALC_OUTPUT_FUNC_INT1_1_D1F0 
OUTPUT_FUNCS_INT1(1)%D1F2=>CALC_OUTPUT_FUNC_INT1_1_D1F2 
OUTPUT_FUNCS_INT1(1)%D1F4=>CALC_OUTPUT_FUNC_INT1_1_D1F4 

OUTPUT_FUNCS_INT1(1)%D2F0=>CALC_OUTPUT_FUNC_INT1_1_D2F0 
OUTPUT_FUNCS_INT1(1)%D2F2=>CALC_OUTPUT_FUNC_INT1_1_D2F2 
OUTPUT_FUNCS_INT1(1)%D2F4=>CALC_OUTPUT_FUNC_INT1_1_D2F4 

IF (Np==1) RETURN 
#endif 

#if MAX_HOR_ORDER>1 
OUTPUT_FUNCS_INT1(2)%F0=>CALC_OUTPUT_FUNC_INT1_2_F0 
OUTPUT_FUNCS_INT1(2)%F2=>CALC_OUTPUT_FUNC_INT1_2_F2 
OUTPUT_FUNCS_INT1(2)%F4=>CALC_OUTPUT_FUNC_INT1_2_F4 

OUTPUT_FUNCS_INT1(2)%D1F0=>CALC_OUTPUT_FUNC_INT1_2_D1F0 
OUTPUT_FUNCS_INT1(2)%D1F2=>CALC_OUTPUT_FUNC_INT1_2_D1F2 
OUTPUT_FUNCS_INT1(2)%D1F4=>CALC_OUTPUT_FUNC_INT1_2_D1F4 

OUTPUT_FUNCS_INT1(2)%D2F0=>CALC_OUTPUT_FUNC_INT1_2_D2F0 
OUTPUT_FUNCS_INT1(2)%D2F2=>CALC_OUTPUT_FUNC_INT1_2_D2F2 
OUTPUT_FUNCS_INT1(2)%D2F4=>CALC_OUTPUT_FUNC_INT1_2_D2F4 

IF (Np==2) RETURN 
#endif 

#if MAX_HOR_ORDER>2 
OUTPUT_FUNCS_INT1(3)%F0=>CALC_OUTPUT_FUNC_INT1_3_F0 
OUTPUT_FUNCS_INT1(3)%F2=>CALC_OUTPUT_FUNC_INT1_3_F2 
OUTPUT_FUNCS_INT1(3)%F4=>CALC_OUTPUT_FUNC_INT1_3_F4 

OUTPUT_FUNCS_INT1(3)%D1F0=>CALC_OUTPUT_FUNC_INT1_3_D1F0 
OUTPUT_FUNCS_INT1(3)%D1F2=>CALC_OUTPUT_FUNC_INT1_3_D1F2 
OUTPUT_FUNCS_INT1(3)%D1F4=>CALC_OUTPUT_FUNC_INT1_3_D1F4 

OUTPUT_FUNCS_INT1(3)%D2F0=>CALC_OUTPUT_FUNC_INT1_3_D2F0 
OUTPUT_FUNCS_INT1(3)%D2F2=>CALC_OUTPUT_FUNC_INT1_3_D2F2 
OUTPUT_FUNCS_INT1(3)%D2F4=>CALC_OUTPUT_FUNC_INT1_3_D2F4 

IF (Np==3) RETURN 
#endif 

#if MAX_HOR_ORDER>3 
OUTPUT_FUNCS_INT1(4)%F0=>CALC_OUTPUT_FUNC_INT1_4_F0 
OUTPUT_FUNCS_INT1(4)%F2=>CALC_OUTPUT_FUNC_INT1_4_F2 
OUTPUT_FUNCS_INT1(4)%F4=>CALC_OUTPUT_FUNC_INT1_4_F4 

OUTPUT_FUNCS_INT1(4)%D1F0=>CALC_OUTPUT_FUNC_INT1_4_D1F0 
OUTPUT_FUNCS_INT1(4)%D1F2=>CALC_OUTPUT_FUNC_INT1_4_D1F2 
OUTPUT_FUNCS_INT1(4)%D1F4=>CALC_OUTPUT_FUNC_INT1_4_D1F4 

OUTPUT_FUNCS_INT1(4)%D2F0=>CALC_OUTPUT_FUNC_INT1_4_D2F0 
OUTPUT_FUNCS_INT1(4)%D2F2=>CALC_OUTPUT_FUNC_INT1_4_D2F2 
OUTPUT_FUNCS_INT1(4)%D2F4=>CALC_OUTPUT_FUNC_INT1_4_D2F4 

IF (Np==4) RETURN 
#endif 

#if MAX_HOR_ORDER>4 
OUTPUT_FUNCS_INT1(5)%F0=>CALC_OUTPUT_FUNC_INT1_5_F0 
OUTPUT_FUNCS_INT1(5)%F2=>CALC_OUTPUT_FUNC_INT1_5_F2 
OUTPUT_FUNCS_INT1(5)%F4=>CALC_OUTPUT_FUNC_INT1_5_F4 

OUTPUT_FUNCS_INT1(5)%D1F0=>CALC_OUTPUT_FUNC_INT1_5_D1F0 
OUTPUT_FUNCS_INT1(5)%D1F2=>CALC_OUTPUT_FUNC_INT1_5_D1F2 
OUTPUT_FUNCS_INT1(5)%D1F4=>CALC_OUTPUT_FUNC_INT1_5_D1F4 

OUTPUT_FUNCS_INT1(5)%D2F0=>CALC_OUTPUT_FUNC_INT1_5_D2F0 
OUTPUT_FUNCS_INT1(5)%D2F2=>CALC_OUTPUT_FUNC_INT1_5_D2F2 
OUTPUT_FUNCS_INT1(5)%D2F4=>CALC_OUTPUT_FUNC_INT1_5_D2F4 

IF (Np==5) RETURN 
#endif 

#if MAX_HOR_ORDER>5 
OUTPUT_FUNCS_INT1(6)%F0=>CALC_OUTPUT_FUNC_INT1_6_F0 
OUTPUT_FUNCS_INT1(6)%F2=>CALC_OUTPUT_FUNC_INT1_6_F2 
OUTPUT_FUNCS_INT1(6)%F4=>CALC_OUTPUT_FUNC_INT1_6_F4 

OUTPUT_FUNCS_INT1(6)%D1F0=>CALC_OUTPUT_FUNC_INT1_6_D1F0 
OUTPUT_FUNCS_INT1(6)%D1F2=>CALC_OUTPUT_FUNC_INT1_6_D1F2 
OUTPUT_FUNCS_INT1(6)%D1F4=>CALC_OUTPUT_FUNC_INT1_6_D1F4 

OUTPUT_FUNCS_INT1(6)%D2F0=>CALC_OUTPUT_FUNC_INT1_6_D2F0 
OUTPUT_FUNCS_INT1(6)%D2F2=>CALC_OUTPUT_FUNC_INT1_6_D2F2 
OUTPUT_FUNCS_INT1(6)%D2F4=>CALC_OUTPUT_FUNC_INT1_6_D2F4 

IF (Np==6) RETURN 
#endif 

#if MAX_HOR_ORDER>6 
OUTPUT_FUNCS_INT1(7)%F0=>CALC_OUTPUT_FUNC_INT1_7_F0 
OUTPUT_FUNCS_INT1(7)%F2=>CALC_OUTPUT_FUNC_INT1_7_F2 
OUTPUT_FUNCS_INT1(7)%F4=>CALC_OUTPUT_FUNC_INT1_7_F4 

OUTPUT_FUNCS_INT1(7)%D1F0=>CALC_OUTPUT_FUNC_INT1_7_D1F0 
OUTPUT_FUNCS_INT1(7)%D1F2=>CALC_OUTPUT_FUNC_INT1_7_D1F2 
OUTPUT_FUNCS_INT1(7)%D1F4=>CALC_OUTPUT_FUNC_INT1_7_D1F4 

OUTPUT_FUNCS_INT1(7)%D2F0=>CALC_OUTPUT_FUNC_INT1_7_D2F0 
OUTPUT_FUNCS_INT1(7)%D2F2=>CALC_OUTPUT_FUNC_INT1_7_D2F2 
OUTPUT_FUNCS_INT1(7)%D2F4=>CALC_OUTPUT_FUNC_INT1_7_D2F4 

IF (Np==7) RETURN 
#endif 

#if MAX_HOR_ORDER>7 
OUTPUT_FUNCS_INT1(8)%F0=>CALC_OUTPUT_FUNC_INT1_8_F0 
OUTPUT_FUNCS_INT1(8)%F2=>CALC_OUTPUT_FUNC_INT1_8_F2 
OUTPUT_FUNCS_INT1(8)%F4=>CALC_OUTPUT_FUNC_INT1_8_F4 

OUTPUT_FUNCS_INT1(8)%D1F0=>CALC_OUTPUT_FUNC_INT1_8_D1F0 
OUTPUT_FUNCS_INT1(8)%D1F2=>CALC_OUTPUT_FUNC_INT1_8_D1F2 
OUTPUT_FUNCS_INT1(8)%D1F4=>CALC_OUTPUT_FUNC_INT1_8_D1F4 

OUTPUT_FUNCS_INT1(8)%D2F0=>CALC_OUTPUT_FUNC_INT1_8_D2F0 
OUTPUT_FUNCS_INT1(8)%D2F2=>CALC_OUTPUT_FUNC_INT1_8_D2F2 
OUTPUT_FUNCS_INT1(8)%D2F4=>CALC_OUTPUT_FUNC_INT1_8_D2F4 

IF (Np==8) RETURN 
#endif 

#if MAX_HOR_ORDER>8 
OUTPUT_FUNCS_INT1(9)%F0=>CALC_OUTPUT_FUNC_INT1_9_F0 
OUTPUT_FUNCS_INT1(9)%F2=>CALC_OUTPUT_FUNC_INT1_9_F2 
OUTPUT_FUNCS_INT1(9)%F4=>CALC_OUTPUT_FUNC_INT1_9_F4 

OUTPUT_FUNCS_INT1(9)%D1F0=>CALC_OUTPUT_FUNC_INT1_9_D1F0 
OUTPUT_FUNCS_INT1(9)%D1F2=>CALC_OUTPUT_FUNC_INT1_9_D1F2 
OUTPUT_FUNCS_INT1(9)%D1F4=>CALC_OUTPUT_FUNC_INT1_9_D1F4 

OUTPUT_FUNCS_INT1(9)%D2F0=>CALC_OUTPUT_FUNC_INT1_9_D2F0 
OUTPUT_FUNCS_INT1(9)%D2F2=>CALC_OUTPUT_FUNC_INT1_9_D2F2 
OUTPUT_FUNCS_INT1(9)%D2F4=>CALC_OUTPUT_FUNC_INT1_9_D2F4 

IF (Np==9) RETURN 
#endif 

#if MAX_HOR_ORDER>9 
OUTPUT_FUNCS_INT1(10)%F0=>CALC_OUTPUT_FUNC_INT1_10_F0 
OUTPUT_FUNCS_INT1(10)%F2=>CALC_OUTPUT_FUNC_INT1_10_F2 
OUTPUT_FUNCS_INT1(10)%F4=>CALC_OUTPUT_FUNC_INT1_10_F4 

OUTPUT_FUNCS_INT1(10)%D1F0=>CALC_OUTPUT_FUNC_INT1_10_D1F0 
OUTPUT_FUNCS_INT1(10)%D1F2=>CALC_OUTPUT_FUNC_INT1_10_D1F2 
OUTPUT_FUNCS_INT1(10)%D1F4=>CALC_OUTPUT_FUNC_INT1_10_D1F4 

OUTPUT_FUNCS_INT1(10)%D2F0=>CALC_OUTPUT_FUNC_INT1_10_D2F0 
OUTPUT_FUNCS_INT1(10)%D2F2=>CALC_OUTPUT_FUNC_INT1_10_D2F2 
OUTPUT_FUNCS_INT1(10)%D2F4=>CALC_OUTPUT_FUNC_INT1_10_D2F4 

IF (Np==10) RETURN 
#endif 

#if MAX_HOR_ORDER>10 
OUTPUT_FUNCS_INT1(11)%F0=>CALC_OUTPUT_FUNC_INT1_11_F0 
OUTPUT_FUNCS_INT1(11)%F2=>CALC_OUTPUT_FUNC_INT1_11_F2 
OUTPUT_FUNCS_INT1(11)%F4=>CALC_OUTPUT_FUNC_INT1_11_F4 

OUTPUT_FUNCS_INT1(11)%D1F0=>CALC_OUTPUT_FUNC_INT1_11_D1F0 
OUTPUT_FUNCS_INT1(11)%D1F2=>CALC_OUTPUT_FUNC_INT1_11_D1F2 
OUTPUT_FUNCS_INT1(11)%D1F4=>CALC_OUTPUT_FUNC_INT1_11_D1F4 

OUTPUT_FUNCS_INT1(11)%D2F0=>CALC_OUTPUT_FUNC_INT1_11_D2F0 
OUTPUT_FUNCS_INT1(11)%D2F2=>CALC_OUTPUT_FUNC_INT1_11_D2F2 
OUTPUT_FUNCS_INT1(11)%D2F4=>CALC_OUTPUT_FUNC_INT1_11_D2F4 

IF (Np==11) RETURN 
#endif 

#if MAX_HOR_ORDER>11 
OUTPUT_FUNCS_INT1(12)%F0=>CALC_OUTPUT_FUNC_INT1_12_F0 
OUTPUT_FUNCS_INT1(12)%F2=>CALC_OUTPUT_FUNC_INT1_12_F2 
OUTPUT_FUNCS_INT1(12)%F4=>CALC_OUTPUT_FUNC_INT1_12_F4 

OUTPUT_FUNCS_INT1(12)%D1F0=>CALC_OUTPUT_FUNC_INT1_12_D1F0 
OUTPUT_FUNCS_INT1(12)%D1F2=>CALC_OUTPUT_FUNC_INT1_12_D1F2 
OUTPUT_FUNCS_INT1(12)%D1F4=>CALC_OUTPUT_FUNC_INT1_12_D1F4 

OUTPUT_FUNCS_INT1(12)%D2F0=>CALC_OUTPUT_FUNC_INT1_12_D2F0 
OUTPUT_FUNCS_INT1(12)%D2F2=>CALC_OUTPUT_FUNC_INT1_12_D2F2 
OUTPUT_FUNCS_INT1(12)%D2F4=>CALC_OUTPUT_FUNC_INT1_12_D2F4 

IF (Np==12) RETURN 
#endif 

#if MAX_HOR_ORDER>12 
OUTPUT_FUNCS_INT1(13)%F0=>CALC_OUTPUT_FUNC_INT1_13_F0 
OUTPUT_FUNCS_INT1(13)%F2=>CALC_OUTPUT_FUNC_INT1_13_F2 
OUTPUT_FUNCS_INT1(13)%F4=>CALC_OUTPUT_FUNC_INT1_13_F4 

OUTPUT_FUNCS_INT1(13)%D1F0=>CALC_OUTPUT_FUNC_INT1_13_D1F0 
OUTPUT_FUNCS_INT1(13)%D1F2=>CALC_OUTPUT_FUNC_INT1_13_D1F2 
OUTPUT_FUNCS_INT1(13)%D1F4=>CALC_OUTPUT_FUNC_INT1_13_D1F4 

OUTPUT_FUNCS_INT1(13)%D2F0=>CALC_OUTPUT_FUNC_INT1_13_D2F0 
OUTPUT_FUNCS_INT1(13)%D2F2=>CALC_OUTPUT_FUNC_INT1_13_D2F2 
OUTPUT_FUNCS_INT1(13)%D2F4=>CALC_OUTPUT_FUNC_INT1_13_D2F4 

IF (Np==13) RETURN 
#endif 

#if MAX_HOR_ORDER>13 
OUTPUT_FUNCS_INT1(14)%F0=>CALC_OUTPUT_FUNC_INT1_14_F0 
OUTPUT_FUNCS_INT1(14)%F2=>CALC_OUTPUT_FUNC_INT1_14_F2 
OUTPUT_FUNCS_INT1(14)%F4=>CALC_OUTPUT_FUNC_INT1_14_F4 

OUTPUT_FUNCS_INT1(14)%D1F0=>CALC_OUTPUT_FUNC_INT1_14_D1F0 
OUTPUT_FUNCS_INT1(14)%D1F2=>CALC_OUTPUT_FUNC_INT1_14_D1F2 
OUTPUT_FUNCS_INT1(14)%D1F4=>CALC_OUTPUT_FUNC_INT1_14_D1F4 

OUTPUT_FUNCS_INT1(14)%D2F0=>CALC_OUTPUT_FUNC_INT1_14_D2F0 
OUTPUT_FUNCS_INT1(14)%D2F2=>CALC_OUTPUT_FUNC_INT1_14_D2F2 
OUTPUT_FUNCS_INT1(14)%D2F4=>CALC_OUTPUT_FUNC_INT1_14_D2F4 

IF (Np==14) RETURN 
#endif 

#if MAX_HOR_ORDER>14 
OUTPUT_FUNCS_INT1(15)%F0=>CALC_OUTPUT_FUNC_INT1_15_F0 
OUTPUT_FUNCS_INT1(15)%F2=>CALC_OUTPUT_FUNC_INT1_15_F2 
OUTPUT_FUNCS_INT1(15)%F4=>CALC_OUTPUT_FUNC_INT1_15_F4 

OUTPUT_FUNCS_INT1(15)%D1F0=>CALC_OUTPUT_FUNC_INT1_15_D1F0 
OUTPUT_FUNCS_INT1(15)%D1F2=>CALC_OUTPUT_FUNC_INT1_15_D1F2 
OUTPUT_FUNCS_INT1(15)%D1F4=>CALC_OUTPUT_FUNC_INT1_15_D1F4 

OUTPUT_FUNCS_INT1(15)%D2F0=>CALC_OUTPUT_FUNC_INT1_15_D2F0 
OUTPUT_FUNCS_INT1(15)%D2F2=>CALC_OUTPUT_FUNC_INT1_15_D2F2 
OUTPUT_FUNCS_INT1(15)%D2F4=>CALC_OUTPUT_FUNC_INT1_15_D2F4 

IF (Np==15) RETURN 
#endif 

