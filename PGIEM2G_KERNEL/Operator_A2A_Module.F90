!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!


MODULE  OPERATOR_A2A_MODULE 
    USE CONST_MODULE
    USE FFTW3
    USE MPI_MODULE
    USE OPERATORS_MODULE 

    USE Timer_Module 
    USE DATA_TYPES_MODULE
    USE POLYNOMIAL_UTILITIES_MODULE
    USE DISTRIBUTED_FFT_MODULE
    USE MEMORY_MANAGER_MODULE
    USE PGIEM2G_LOGGER_MODULE
    !   USE LOCAL_OMP_FFT_MODULE
    IMPLICIT NONE

    PRIVATE

    COMPLEX(REALPARM),PARAMETER::SYMM_SIGN_X(4)=(/C_ONE,-C_ONE,C_ONE,C_ONE/)    
    COMPLEX(REALPARM),PARAMETER::SYMM_SIGN_Y(4)=(/C_ONE,-C_ONE,C_ONE,C_ONE/)    

    COMPLEX(REALPARM),PARAMETER::ASYM_SIGN_X(2)=(/-C_ONE,C_ONE/)    
    COMPLEX(REALPARM),PARAMETER::ASYM_SIGN_Y(2)=(/C_ONE,-C_ONE/)    

    PUBLIC:: OPERATOR_A2A
    PUBLIC:: PREPARE_OPERATOR_A2A
    PUBLIC:: PREPARE_OPERATOR_S2A
    PUBLIC:: DELETE_OPERATOR_A2A
    PUBLIC:: A2A_LOCAL_KERNEL_SIZE
    PUBLIC:: A2A_FFTW_FWD_PREPARED,A2A_FFTW_BWD_PREPARED
    PUBLIC:: CALC_FFT_OF_A2A_KERNEL
    PUBLIC:: PRINT_STATS,DROP_IE_COUNTER
    PUBLIC:: REPACK_TENSOR
CONTAINS


    SUBROUTINE  PREPARE_OPERATOR_A2A(ie_op,anomaly,bkg,P,comm,fftw_threads_ok,cptrs,&
            &ALLOCATE_KERNEL,PREPARE_FFT)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        TYPE (ANOMALY_TYPE),INTENT(INOUT)::anomaly
        TYPE(BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::comm 
        LOGICAL,INTENT(IN)::fftw_threads_ok
        TYPE(OPERATOR_C_PTR),INTENT(IN),OPTIONAL::cptrs
        LOGICAL,INTENT(IN),OPTIONAL::ALLOCATE_KERNEL
        LOGICAL,INTENT(IN),OPTIONAL::PREPARE_FFT
        INTEGER::Nz
        INTEGER(MPI_CTL_KIND)::Np,IERROR
        LOGICAL::alloc_k,pr_fft
        IF (PRESENT(ALLOCATE_KERNEL)) THEN
            alloc_k=ALLOCATE_KERNEL
        ELSE
            alloc_k=.TRUE.
        ENDIF
        IF (PRESENT(PREPARE_FFT)) THEN
            pr_fft=PREPARE_FFT
        ELSE
            pr_fft=.TRUE.
        ENDIF
        IF (.NOT. alloc_k) pr_fft=.FALSE.
        IF (PRESENT(cptrs)) THEN
            ie_op%cptrs=cptrs
            ie_op%SELF_ALLOCATED=.FALSE.
        ELSE
            IF (alloc_k) THEN
                CALL MPI_COMM_SIZE(comm, Np, IERROR) 
                ie_op%cptrs=ALLOCATE_A2A_PTRS(anomaly,P,Np)
                ie_op%SELF_ALLOCATED=.TRUE.
            ENDIF
        ENDIF
        CALL SET_D2D_TENSOR_DIMENSIONS(ie_op,anomaly,P,comm)
        CALL SET_A2A_KERNEL_POINTERS(ie_op)
        CALL DROP_IE_COUNTER(ie_op)
        ie_op%fftw_threads_ok=fftw_threads_ok
        IF (pr_fft) CALL PREPARE_A2A_FFT(ie_op)
        Nz=ie_op%col%Nz
        ALLOCATE(ie_op%csigb(Nz))
        ALLOCATE(ie_op%sqsigb(Nz))
        ie_op%csiga=>NULL()
        CALL ANOMALY_TO_VERTICAL_DOMAIN(anomaly,ie_op%col)
        CALL BKG_TO_VERTICAL_DOMAIN(bkg,ie_op%col)
        IF (ie_op%SELF_ALLOCATED) ie_op%G_LDA=ie_op%col%Nd
    END SUBROUTINE

    SUBROUTINE  PREPARE_OPERATOR_S2A(ie_op, src, anomaly, bkg, P, comm,&
            fftw_threads_ok,cptrs,&
            &ALLOCATE_KERNEL,PREPARE_FFT)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        TYPE (ANOMALY_TYPE),INTENT(IN)::src
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::comm 
        LOGICAL,INTENT(IN)::fftw_threads_ok
        TYPE(OPERATOR_C_PTR),INTENT(IN),OPTIONAL::cptrs
        LOGICAL,INTENT(IN),OPTIONAL::ALLOCATE_KERNEL
        LOGICAL,INTENT(IN),OPTIONAL::PREPARE_FFT

        INTEGER, PARAMETER :: Pz_src=0
        INTEGER::Nz
        INTEGER(MPI_CTL_KIND)::Np,IERROR
        LOGICAL::alloc_k,pr_fft
        IF (PRESENT(ALLOCATE_KERNEL)) THEN
            alloc_k=ALLOCATE_KERNEL
        ELSE
            alloc_k=.TRUE.
        ENDIF
        IF (PRESENT(PREPARE_FFT)) THEN
            pr_fft=PREPARE_FFT
        ELSE
            pr_fft=.TRUE.
        ENDIF
        IF (.NOT. alloc_k) pr_fft=.FALSE.
        IF (PRESENT(cptrs)) THEN
            ie_op%cptrs=cptrs
            ie_op%SELF_ALLOCATED=.FALSE.
        ELSE
            IF (alloc_k) THEN
                CALL MPI_COMM_SIZE(comm,Np,IERROR) 
                ie_op%cptrs=ALLOCATE_SRC_TO_DOMAIN_PTRS(src, Pz_src, anomaly%Nz,  P, Np)
                ie_op%SELF_ALLOCATED=.TRUE.
            ENDIF
        ENDIF

        CALL SET_D2D_TENSOR_DIMENSIONS(ie_op, src, P,comm)

        ie_op%col=GET_VERTICAL_DOMAIN(anomaly%Nz, P)  !fix vertical dimension

        CALL SET_S2A_KERNEL_POINTERS(ie_op, src%Nz*(Pz_src+1))
        CALL DROP_IE_COUNTER(ie_op)
        ie_op%fftw_threads_ok=fftw_threads_ok

        IF (pr_fft) CALL PREPARE_A2A_FFT(ie_op)
        Nz=ie_op%col%Nz
        
        CALL ANOMALY_TO_VERTICAL_DOMAIN(anomaly,ie_op%col)
        CALL BKG_TO_VERTICAL_DOMAIN(bkg,ie_op%col)
    END SUBROUTINE

    SUBROUTINE  SET_D2D_TENSOR_DIMENSIONS(ie_op,domain,P,comm)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        TYPE (ANOMALY_TYPE),INTENT(IN)::domain
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::comm 
        TYPE(MPI_POSITION_TYPE)::mpi
        INTEGER::Nx, Ny
        INTEGER(MPI_CTL_KIND)::IERROR

        Nx=domain%Nx
        Ny=domain%Ny

        ie_op%Nx=domain%Nx
        ie_op%Ny=domain%Ny

        ie_op%col=GET_VERTICAL_DOMAIN(domain%Nz,P)

        ie_op%Lx=CALC_FULL_LENGTH(P%Nx)
        ie_op%Ly=CALC_FULL_LENGTH(P%Ny)

        mpi=GET_MPI_DATA(comm,Ny)
        ie_op%mpi=mpi

        ie_op%master=(mpi%me==mpi%master_proc)

        IF (MOD(2*Ny,mpi%comm_size)/=0) THEN
            CALL LOGGER('Wrong number of processes. GIEM2G forced to halt!')
            CALL MPI_FINALIZE(IERROR)
            STOP
        ENDIF
        !---------------------------------------------------------------------------------------------!
        ie_op%Nx_loc=Nx/2
        ie_op%Ny_loc=2*Ny/mpi%comm_size
        ie_op%Ny_offset=mpi%me*ie_op%Ny_loc
        ie_op%NxNy_loc=ie_op%Nx_loc*ie_op%Ny_loc
        ie_op%NxHalf=Nx/2
        !---------------------------------------------------------------------------------------------!
        ie_op%real_space=mpi%real_space


    ENDSUBROUTINE
    !--------------------------------------------------------------------------------------------------------------------!


    SUBROUTINE  SET_A2A_KERNEL_POINTERS(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        COMPLEX(REALPARM),POINTER:: ptr(:)
        INTEGER::Nd, NxHalf, Lx, Ly
        INTEGER::N1, N2
        INTEGER::Nxfirst,Nxlast
        CALL C_F_POINTER(ie_op%cptrs%kernel,ptr,(/ie_op%cptrs%kernel_length/))
        IF (ie_op%SELF_ALLOCATED) ptr=C_ZERO
        Nd=ie_op%col%Nd
        NxHalf=ie_op%NxHalf
        N1=ie_op%Ny_offset
        N2=ie_op%Ny_offset+ie_op%Ny_loc-1

        Lx=ie_op%Lx
        Ly=ie_op%Ly

        IF (ie_op%real_space) THEN
            Nxfirst=0
            Nxlast=NxHalf-1
        ELSE
            Nxfirst=NxHalf
            Nxlast=ie_op%Nx-1
        ENDIF

        ie_op%G_asymP(1:Nd,1:Nd,0:Lx-1,0:Ly-1,A_EXZ:A_EYZ,N1:N2,0:NxHalf-1)=>ptr
        ie_op%G_asymPT(1:Nd*Nd,0:Lx-1,0:Ly-1,A_EXZ:A_EYZ,1:ie_op%Ny_loc,Nxfirst:Nxlast)=>ptr

        ptr=>ptr(SIZE(ie_op%G_asymP)+1:)
        ie_op%G_symmP(1:(Nd*(Nd+1))/2,0:Lx-1,0:Ly-1,S_EXX:S_EZZ,N1:N2,0:NxHalf-1)=>ptr
        ie_op%G_symmPT(1:(Nd*(Nd+1))/2,0:Lx-1,0:Ly-1,S_EXX:S_EZZ,1:ie_op%Ny_loc,Nxfirst:Nxlast)=>ptr

        ie_op%MULT_SYMM=>SIMPLE_ZSPMV
        ie_op%MULT_ASYM=>SIMPLE_ZGEMV
    ENDSUBROUTINE

    SUBROUTINE  SET_S2A_KERNEL_POINTERS(ie_op, Nd_src)
        TYPE(OPERATOR_A2A), INTENT(INOUT)::ie_op
        INTEGER,  INTENT(IN) :: Nd_src
        COMPLEX(REALPARM), POINTER:: ptr(:)
        INTEGER::Nx, Ny, Nd, NxHalf, Lx, Ly
        INTEGER::N1, N2
        INTEGER::Nxfirst, Nxlast
        CALL C_F_POINTER(ie_op%cptrs%kernel,ptr,(/ie_op%cptrs%kernel_length/))
        ptr=C_ZERO
        Nd=ie_op%col%Nd
        Nx=ie_op%Nx
        Ny=ie_op%Ny
        NxHalf=ie_op%NxHalf
        N1=ie_op%Ny_offset
        N2=ie_op%Ny_offset+ie_op%Ny_loc-1

        Lx=ie_op%Lx
        Ly=ie_op%Ly

        IF (ie_op%real_space) THEN
            Nxfirst=0
            Nxlast=NxHalf-1
        ELSE
            Nxfirst=NxHalf
            Nxlast=Nx-1
        ENDIF

        ie_op%G_asymP(1:Nd_src,1:Nd,0:Lx-1,0:Ly-1,A_EXZ:A_EYZ,N1:N2,0:NxHalf-1)=>ptr
        ie_op%G_asymPT(1:Nd*Nd_src,0:Lx-1,0:Ly-1,A_EXZ:A_EYZ,1:ie_op%Ny_loc,Nxfirst:Nxlast)=>ptr

        ptr=>ptr(SIZE(ie_op%G_asymP)+1:)
        ie_op%G_symmP(1:Nd*Nd_src,0:Lx-1,0:Ly-1,S_EXX:S_EZZ,N1:N2,0:NxHalf-1)=>ptr
        ie_op%G_symmPT(1:Nd*Nd_src, 0:Lx-1,0:Ly-1,S_EXX:S_EZZ,1:ie_op%Ny_loc,Nxfirst:Nxlast)=>ptr

        ie_op%MULT_SYMM=>ZGEMV_FOR_S2A
        ie_op%MULT_ASYM=>ZGEMV_FOR_S2A
    ENDSUBROUTINE
    !--------------------------------------------------------------------------------------------------------------------!
    SUBROUTINE  PREPARE_A2A_FFT(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        TYPE(C_PTR)::fft_buff_in
        TYPE(C_PTR)::fft_buff_out
        INTEGER(C_INTPTR_T)::buff_len
        INTEGER::Nx,Nx2,Nz,Nd,Ny_loc
        INTEGER::Nx2Ny_loc,Px,Py,Nu
#ifdef USE_FFTW_OPENMP
        INTEGER::NT
#endif
        IF (ie_op%fftw_threads_ok) THEN
#ifdef USE_FFTW_OPENMP
            NT=OMP_GET_MAX_THREADS()
            CALL FFTW_PLAN_WITH_NTHREADS(NT)
#endif
        ENDIF

        Nz=ie_op%col%Nz
        Nd=ie_op%col%Nd
        Nu=ie_op%col%Nu
        Nx=ie_op%Nx
        Nx2=2*ie_op%Nx
        Ny_loc=ie_op%Ny_loc
        Nx2Ny_loc=Nx2*Ny_loc
        Px=ie_op%col%Px
        Py=ie_op%col%Py
        buff_len=ie_op%cptrs%input_fft_length
        fft_buff_in=ie_op%cptrs%input_fft_in
        fft_buff_out=ie_op%cptrs%input_fft_out

        CALL    PrepareDistributedFourierData(ie_op%DFD,Nx2,2*ie_op%Ny,3*Nu,ie_op%mpi%comm,&
            &fft_buff_in,fft_buff_out,buff_len)

        ie_op%field_inP(1:Nd,0:Px,0:Py,1:2,1:3,1:Ny_loc,0:Nx-1)=>ie_op%DFD%field_out
        ie_op%field_outP(1:Nd,0:Px,0:Py,1:2,1:3,1:Ny_loc,0:Nx-1)=>ie_op%DFD%field_in
    ENDSUBROUTINE


    SUBROUTINE A2A_FFTW_FWD_PREPARED(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        CALL CalcPreparedForwardFFT(ie_op%DFD)

    ENDSUBROUTINE

    SUBROUTINE A2A_FFTW_BWD_PREPARED(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        CALL CalcPreparedBackwardFFT(ie_op%DFD)
    ENDSUBROUTINE

    SUBROUTINE CALC_FFT_OF_A2A_KERNEL(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        CALL CalcFFTofIETensorGeneral(ie_op)
    END SUBROUTINE

    SUBROUTINE CalcFFTofIETensorGeneral(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        INTEGER::N,Nd,Nc
        REAL(DOUBLEPARM)::time1,time2
        COMPLEX(REALPARM),POINTER::G(:,:,:,:,:)
        COMPLEX(REALPARM),POINTER::Gout(:,:,:,:,:,:)
        COMPLEX(REALPARM),POINTER:: ptr(:)
        COMPLEX(REALPARM),POINTER:: ptr_symm(:)
        COMPLEX(REALPARM),POINTER:: ptr_asym(:)
        INTEGER::Ipy, Ly, Lpx, Lmx, Lx, Ic
        COMPLEX(REALPARM)::sx,sy
        time1=GetTime()
        Nd=ie_op%col%Nd

        Lx=ie_op%Lx
        Ly=ie_op%Ly
        Lpx=CALC_SAME_SIGN_LENGTH(ie_op%col%Px)
        Lmx=CALC_DIFF_SIGN_LENGTH(ie_op%col%Px)

        Nc=(Nd*(Nd+1))/2
        CALL C_F_POINTER(ie_op%cptrs%kernel,ptr,(/ie_op%cptrs%kernel_length/))
        ptr_symm=>ptr(SIZE(ie_op%G_asymP)+1:)
        Nc= SIZE(ie_op%G_symmP,1)
        N=Nc*Lx

        G(1:N,1:Ly,1:4,1:ie_op%Ny_loc,1:ie_op%NxHalf)=>ptr_symm
        Gout(1:Nc,1:Lx,1:Ly,1:4,1:ie_op%Ny_loc,1:ie_op%NxHalf)=>ptr_symm

        DO Ic=S_EXX,S_EZZ
            sx=SYMM_SIGN_X(Ic)
            IF (Lmx>0) THEN
                DO Ipy=1,Ly
                    sy=GET_SIGN_NAIVE(Ipy)*SYMM_SIGN_Y(Ic)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD, ie_op%mpi,0,Nc*Lpx, Ipy, Ic, sx, sy, G)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD, ie_op%mpi, Nc*Lpx, Nc*Lmx, Ipy, Ic, -sx, sy, G)
                ENDDO
            ELSE
                DO Ipy=1,Ly
                    sy=GET_SIGN_NAIVE(Ipy)*SYMM_SIGN_Y(Ic)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD, ie_op%mpi, 0, Nc*Lpx, Ipy, Ic, sx, sy, G)
                ENDDO
            ENDIF
        ENDDO
        CALL REPACK_TENSOR(G, Nc, Lpx, Lx, Ly, 4, ie_op, Gout)
        ptr_asym=>ptr
        !Nc=Nd*Nd
        Nc= SIZE(ie_op%G_asymP,1)*SIZE(ie_op%G_asymP,2)
        N=Nc*Lx 

        G(1:N,1:Ly,1:2,1:ie_op%Ny_loc,1:ie_op%NxHalf)=>ptr_asym
        Gout(1:Nc,1:Lx,1:Ly,1:2,1:ie_op%Ny_loc,1:ie_op%NxHalf)=>ptr_asym
        DO Ic=A_EXZ,A_EYZ
            sx=ASYM_SIGN_X(Ic)
            IF (Lmx>0) THEN
                DO Ipy=1,Ly
                    sy=GET_SIGN_NAIVE(Ipy)*ASYM_SIGN_Y(Ic)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD,ie_op%mpi,0,Nc*Lpx,Ipy,Ic,sx,sy,G)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD,ie_op%mpi,Nc*Lpx,Nc*Lmx,Ipy,Ic,-sx,sy,G)
                ENDDO
            ELSE
                DO Ipy=1,Ly
                    sy=GET_SIGN_NAIVE(Ipy)*ASYM_SIGN_Y(Ic)
                    CALL CALC_FFT_OF_TENSOR_COMPONENT(ie_op%DFD,ie_op%mpi,0,Nc*Lpx,Ipy,Ic,sx,sy,G)
                ENDDO
            ENDIF
        ENDDO

        CALL REPACK_TENSOR(G,Nc,Lpx,Lx,Ly,2,ie_op,Gout)
        time2=GetTime()
        CALL PRINT_CALC_TIME('FFT of  IE tensor has been computed: ',time2-time1)
        ie_op%counter%tensor_fft=time2-time1
    ENDSUBROUTINE



    SUBROUTINE REPACK_TENSOR(G,Nd,Lpx,Lx,Ly,Lc,ie_op,Gout)
        TYPE(OPERATOR_A2A),TARGET,INTENT(INOUT)::ie_op
        COMPLEX(REALPARM),INTENT(INOUT)::G(1:,1:,1:,1:,1:)
        COMPLEX(REALPARM),INTENT(INOUT)::Gout(1:,1:,1:,1:,1:,1:)
        INTEGER,INTENT(IN)::Nd,Lpx,Lx,Ly,Lc
        COMPLEX(REALPARM)::Gtmp(Nd,Lx)
        INTEGER::Ix,Iy,Ic,Ipy,Ipx1,Ipx2,Id
        INTEGER::Ipx,Jpx
        DO Ix=1,ie_op%NxHalf
            DO Iy=1,ie_op%Ny_loc
                DO Ic=1,Lc
                    DO Ipy=1,Ly
                        Ipx=1
                        DO Ipx1=0,ie_op%col%Px
                            DO Ipx2=0,Ipx1
                                Jpx=GET_REPACK_INDEX(Ipx-1)
                                Jpx=Jpx+Lpx*MOD(Ipx1+Ipx2,2)
                                DO Id=1,Nd
                                    Gtmp(Id, Ipx)=G((Jpx-1)*Nd+Id, Ipy, Ic, Iy, Ix)
                                ENDDO
                                Ipx=Ipx+1
                            ENDDO
                        ENDDO
                        Gout(:, :, Ipy, Ic, Iy, Ix)=Gtmp
                    ENDDO
                ENDDO
            ENDDO
        ENDDO
    ENDSUBROUTINE


    SUBROUTINE PRINT_STATS(ie_op)
        TYPE(OPERATOR_A2A),INTENT(IN)::ie_op
        CALL PRINT_BORDER
        CALL PRINT_CALC_TIME('Time for multiplications:                 ', ie_op%counter%apply)
        CALL PRINT_CALC_TIME('Average fftw forward:                 ', ie_op%counter%mult_fftw/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average fftw backward:                    ', ie_op%counter%mult_fftw_b/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average zgemv:                        ', ie_op%counter%mult_zgemv/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average mult:                     ', ie_op%counter%apply/ie_op%counter%mult_num)
        CALL PRINT_CALC_NUMBER('Number of matrix-vector multiplications:          ', ie_op%counter%mult_num)
        CALL PRINT_BORDER
        CALL PrintTimings(ie_op%DFD)
    END SUBROUTINE

    SUBROUTINE DROP_IE_COUNTER(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        ie_op%counter%apply=0
        ie_op%counter%mult_fftw=0
        ie_op%counter%mult_fftw_b=0
        ie_op%counter%mult_zgemv=0
        ie_op%counter%mult_num=0
        ie_op%counter%dotprod_num=0
        ie_op%counter%dotprod=0
        ie_op%counter%tensor_fft=0
        ie_op%counter%tensor_calc=0
        ie_op%counter%plans=0
        ie_op%counter%solving=0
    END SUBROUTINE

    FUNCTION ALLOCATE_A2A_PTRS(anomaly,P,Np) RESULT(a2a_ptrs)
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Np 
        TYPE (OPERATOR_C_PTR)::a2a_ptrs
        INTEGER(C_INTPTR_T)::local_len
        INTEGER::Nd

        local_len=A2A_LOCAL_KERNEL_SIZE(anomaly,P,Np) 
        CALL PRINT_STORAGE_SIZE('A2A matrix needs', local_len*R_TWO*REALSIZE/1024/1024/1024)
        a2a_ptrs%kernel=ALLOCATE_COMPLEX(local_len)
        a2a_ptrs%kernel_length=local_len

        Nd=anomaly%Nz*(P%Nx+1)*(P%Ny+1)*(P%Nz+1)
        local_len=CalcLocalFFTSize(2*anomaly%Nx,2*anomaly%Ny,3*Nd,Np)
        a2a_ptrs%input_fft_length=local_len
        a2a_ptrs%input_fft_in=ALLOCATE_COMPLEX(local_len)
        a2a_ptrs%input_fft_out=ALLOCATE_COMPLEX(local_len)
        a2a_ptrs%output_fft_length=local_len
        a2a_ptrs%output_fft_in=a2a_ptrs%input_fft_in
        a2a_ptrs%output_fft_out=a2a_ptrs%input_fft_out
    ENDFUNCTION
    

    FUNCTION ALLOCATE_SRC_TO_DOMAIN_PTRS(src, Pz_src, Nz_a, P, Np) RESULT(s2a_ptrs)
        TYPE (ANOMALY_TYPE),INTENT(IN) :: src
        INTEGER, INTENT(IN) ::  Pz_src, Nz_a
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Np 
        TYPE (OPERATOR_C_PTR)::s2a_ptrs
        INTEGER(C_INTPTR_T)::local_len
        INTEGER::Nd

        local_len=S2A_LOCAL_KERNEL_SIZE(Nz_a, Pz_src,  src, P, Np) 
        CALL PRINT_STORAGE_SIZE('S2A matrix needs', local_len*R_TWO*REALSIZE/1024/1024/1024)
        s2a_ptrs%kernel=ALLOCATE_COMPLEX(local_len)
        s2a_ptrs%kernel_length=local_len

        Nd=Nz_a*(P%Nx+1)*(P%Ny+1)*(P%Nz+1)
        local_len=CalcLocalFFTSize(2*src%Nx,2*src%Ny,3*Nd,Np)
        s2a_ptrs%input_fft_length=local_len
        s2a_ptrs%input_fft_in=ALLOCATE_COMPLEX(local_len)
        s2a_ptrs%input_fft_out=ALLOCATE_COMPLEX(local_len)
        s2a_ptrs%output_fft_length=local_len
        s2a_ptrs%output_fft_in=s2a_ptrs%input_fft_in
        s2a_ptrs%output_fft_out=s2a_ptrs%input_fft_out
    ENDFUNCTION

    SUBROUTINE DEALLOCATE_A2A_PTRS(a2a_ptrs)
        TYPE (OPERATOR_C_PTR),INTENT(INOUT)::a2a_ptrs
        CALL DEALLOCATE_COMPLEX(a2a_ptrs%kernel)
        a2a_ptrs%kernel=C_NULL_PTR
        a2a_ptrs%kernel_length=-ONE
        CALL DEALLOCATE_COMPLEX(a2a_ptrs%input_fft_in)
        CALL DEALLOCATE_COMPLEX(a2a_ptrs%input_fft_out)
        a2a_ptrs%input_fft_length=-ONE
        a2a_ptrs%output_fft_length=-ONE
        a2a_ptrs%output_fft_in=C_NULL_PTR
        a2a_ptrs%input_fft_in=C_NULL_PTR
        a2a_ptrs%output_fft_out=C_NULL_PTR
        a2a_ptrs%input_fft_out=C_NULL_PTR
    ENDSUBROUTINE


    FUNCTION A2A_LOCAL_KERNEL_SIZE(anomaly,P,Np) RESULT(local_length)!Quantity of  complex numbers
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Np 
        INTEGER(LONG_INT)::local_length


        INTEGER(LONG_INT)::symm_length
        INTEGER(LONG_INT)::asym_length
        INTEGER(LONG_INT)::NxNyloc
        INTEGER::Nx, Ny, Nd, Npx, Npy

        Nd=anomaly%Nz*(P%Nz+1)
        Npx=CALC_FULL_LENGTH(P%Nx)
        Npy=CALC_FULL_LENGTH(P%Ny)
        Nx=anomaly%Nx
        Ny=anomaly%Ny
        IF ( (MOD(2*Ny,Np)/=0).OR.(MOD(Np,2)/=0) ) THEN
            local_length=-1
            RETURN
        ENDIF

        NxNyloc=(Nx*Ny)/Np
        symm_length=2*Nd*(Nd+1)*NxNyloc*Npx*Npy
        asym_length=2*Nd*Nd*NxNyloc*Npx*Npy
        local_length=symm_length+asym_length
    ENDFUNCTION

    FUNCTION S2A_LOCAL_KERNEL_SIZE(Nz_a, Psrc, src, Pa, Np) RESULT(local_length)!Quantity of  complex numbers
        INTEGER, INTENT(IN) :: Nz_a, Psrc
        TYPE (ANOMALY_TYPE),INTENT(IN)  :: src
        TYPE(POL_ORDER_TYPE),INTENT(IN) :: Pa
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Np 
        INTEGER(LONG_INT)::local_length


        INTEGER(LONG_INT)::symm_length
        INTEGER(LONG_INT)::asym_length
        INTEGER(LONG_INT)::NxNyloc
        INTEGER::Nx, Ny, Npx, Npy
        INTEGER :: Nd_src, Nd_recv

        Nd_src=src%Nz*(Psrc+1)
        Nd_recv=Nz_a*(Pa%Nz+1)
        Npx=CALC_FULL_LENGTH(Pa%Nx)
        Npy=CALC_FULL_LENGTH(Pa%Ny)
        Nx=src%Nx
        Ny=src%Ny
        IF ( (MOD(2*Ny,Np)/=0).OR.(MOD(Np,2)/=0) ) THEN
            local_length=-1
            RETURN
        ENDIF

        NxNyloc=(Nx*Ny)/Np
        symm_length=4*Nd_src*Nd_recv*NxNyloc*Npx*Npy
        asym_length=2*Nd_src*Nd_recv*NxNyloc*Npx*Npy
        local_length=symm_length+asym_length
    ENDFUNCTION

    SUBROUTINE DELETE_OPERATOR_A2A(ie_op)
        TYPE(OPERATOR_A2A),INTENT(INOUT)::ie_op
        ie_op%G_symmP=>NULL()
        ie_op%G_symmPT=>NULL()
        ie_op%G_asymP=>NULL()
        ie_op%G_asymPT=>NULL()
        ie_op%field_inP=>NULL()
        ie_op%field_outP=>NULL()
        DEALLOCATE(ie_op%col%dz,ie_op%col%z)
        !   CALL DeleteDistributedFourierData(ie_op%DFD)
        IF (ie_op%real_space ) THEN
            IF (ASSOCIATED(ie_op%csigb)) DEALLOCATE(ie_op%csigb)
            IF (ASSOCIATED(ie_op%sqsigb)) DEALLOCATE(ie_op%sqsigb)
        ENDIF
        IF (ie_op%SELF_ALLOCATED) CALL DEALLOCATE_A2A_PTRS(ie_op%cptrs)
    ENDSUBROUTINE

    SUBROUTINE SIMPLE_ZGEMV(TRANS, ALPHA, LDA, G, p_in, p_out)
        CHARACTER, INTENT(IN):: TRANS
        COMPLEX(REALPARM),INTENT(IN)::ALPHA
        INTEGER, INTENT(IN) :: LDA
        COMPLEX(REALPARM),INTENT(IN)::G(:)
        COMPLEX(REALPARM),INTENT(IN)::p_in(1:)
        COMPLEX(REALPARM),INTENT(INOUT)::p_out(1:)

        INTEGER :: K
        K=SIZE(p_in,1)
        CALL ZGEMV(TRANS,K, K, ALPHA, G, LDA, p_in, ONE, C_ONE, p_out, ONE)
    ENDSUBROUTINE

    SUBROUTINE SIMPLE_ZSPMV(TRANS, ALPHA, N, G, p_in, p_out)
        CHARACTER, INTENT(IN)::TRANS 
        COMPLEX(REALPARM),INTENT(IN)::ALPHA
        INTEGER, INTENT(IN) :: N
        COMPLEX(REALPARM),INTENT(IN)::G(:)
        COMPLEX(REALPARM),INTENT(IN)::p_in(1:)
        COMPLEX(REALPARM),INTENT(INOUT)::p_out(1:)

        INTEGER :: K
        K=SIZE(p_in,1)
        CALL ZSPMV(TRANS,K,ALPHA,G,p_in,ONE,C_ONE,p_out,ONE)
    ENDSUBROUTINE

    SUBROUTINE ZGEMV_FOR_S2A(T, ALPHA, LDA, G, p_in, p_out)
        CHARACTER, INTENT(IN)::T
        COMPLEX(REALPARM),INTENT(IN)::ALPHA
        INTEGER, INTENT(IN) :: LDA
        COMPLEX(REALPARM),INTENT(IN)::G(:)
        COMPLEX(REALPARM),INTENT(IN)::p_in(:)
        COMPLEX(REALPARM),INTENT(INOUT)::p_out(:)
        CHARACTER :: TRANS
        INTEGER :: N, N_in, N_out

        IF (T=='N') THEN !!! NO Jz in the source by design !!!
            RETURN
        ENDIF

        N=SIZE(G)
        N_out=SIZE(p_out)
        N_in=N/N_out
        TRANS=T
        IF (T=='U') TRANS='T'
        CALL ZGEMV(TRANS, N_in, N_out, ALPHA, G, N_in, p_in, ONE, C_ONE, p_out, ONE)
    ENDSUBROUTINE
ENDMODULE
