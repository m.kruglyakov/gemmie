MODULE EXPONENTIONAL_INTEGRAL_MODULE
    IMPLICIT NONE
    PRIVATE
    INTEGER, PARAMETER ::SP = SELECTED_REAL_KIND(5)
    INTEGER, PARAMETER ::DP = SELECTED_REAL_KIND(15)
    INTEGER, PARAMETER ::QP = SELECTED_REAL_KIND(30)

    REAL (SP),PARAMETER :: PI_SP=3.1415926535897932_SP
    REAL (DP),PARAMETER :: PI_DP=3.1415926535897932_DP
    REAL (QP),PARAMETER :: PI_QP=3.1415926535897932384626433832795028841971693993751058209_QP

    COMPLEX (SP),PARAMETER :: IPI_SP=(0.0_SP,PI_SP)
    COMPLEX (DP),PARAMETER :: IPI_DP=(0.0_DP,PI_DP)
    COMPLEX (QP),PARAMETER :: IPI_QP=(0.0_QP,PI_QP)

    REAL (SP),PARAMETER :: EULER_MASCHERONI_SP=0.5772156649015328606_SP
    REAL (DP),PARAMETER :: EULER_MASCHERONI_DP=0.5772156649015328606_DP
    REAL (QP),PARAMETER :: EULER_MASCHERONI_QP=0.5772156649015328606065120900824024310421593_QP
    INTERFACE E1
        MODULE PROCEDURE E1_SP
        MODULE PROCEDURE E1_DP
        MODULE PROCEDURE E1_QP
    ENDINTERFACE
    INTERFACE E1_SCALED
        MODULE PROCEDURE E1_SCALED_SP
        MODULE PROCEDURE E1_SCALED_DP
        MODULE PROCEDURE E1_SCALED_QP
    ENDINTERFACE
!    PUBLIC::E1,E1_SCALED
CONTAINS

    ELEMENTAL FUNCTION E1_DP(z) RESULT(RES)
        COMPLEX(DP),INTENT(IN)::z
        COMPLEX(DP)::RES
        REAL(DP),PARAMETER::MAX_ABS_SERIES=10.0_DP
        REAL(DP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_DP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=40
        COMPLEX(DP),PARAMETER::C_ZERO=0.0_DP
        COMPLEX(DP),PARAMETER::C_ONE=1.0_DP
        COMPLEX(DP),PARAMETER::C_TWO=2.0_DP
        INTEGER::I
        COMPLEX(DP)::R,E,A,B,Y
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(-z/C_TWO)
            R=-EULER_MASCHERONI_DP-LOG(z)
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                R=R+Y
                IF (ABS(Y/R)<ACCURACY) EXIT
            ENDDO
            RES=R
        ELSE
            E=EXP(-z)
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                A = I /( C_ONE + I /( z + A ))
            ENDDO
            R=C_ONE/(z+A)
            R=R*E
            IF (REAL(z) <=0.0_DP .AND. AIMAG(z)==0.0_DP) THEN
                R=R-IPI_DP
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION
    ELEMENTAL FUNCTION E1_SCALED_DP(z) RESULT(RES) !E1(z)*exp(z)*z
        COMPLEX(DP),INTENT(IN)::z
        COMPLEX(DP)::RES
        REAL(DP),PARAMETER::MAX_ABS_SERIES=2.0_DP
        REAL(DP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_DP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=80
        COMPLEX(DP),PARAMETER::C_ZERO=0.0_DP
        COMPLEX(DP),PARAMETER::C_ONE=1.0_DP
        COMPLEX(DP),PARAMETER::C_TWO=2.0_DP
        INTEGER::I
        COMPLEX(DP)::R,E,A,B,Y,E2
        RES=C_ZERO
        IF (ABS(z)==0.0_DP) RETURN
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(z/C_TWO)
            E2=E*E*z
            E=E*z
            R=-(EULER_MASCHERONI_DP+LOG(z))*E2
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                IF (ABS(Y/R)<ACCURACY) EXIT
                R=R+Y
            ENDDO
            RES=R
        ELSE
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                B=C_ONE/I
                A = C_ONE /( B + C_ONE /( z + A ))
            ENDDO
            A=A/z
            R=C_ONE/(C_ONE+A)
            IF (REAL(z) <=0.0_DP .AND. AIMAG(z)==0.0_DP) THEN
                R=R-IPI_DP*EXP(z)*z
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION

    ELEMENTAL FUNCTION E1_QP(z) RESULT(RES)
        COMPLEX(QP),INTENT(IN)::z
        COMPLEX(QP)::RES
        REAL(QP),PARAMETER::MAX_ABS_SERIES=10.0_QP
        REAL(QP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_QP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=80
        COMPLEX(QP),PARAMETER::C_ZERO=0.0_QP
        COMPLEX(QP),PARAMETER::C_ONE=1.0_QP
        COMPLEX(QP),PARAMETER::C_TWO=2.0_QP
        INTEGER::I
        COMPLEX(QP)::R,E,A,B,Y
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(-z/C_TWO)
            R=-EULER_MASCHERONI_QP-LOG(z)
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                IF (ABS(Y/R)<ACCURACY) EXIT
                R=R+Y
            ENDDO
            RES=E
        ELSE
            E=EXP(-z)
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                A = I /( C_ONE + I /( z + A ))
            ENDDO
            R=C_ONE/(z+A)
            R=R*E
            IF (REAL(z) <=0.0_QP .AND. AIMAG(z)==0.0_QP) THEN
                R=R-IPI_DP
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION
    ELEMENTAL FUNCTION E1_SCALED_QP(z) RESULT(RES) !E1(z)*exp(z)*z
        COMPLEX(QP),INTENT(IN)::z
        COMPLEX(QP)::RES
        REAL(QP),PARAMETER::MAX_ABS_SERIES=2.0_QP
        REAL(QP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_QP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=160
        COMPLEX(QP),PARAMETER::C_ZERO=0.0_QP
        COMPLEX(QP),PARAMETER::C_ONE=1.0_QP
        COMPLEX(QP),PARAMETER::C_TWO=2.0_QP
        INTEGER::I
        COMPLEX(QP)::R,E,A,B,Y,E2
        RES=C_ZERO
        IF (ABS(z)==0.0_QP) RETURN
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(z/C_TWO)
            E2=E*E*z
            E=E*z
            R=-(EULER_MASCHERONI_QP+LOG(z))*E2
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                IF (ABS(Y/R)<ACCURACY) EXIT
                R=R+Y
            ENDDO
            RES=R
        ELSE
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                B=C_ONE/I
                A = C_ONE /( B + C_ONE /( z + A ))
            ENDDO
            A=A/z
            R=C_ONE/(C_ONE+A)
            IF (REAL(z) <=0.0_QP .AND. AIMAG(z)==0.0_QP) THEN
                R=R-IPI_QP*EXP(z)*z
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION

    ELEMENTAL FUNCTION E1_SP(z) RESULT(RES)
        COMPLEX(SP),INTENT(IN)::z
        COMPLEX(SP)::RES
        REAL(SP),PARAMETER::MAX_ABS_SERIES=10.0_SP
        REAL(SP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_SP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=20
        COMPLEX(SP),PARAMETER::C_ZERO=0.0_SP
        COMPLEX(SP),PARAMETER::C_ONE=1.0_SP
        COMPLEX(SP),PARAMETER::C_TWO=2.0_SP
        INTEGER::I
        COMPLEX(SP)::R,E,A,B,Y
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(-z/C_TWO)
            R=-EULER_MASCHERONI_SP-LOG(z)
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                IF (ABS(Y/R)<ACCURACY) EXIT
                R=R+Y
            ENDDO
            RES=E
        ELSE
            E=EXP(-z)
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                A = I /( C_ONE + I /( z + A ))
            ENDDO
            R=C_ONE/(z+A)
            R=R*E
            IF (REAL(z) <=0.0_SP .AND. AIMAG(z)==0.0_SP) THEN
                R=R-IPI_SP
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION
    ELEMENTAL FUNCTION E1_SCALED_SP(z) RESULT(RES) !E1(z)*exp(z)*z
        COMPLEX(SP),INTENT(IN)::z
        COMPLEX(SP)::RES
        REAL(SP),PARAMETER::MAX_ABS_SERIES=10.0_SP
        REAL(SP),PARAMETER::ACCURACY=EPSILON(EULER_MASCHERONI_SP)
        INTEGER,PARAMETER::E1_FRACTION_DEPTH=30
        COMPLEX(SP),PARAMETER::C_ZERO=0.0_SP
        COMPLEX(SP),PARAMETER::C_ONE=1.0_SP
        COMPLEX(SP),PARAMETER::C_TWO=2.0_SP
        INTEGER::I
        COMPLEX(SP)::R,E,A,B,Y,E2
        RES=C_ZERO
        IF (ABS(z)==0.0_SP) RETURN
        IF (ABS(z)<MAX_ABS_SERIES) THEN
            E=EXP(z/C_TWO)
            E2=E*E*z
            E=E*z
            R=-(EULER_MASCHERONI_SP+LOG(z))*E2
            I=1
            A=z*E
            B=C_ONE
            Y=C_ZERO
            DO
                Y=A*B
                A=A*z/(I+C_ONE)/C_TWO
                Y=Y+A*B
                I=I+2
                B=B+C_ONE/I
                A=A*z/I/C_TWO
                IF (ABS(Y/R)<ACCURACY) EXIT
                R=R+Y
            ENDDO
            RES=R
        ELSE
            A=C_ZERO
            DO I = E1_FRACTION_DEPTH,1,-1
                B=C_ONE/I
                A = C_ONE /( B + C_ONE /( z + A ))
            ENDDO
            A=A/z
            R=C_ONE/(C_ONE+A)
            IF (REAL(z) <=0.0_SP .AND. AIMAG(z)==0.0_SP) THEN
                R=R-IPI_SP*EXP(z)*z
            ENDIF

            RES=R
        ENDIF
    ENDFUNCTION

ENDMODULE
