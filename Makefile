
ifndef PLATFORM
	PLATFORM=RELEASE
endif

USE_PGIEM2G=true #Only for the sake of compatibility with legacy configuration format

ifneq ($(MAKECMDGOALS),clean)
ifndef LOCAL_OPTIONS
	LOCAL_OPTIONS = make.inc.develop
endif

include $(LOCAL_OPTIONS)

endif
####################################################################################
OPTS+= $(NO_WARN)
OPTS+=$(USER_OPTIONS)
ifdef use_traps
	OPTS+=$(TRAPS)
endif


INCLUDE+= -I. -I./General_Utilities 

GEMMIE_SRC:=all_gemmie_src
GEMMIE_DEPS:=all_gemmie_deps
GEMMIE_OBJS:=all_gemmie_objs

GLOB_GEMMIE_DEPS:=global_gemmie_deps
GLOB_GEMMIE_OBJS:=global_gemmie_objs

ifndef NO_PGIEM2G

	PGIEM2G_DIR=./PGIEM2G_KERNEL
	PGIEM2G_INC= -I$(PGIEM2G_DIR) 
	PGIEM2G_INC+= -I$(PGIEM2G_DIR)/generated/Horizontal  -I$(PGIEM2G_DIR)/generated/include
	PGIEM2G_INC+= -I$(PGIEM2G_DIR)/generated/Templates
	INCLUDE+= $(PGIEM2G_INC)
	LIBS:= $(PGIEM2G_FFTW3_MPI_LIB) $(LIBS)
	OPTS+=-D RECIEVERS_AT_CELL_BORDERS
	OPTS+=-D USE_OLD_LATERAL_PWC
endif


ifndef NO_HDF5
	OPTS+=-D USE_HDF5_IO
	USE_HDF5:=1
else
	OPTS+=-D NO_HDF5
endif


ifneq ($(USE_HDF5), 1)
	EXCLUDE_MODULES+= ./GEMMIE_UTILS/HDF5_IO_UTILS.F90
endif



ifeq ($(MAKECMDGOALS),clean)
	CLEAN_O:=$(shell sed  ' s/F90$$/o/g; s/for$$/o/g ' < $(GEMMIE_SRC))
else
ifdef REBUILD_DEPS
$(shell ./gen_deps.sh $(GEMMIE_SRC) $(GEMMIE_DEPS) $(GEMMIE_OBJS) $(GLOB_GEMMIE_DEPS) $(GLOB_GEMMIE_OBJS) )
endif

ifdef NO_PGIEM2G
GEMMIE_DEPS:=$(GLOB_GEMMIE_DEPS)
GEMMIE_OBJS:=$(GLOB_GEMMIE_OBJS)
endif

include $(GEMMIE_OBJS)

ifndef MAKECMDGOALS
	progs:=$(ALL_PROGRAMS)
else
ifneq ($(MAKECMDGOALS), all)
	progs:=$(MAKECMDGOALS)
else
	progs:=$(ALL_PROGRAMS)
endif
endif
endif

ifeq ($(filter reshape_dataset, $(MAKECMDGOALS)), reshape_dataset)
	LIBS:=
endif

ifeq ($(USE_HDF5), 1)
	LIBS+= $(HDF5_LIBS)
endif

ifndef DYNAMIC_LIBS
	ifneq ($(FC), ftn)
	LIBS:= -Wl,-Bstatic $(LIBS)  -Wl,-Bdynamic 
else
	LIBS:= -h static $(LIBS) 
endif
endif

ifeq ($(USE_HDF5), 1)
	INCLUDE+= $(HDF5_INCS)
	LIBS+= $(DYN_LIBS)
endif


LOPTS:=$(LOPTS) $(OPTS)



all: $(progs)

.PHONY: all

ifneq ($(MAKECMDGOALS),clean)
include $(GEMMIE_DEPS)
endif

$(progs): $(all_objs)
	$(FC) $(LOPTS)   $(OBJ_$(@)) -o $@  $(LIBS)
ifdef INSTALL_PATH
	cp  $@ $(INSTALL_PATH)/
endif

reshape_dataset: Utilities/reshape_dataset.o 


# USE-based dependencies


include include/Radial/templates/mk_by_templates


%.o : %.for  $(LOCAL_OPTIONS) Makefile 
	$(FC) $(OPTS)  -c $*.for -o $*.o $(INCLUDE)

%.o : %.F90  $(LOCAL_OPTIONS) Makefile
	$(FC) $(OPTS)  -c $*.F90 -o $*.o $(INCLUDE)


# Cleaning
clean:  
	rm -rf $(CLEAN_O)
	rm -rf *.mod *.MOD	
.PHONY: clean


