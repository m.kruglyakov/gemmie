
ifndef PLATFORM
	PLATFORM=DEBUG
endif


ifeq ($(PLATFORM), DEBUG)
	OPTS=-g  -O3 -fstack-arrays -fcheck=all -fbacktrace  -Wall -Wextra -Wpedantic  -Wfatal-errors 	 
	OPTS+= -D trap_errorneous
else
	#OPTS=-g   -O3  -fbacktrace -fstack-arrays -flto   -march=native -mtune=native  
	OPTS=-g   -Ofast -ffast-math    -march=native -mtune=native  
	NO_WARN=  -Wno-unused-variable -Wno-tabs -Wno-unused-parameter -Wno-unused-dummy-argument -Wno-unused-function
endif

ifdef USE_PGIEM2G
	USE_MPI=1
	USE_OPENMP=1
	USE_FFTW_OPENMP=1

	PGIEM2G_DIR=$(HOME)/src/PGIEM2G
	PGIEM2G_LIB= -L$(PGIEM2G_DIR) -lgiem2g -L$(FFTW3_ROOT_DIR)/lib  -lfftw3_mpi -lfftw3_omp -lfftw3
	PGIEM2G_INC= -I$(PGIEM2G_DIR) -I$(PGIEM2G_DIR)/generated/Vertical
	PGIEM2G_INC+= 	-I$(PGIEM2G_DIR)/generated/Horizontal  -I$(PGIEM2G_DIR)/generated/include
  	PGIEM2G_INC+= -I$(PGIEM2G_DIR)/generated/Templates

	OPTS+=-D MERGE_PGIEM2G
endif

ifndef USE_MPI
	FC=gfortran
else
	FC=mpifort
	OPTS+=-D USE_MPI
	OPTS+=-D LEGACY_MPI=1
endif

ifdef USE_OPENMP
	OPTS+= -D USE_OPENMP 
	OPTS+= -fopenmp
else
	#OPTS+= -fopenmp
endif

TRAPS= -ffpe-trap=zero,underflow,overflow,denormal,invalid -finit-real=snan -finit-derived

FFTW_INC=$(FFTW3_ROOT_DIR)/include/
LIB_BLAS=$(GOTOBLAS2)/lib/ -lopenblas


ifdef USE_FFTW_OPENMP
	LIB_FFTW=$(FFTW3_ROOT_DIR)/lib  -lfftw3_omp  -lfftw3 
	OPTS+= -D USE_FFTW_OPENMP
else
	LIB_FFTW=$(FFTW3_ROOT_DIR)/lib    -lfftw3 
endif

HDF5_LIBS =-L$(HDF5ROOT)/lib  -lhdf5hl_fortran -lhdf5_hl -lhdf5_fortran -lhdf5
HDF5_INCS=-I$(HDF5ROOT)/include


####################################################################################
OPTS+= $(NO_WARN)
OPTS+=$(USER_OPTIONS)
ifdef use_traps
	OPTS+=$(TRAPS)
endif


LIBS+= -L$(LIB_BLAS)
LIBS+=  -L$(LIB_FFTW)
INCLUDE= -I. -I./General_Utilities -I$(FFTW_INC)

ifdef USE_PGIEM2G
	INCLUDE+= $(PGIEM2G_INC)
	OPTS+= -fopenmp
endif


ifneq ($(use-git-files), no)

SRC:=$(shell git ls-files |grep -E "\w+\.for|\.FOR|\.f90|\.F90" )

else

$(warning "All fortran source files from current directory and subdirectories  will be used in building dependencies")

endif

ifeq ($(MAKECMDGOALS),clean)
	CLEAN_O:=$(SRC:%.F90=%.o)
endif
ifdef USE_HDF5_IO
	OPTS+=-D USE_HDF5_IO
	USE_HDF5:=1
endif

ifeq ($(filter gemmie, $(MAKECMDGOALS)), gemmie)
endif

ifeq ($(filter convert_br, $(MAKECMDGOALS)), convert_br)
	USE_HDF5:=1
endif
ifeq ($(filter prepare_grid, $(MAKECMDGOALS)), prepare_grid)
	USE_HDF5:=1
endif

ifeq ($(filter tester, $(MAKECMDGOALS)), tester)
	USE_HDF5:=1
endif

ifneq ($(USE_HDF5), 1)
	EXCLUDE_MODULES+= ./GEMMIE_UTILS/HDF5_IO_UTILS.F90
endif

ifneq ($(NETCDF_IO), 1)
	EXCLUDE_MODULES+= ./GEMMIE_UTILS/NETCDF_IO_UTILS.F90
endif


ifeq ($(filter gemmie, $(MAKECMDGOALS)), gemmie)
MAIN_PROG=./gemmie.F90
DEPS=gemmie.deps
fobjs:=$(shell ./mk_fortan_deps.pl -r  -f $(SRC) --include-paths $(INCLUDE)  -t $(MAIN_PROG) -o $(DEPS)  -e $(EXCLUDE_MODULES))
ifeq ($(fobjs), ) 

$(error "Error in the autogeneration of the dependences")
else

endif
endif

ifeq ($(filter validator, $(MAKECMDGOALS)), validator)
	VALIDATOR_PROG:=./Utilities/validate_input.F90
	VAL_DEPS:=validator.deps
	validate_objs:=$(shell ./mk_fortan_deps.pl -r -f $(SRC) --include-paths $(INCLUDE) -t $(VALIDATOR_PROG) -o $(VAL_DEPS) -e $(EXCLUDE_MODULES) )
endif

ifeq ($(filter tester, $(MAKECMDGOALS)), tester)
	TESTER_PROG=./Utilities/tester.F90
	TESTER_DEPS=tester.deps
	tester_objs:=$(shell ./mk_fortan_deps.pl -r -f $(SRC) --include-paths $(INCLUDE) -t $(TESTER_PROG) -o $(TESTER_DEPS) -e $(EXCLUDE_MODULES) )
endif

ifeq ($(filter prepare_grid, $(MAKECMDGOALS)), prepare_grid)
	PG_PROG=./GAMERA_UTILITIES/prepare_grid.F90
	PG_DEPS=pg.deps
	pg_objs:=$(shell ./mk_fortan_deps.pl -r -f $(SRC) --include-paths $(INCLUDE) -t $(PG_PROG) -o $(PG_DEPS) -e $(EXCLUDE_MODULES) )
endif

ifeq ($(filter convert_br, $(MAKECMDGOALS)), convert_br)
	CBR_PROG=./GAMERA_UTILITIES/Convert_Br.F90
	CBR_DEPS=cbr.deps
	cbr_objs:=$(shell ./mk_fortan_deps.pl -r -f $(SRC) --include-paths $(INCLUDE) -t $(CBR_PROG) -o $(CBR_DEPS) -e $(EXCLUDE_MODULES) )
endif

ifeq ($(USE_HDF5), 1)
	LIBS+= $(HDF5_LIBS)
endif

LIBS:= -Wl,-Bstatic $(LIBS)  -Wl,-Bdynamic

ifeq ($(USE_HDF5), 1)
	INCLUDE+= $(HDF5_INCS)
	LIBS+= -lz -lsz -ldl
endif


gemmie: $(fobjs) 
	$(FC) $(OPTS)   $(fobjs)  -o gemmie $(PGIEM2G_LIB) $(LIBS)
	echo "DONE!!"

validator: $(validate_objs)
	$(FC) $(OPTS)   $(validate_objs)  -o validate_gemmie_input  $(LIBS)
	echo "DONE!!"

tester: $(tester_objs)
	$(FC) $(OPTS)   $(tester_objs)  -o tester  $(LIBS)

prepare_grid: $(pg_objs)
	$(FC) $(OPTS)   $(pg_objs)  -o prepare_grid  $(LIBS)

convert_br: $(cbr_objs)
	$(FC) $(OPTS)   $(cbr_objs)  -o convert_br  $(LIBS)

$(fobjs):	 Makefile.euler.gnu

pgiem2g: 
ifdef USE_PGIEM2G
	cd $(PGIEM2G_DIR) && $(MAKE) -j PLATFORM=GNU_EULER libgiem2g.a
endif
# USE-based dependencies


include $(DEPS)
include $(VAL_DEPS)
include $(TESTER_DEPS)
include $(PG_DEPS)

include include/Radial/templates/mk_by_templates

Cartesian/Cartesian_Operators.o: pgiem2g

%.o : %.F90 
	$(FC) $(OPTS)  -c $*.F90 -o $*.o $(INCLUDE)

# Cleaning
clean:  
	rm -rf $(CLEAN_O)
	rm -rf *.mod *.MOD	
	rm -rf *.deps


