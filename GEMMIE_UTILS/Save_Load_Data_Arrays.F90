MODULE SAVE_LOAD_DATA_ARRAYS_MODULE
    USE, INTRINSIC :: ISO_FORTRAN_ENV
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE MPI_UTILITIES_MODULE
    USE LOGGER_MODULE
    USE CTL_TYPES_MODULE
    USE RADIAL_DATA_TYPES_MODULE
    USE CARTESIAN_DATA_TYPES
    USE CONTAINERS_MODULE
    USE IO_UTILITIES
    USE Distributed_IO_Module
    USE HDF5_IO

    USE ARRAYS_UTILITIES
    IMPLICIT NONE
    PRIVATE
    
    INTERFACE LOAD_VECTOR_FIELD
        MODULE PROCEDURE LOAD_VECTOR_FIELD_Currents
        MODULE PROCEDURE LOAD_VECTOR_FIELD_SPH
        MODULE PROCEDURE LOAD_VECTOR_FIELD_CART
    ENDINTERFACE

    INTERFACE ALLOCATE_FIELD_FOR_DOMAIN
        MODULE PROCEDURE ALLOCATE_CURRENTS_IN_DOMAIN_SPH
        MODULE PROCEDURE ALLOCATE_CURRENTS_IN_DOMAIN_CART
    ENDINTERFACE

    PUBLIC :: LOAD_VECTOR_FIELD
    PUBLIC :: ALLOCATE_FIELD_FOR_DOMAIN
CONTAINS

    SUBROUTINE LOAD_VECTOR_FIELD_Currents(run_data, I, mpi, S_A, Currents)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        TYPE(CURRENTS_CONTAINER),  INTENT(INOUT) :: Currents
        IF (run_data%solver%IO_format==GEMMIE_BIN_FORMAT) THEN
            CALL LOAD_VECTOR_FIELD_GEMMIE_BIN(run_data, I, mpi, S_A, Currents)
        ELSEIF (run_data%solver%IO_format==HDF5_FORMAT) THEN
            CALL LOAD_VECTOR_FIELD_HDF5(run_data, I, mpi, S_A, Currents)
        ELSE
        ENDIF    
    ENDSUBROUTINE

    SUBROUTINE LOAD_VECTOR_FIELD_SPH(run_data, I, mpi, S_A, J_ext_sph)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: J_ext_sph(:,:,:,:)
        TYPE(CURRENTS_CONTAINER)  :: Currents
         
        CALL LOAD_VECTOR_FIELD(run_data, I, mpi, S_A, Currents)
        CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%Sources(I)%Currents_Data%SPH_Source_Domain, J_ext_sph)
        J_ext_sph=Currents%J_ext_sph
    ENDSUBROUTINE

    SUBROUTINE LOAD_VECTOR_FIELD_CART(run_data, I, mpi, S_A, J_ext_cart)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: J_ext_cart(:,:,:,:,:,:,:)
        TYPE(CURRENTS_CONTAINER)  :: Currents
         
        CALL LOAD_VECTOR_FIELD(run_data, I, mpi, S_A, Currents)
        CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%Sources(I)%Currents_Data%Cart_Source_Domain, J_ext_cart)
        J_ext_cart=Currents%J_ext_cart
    ENDSUBROUTINE

    SUBROUTINE LOAD_VECTOR_FIELD_GEMMIE_BIN(run_data, I, mpi, S_A, Currents)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        TYPE(CURRENTS_CONTAINER),  INTENT(INOUT) :: Currents
        INTEGER :: IERROR
        CHARACTER(len=2048) :: ERR_MSG
        IERROR=0

        IF (S_A .EQV. SOURCE_IS_ANOMALY) THEN
            IF (run_data%solver%SPHERICAL) THEN
                CALL LOAD_SRC_INSIDE_ANOMALY_SPH(run_data%Sources(I), mpi, &
                   Currents%J_ext_sph, IERROR, ERR_MSG)
            ELSE

            ENDIF
        ELSE
            IF (run_data%solver%SPHERICAL) THEN
                CALL LOAD_SRC_OUTSIDE_ANOMALY_SPH(run_data%Sources(I), mpi, &
                    Currents%J_ext_sph, IERROR, ERR_MSG)
            ELSE
                CALL LOAD_SRC_OUTSIDE_ANOMALY_CART(run_data%Sources(I), mpi, &
                    Currents%J_ext_cart, IERROR, ERR_MSG)
            ENDIF
        ENDIF

        IF (IERROR/=0) THEN
            CALL LOGGER_ERROR("During loading fields")
            CALL LOGGER_ERROR(TRIM(ERR_MSG))
            CALL FINALIZE_GEMMIE()
            STOP 1
        ENDIF
    ENDSUBROUTINE

    SUBROUTINE LOAD_VECTOR_FIELD_HDF5(run_data, I, mpi, S_A, Currents)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        TYPE(CURRENTS_CONTAINER),  INTENT(INOUT) :: Currents
        INTEGER :: IERROR
        CHARACTER(len=2048) :: ERR_MSG
        IERROR=0

        IF (S_A .EQV. SOURCE_IS_ANOMALY) THEN
            IF (run_data%solver%SPHERICAL) THEN
                CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%SPH_Anomaly, Currents%J_ext_sph)
                CALL LOAD_CURRENTS_SPH(run_data%Sources(I)%Source_Data_fn, mpi, &
                   Currents%J_ext_sph)
            ELSE
                CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%CART_Anomaly, Currents%J_ext_cart)
                CALL LOAD_CURRENTS_CART(run_data%Sources(I)%Source_Data_fn, mpi, &
                   Currents%J_ext_cart)
            ENDIF
        ELSE
            IF (run_data%solver%SPHERICAL) THEN
                CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%Sources(I)%Currents_Data%SPH_Source_Domain,&
                     Currents%J_ext_sph)
                CALL LOAD_CURRENTS_SPH(run_data%Sources(I)%Source_Data_fn, mpi, &
                   Currents%J_ext_sph)
            ELSE
                CALL ALLOCATE_FIELD_FOR_DOMAIN(run_data%Sources(I)%Currents_Data%Cart_Source_Domain,&
                     Currents%J_ext_cart)
                CALL LOAD_CURRENTS_CART(run_data%Sources(I)%Source_Data_fn, mpi, &
                   Currents%J_ext_cart)
            ENDIF
        ENDIF

        IF (IERROR/=0) THEN
            CALL LOGGER_ERROR("During loading fields")
            CALL LOGGER_ERROR(TRIM(ERR_MSG))
            CALL FINALIZE_GEMMIE()
            STOP 1
        ENDIF
    ENDSUBROUTINE

    SUBROUTINE LOAD_VECTOR_FIELD_NETCDF(run_data, I, mpi, S_A, Currents)
        TYPE(RUN_INPUT), INTENT(IN) :: run_data
        INTEGER :: I
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        LOGICAL, INTENT(IN) :: S_A
        TYPE(CURRENTS_CONTAINER),  INTENT(INOUT) :: Currents
        INTEGER :: IERROR
        IERROR=-1

    ENDSUBROUTINE

    SUBROUTINE LOAD_SRC_INSIDE_ANOMALY_SPH(Source, mpi, J_data, IERROR, ERR_MSG)
        TYPE (SOURCE_DATA_TYPE), INTENT(IN) :: Source
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        COMPLEX(DP), INTENT(INOUT) :: J_data(:,:,:,:)
        INTEGER, INTENT(OUT) :: IERROR
        CHARACTER(len=*), INTENT(OUT) :: ERR_MSG
        COMPLEX(DP), ALLOCATABLE  :: J_tmp(:,:,:,:)
        IERROR=0

        ALLOCATE (J_tmp(SIZE(J_data,3), SIZE(J_data,1), SIZE(J_data,2),&
            SIZE(J_data,4)))

        CALL LOAD_ARRAY(Source%Source_Data_fn, mpi, J_tmp, IERROR, ERR_MSG)
        J_data=RESHAPE(J_tmp, SHAPE(J_data), ORDER=[3, 1, 2, 4]);
        DEALLOCATE(J_tmp)
    ENDSUBROUTINE

    SUBROUTINE ALLOCATE_CURRENTS_IN_DOMAIN_SPH(Domain, j_data)
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Domain
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: j_data(:,:,:,:)
        INTEGER :: J_BNDS(4,2)

        J_BNDS(:,1)=[1,1, E_THETA, Domain%Nphi_loc(1)]
        J_BNDS(:,2)=[Domain%Nr, Domain%Ntheta, E_R, Domain%Nphi_loc(2)]
        CALL REALLOCATE_IF_SHAPE_DIFFERENT(J_data, J_BNDS)
    ENDSUBROUTINE

    SUBROUTINE LOAD_SRC_OUTSIDE_ANOMALY_SPH(Source, mpi, j_data, IERROR, ERR_MSG)
        TYPE (SOURCE_DATA_TYPE), INTENT(IN) :: Source
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: j_data(:,:,:,:)
        INTEGER, INTENT(OUT) :: IERROR
        CHARACTER(len=*), INTENT(OUT) :: ERR_MSG
        INTEGER :: Nphi_loc(2)
        COMPLEX(DP), ALLOCATABLE  :: j_tmp(:,:,:,:)
        IERROR=0
        ASSOCIATE(Domain=>Source%Currents_Data%SPH_Source_Domain)
            Nphi_loc=GET_SUB_ARRAY_INDEXES(Domain%Nphi,&
            mpi%me, mpi%comm_size)

            CALL ALLOCATE_FIELD_FOR_DOMAIN(Source%Currents_Data%SPH_Source_Domain, j_data)

            IF (ALLOCATED(Source%Currents_Data%J_ext_sph)) THEN
                J_data=Source%Currents_Data%J_ext_sph
                RETURN
            ENDIF

            ALLOCATE (J_tmp(1:3, Domain%Nr, Domain%Ntheta,&
                 Nphi_loc(1): Nphi_loc(2)))
            CALL LOAD_ARRAY(Source%Source_Data_fn, mpi, J_tmp, IERROR, ERR_MSG)
            J_data=RESHAPE(J_tmp,SHAPE(J_data),ORDER=[3, 1, 2, 4]);
            DEALLOCATE(J_tmp)
        END ASSOCIATE
    ENDSUBROUTINE

    SUBROUTINE ALLOCATE_CURRENTS_IN_DOMAIN_CART(Domain, j_data)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Domain
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: j_data(:,:,:,:,:,:,:)
        INTEGER :: J_BNDS(7,2)
        ASSOCIATE(&
                Px=>Domain%Px,& 
                Py=>Domain%Py,& 
                Pz=>Domain%Pz) 

            J_BNDS(:,1)=[1, 1, 0,0,0,   1,  Domain%Ny_loc(1)]
            J_BNDS(:,2)=[Domain%Nz, 3, Pz, Px, Py,  Domain%Nx,   Domain%Ny_loc(2)]
            CALL REALLOCATE_IF_SHAPE_DIFFERENT(J_data, J_BNDS)

        END ASSOCIATE
    ENDSUBROUTINE


    SUBROUTINE LOAD_SRC_OUTSIDE_ANOMALY_CART(Source, mpi, j_data, IERROR, ERR_MSG)
        TYPE (SOURCE_DATA_TYPE), INTENT(IN) :: Source
        TYPE(MPI_STATE_TYPE), INTENT(IN) :: mpi
        COMPLEX(DP), ALLOCATABLE, INTENT(INOUT) :: j_data(:,:,:,:,:,:,:)
        INTEGER, INTENT(OUT) :: IERROR
        CHARACTER(len=*), INTENT(OUT) :: ERR_MSG
        INTEGER :: Ny_loc(2)
        COMPLEX(DP), ALLOCATABLE  :: j_tmp(:,:,:,:,:,:,:)
        INTEGER :: J_BNDS(7,2)
        IERROR=0
        ASSOCIATE(Domain=>Source%Currents_Data%Cart_Source_Domain,&
                Px=>Source%Currents_Data%Cart_Source_Domain%Px,& 
                Py=>Source%Currents_Data%Cart_Source_Domain%Py,& 
                Pz=>Source%Currents_Data%Cart_Source_Domain%Pz) 
            
            Ny_loc=GET_SUB_ARRAY_INDEXES(Domain%Ny,&
            mpi%me, mpi%comm_size)

            J_BNDS(:,1)=[0,0,0, 1, 1, 1,  Ny_loc(1)]
            J_BNDS(:,2)=[Pz, Px, Py, 3, Domain%Nz, Domain%Nx,  Ny_loc(2)]

            CALL REALLOCATE_IF_SHAPE_DIFFERENT(J_tmp, J_BNDS)


            CALL LOAD_ARRAY(Source%Source_Data_fn, mpi, J_tmp, IERROR, ERR_MSG)

            CALL ALLOCATE_FIELD_FOR_DOMAIN(Source%Currents_Data%Cart_Source_Domain, j_data)

            J_data=RESHAPE(J_tmp,SHAPE(J_data),ORDER=[5, 4, 1, 2, 3, 6, 7]);
            DEALLOCATE(J_tmp)
        END ASSOCIATE
    ENDSUBROUTINE


END MODULE
