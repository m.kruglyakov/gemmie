function cnv_currents_PGIM2G_TO_GEMMIE(fin, fout)
	jp=read_GEMMIE_bin_array (fin , 7);
	s=size(jp);
	
	s2=s;
	s2(2)=[];
	
	p_order=[2, 3, 4, 1, 5, 6];

	Jx=reshape(jp(:,1,:,:,:,:,:), s2);
	Jx=permute(Jx, p_order);

	Jy=reshape(jp(:,2,:,:,:,:,:), s2);
	Jy=permute(Jy, p_order);

	Jz=reshape(jp(:,3,:,:,:,:,:), s2);
	Jz=permute(Jz, p_order);

	write_currents_hdf5(Jx, Jy, Jz, fout ,{'J_x', 'J_y', 'J_z'});
end

