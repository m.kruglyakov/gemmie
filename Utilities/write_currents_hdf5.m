%J* are arrays of the following format 
%In spherical case J(Nr, Ntheta, Nphi)
%In Cartesian case J(Pz+1, Px+1, Py+1, Nz, Nx, Ny); P* state for polynomial orders
%fname - name of the output hdf5 file
%d_names - cell-array of names for the current components in hdf5 file. Default names are {'J_theta', 'J_phi', 'J_r'} i.e. spherical case
%for Cartesian case d_names should be {'J_x', 'J_y', 'J_z'}
function write_currents_hdf5(Jtheta, Jphi, Jr, fname, d_names)
	if exist(fname, 'file')
		delete(fname);
	end

	if ~exist('d_names', 'var')
		d_names={'J_theta', 'J_phi', 'J_r'};
	end

	if isOctave
		d_names0={'J_theta', 'J_phi', 'J_r'};
		tmp=struct();
		tmp.Re_J_theta=real(Jtheta);
		tmp.Im_J_theta=imag(Jtheta);

		tmp.Re_J_phi=real(Jphi);
		tmp.Im_J_phi=imag(Jphi);

		tmp.Re_J_r=real(Jr);
		tmp.Im_J_r=imag(Jr);


		tmp_file=[fname '.tmp'];
		save('-hdf5', fname, '-struct', 'tmp');
		reim={'Re_', 'Im_'};
		for I=1:3
			for J=1:2
				str=reim{J};
				d_in=[str d_names0{I} '/value']
			    d_out=[str d_names{I}]	
				system(['h5copy   -i ' fname ' -o ' tmp_file ' -s ' d_in ' -d ' d_out]);
			end
		end
		system(['h5repack -f GZIP=9  ' tmp_file ' ' fname ]);
		delete(tmp_file);
	else
		wrt_cmplx(fname, d_names{1}, Jtheta);
		wrt_cmplx(fname, d_names{2}, Jphi);
		wrt_cmplx(fname, d_names{3}, Jr);
	end
end 

function wrt_cmplx(fname, val_name, val)
		re_name=['/Re_' val_name];
		im_name=['/Im_' val_name];
		h5create(fname, re_name, size(val), 'Datatype','double', ...
			'ChunkSize', size(val),'Deflate',9);
		h5write(fname, re_name, real(val));
		h5create(fname, im_name, size(val), 'Datatype','double', ...
			'ChunkSize', size(val),'Deflate',9);
		h5write(fname, im_name, imag(val));
end


function retval = isOctave
  persistent cacheval;  % speeds up repeated calls

  if isempty (cacheval)
    cacheval = (exist ('OCTAVE_VERSION', 'builtin') > 0);
  end

  retval = cacheval;
end
