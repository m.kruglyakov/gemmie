PROGRAM FINALIZE_NESTED
    USE ISO_FORTRAN_ENV
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE LOGGER_MODULE
    USE MPI_UTILITIES_MODULE
    USE HDF5_IO_UTILITIES_MODULE
    USE HDF5_IO
    USE CTL_TYPES_MODULE
    USE RECEIVERS_UTILS_MODULE
    USE LOAD_INPUT_MODULE
    USE PROCESS_JSON
    USE INFORMATION_MODULE
    IMPLICIT NONE

    TYPE FIELDS_AT_OBS_DATA
        COMPLEX(DP), ALLOCATABLE :: E(:,:), H(:,:)
        REAL(DP), ALLOCATABLE :: X(:), Y(:), Z(:)
    ENDTYPE


    TYPE FIELDS_AT_OBS_TYPE_LIST
        TYPE(FIELDS_AT_OBS_DATA) :: F
        TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER :: next=>NULL()
    ENDTYPE

    TYPE (RUN_INPUT) :: run_data
    TYPE (FIELDS_AT_OBS_TYPE_LIST), POINTER :: fields=>NULL()
    TYPE(FIELDS_AT_OBS_DATA) :: fields_out
    REAL(DP) :: x_tmp, y_tmp, z_tmp
    COMPLEX(DP) :: E0(3), H0(3)
    TYPE(MPI_STATE_TYPE):: mpi
    CHARACTER(len=128) ::arg
    INTEGER :: N, I, J
    INTEGER, ALLOCATABLE :: I_n(:)
    LOGICAL :: success
    mpi%me=0
    mpi%comm_size=1

    CALL INITTIMER

    CALL INITILIZE_LOGGER("Interpolation for observtaion sites", .FALSE.)
    CALL LOGGER("Compatible with GEMMIE VERSION: "//GEMMIE_VERSION)
    N=COMMAND_ARGUMENT_COUNT()
    IF (N==0) THEN
        CALL LOGGER("Arguments are missing")
        CALL FINALIZE_GEMMIE()
        STOP
    ENDIF

    CALL GET_COMMAND_ARGUMENT(1, arg)
    CALL INIT_WORK_WITH_HDF5()

    ALLOCATE(CHARACTER (len=LEN_TRIM(arg))::run_data%input_file)
    run_data%input_file=TRIM(arg)
    CALL PARSE_JSON(run_data%input_file, mpi, run_data%input_data)
    CALL LOAD_RUN_INPUT(run_data, mpi, success)

    IF (run_data%solver%SPHERICAL) THEN
        CALL LOGGER("Spherical case")
        CALL LOGGER("Bilinear interpolation by FOUR nearest sites will be applied")
    ELSE
        CALL LOGGER("Cartesian case")
        CALL LOGGER("Inverse radiuses interpolation by FOUR nearest sites will be applied")
    ENDIF
    
    IF (N==1) THEN
        DO I=1, SIZE(run_data%Sources)
            CALL SHOW_SOURCE_INFO(run_data%Sources, I)
            CALL LOAD_AND_INTERPOLATE_FIELDS(run_data, I)
        ENDDO
    ELSEIF (N==3) THEN
        CALL GET_COMMAND_ARGUMENT(2, arg)
        CALL LOAD_COORDINATES_FROM_HDF5_FILE(arg, run_data%solver%SPHERICAL, &
            mpi, fields_out)
        CALL GET_COMMAND_ARGUMENT(3, arg)
        CALL CREATE_EMPTY_FILE(arg, mpi)
        CALL WRITE_STATIONS_POSITIONS_TO_FILE(arg, &
          run_data%solver%SPHERICAL, mpi,&
         fields_out%X, fields_out%Y, fields_out%Z)

        ALLOCATE(I_n(0))
        DO I=1, SIZE(run_data%Sources)
            CALL SHOW_SOURCE_INFO(run_data%Sources, I)
            CALL LOAD_FIELDS_FOR_SITE_REC(run_data, I, I_n,  mpi, fields)
            DO J=1, SIZE(fields_out%X) 
                E0=C_ZERO
                H0=C_ZERO
                IF (run_data%solver%SPHERICAL) THEN
                    CALL CALCULATE_FIELDS_AT_SITE_SPH(fields_out%X(J),&
                        fields_out%Y(J),fields_out%Z(J), fields, E0, H0)
                ELSE
                    CALL CALCULATE_FIELDS_AT_SITE_CART(fields_out%X(J),&
                        fields_out%Y(J),fields_out%Z(J), fields, E0, H0)
                ENDIF
                fields_out%E(:,J)=E0
                fields_out%H(:,J)=H0
            ENDDO
            CALL DEALLOCATE_FIELDS(fields)
            CALL STORE_PROCESSED_EM_FIELDS_TO_HDF5_FILE(arg,&
                run_data%solver%SPHERICAL, mpi,&
                fields_out%E, fields_out%H, &
                "_"//run_data%Sources(I)%output_suffix)
        ENDDO
    ELSE
        ALLOCATE(I_n(N-4))
        DO I=1, N-4
            CALL GET_COMMAND_ARGUMENT(I+1, arg)
            READ(arg, *) I_n(I)
        ENDDO
        CALL GET_COMMAND_ARGUMENT(N-2, arg)
        READ(arg, *) x_tmp
        CALL GET_COMMAND_ARGUMENT(N-1, arg)
        READ(arg, *) y_tmp
        CALL GET_COMMAND_ARGUMENT(N, arg)
        READ(arg, *) z_tmp
        DO I=1, SIZE(run_data%Sources)
            CALL SHOW_SOURCE_INFO(run_data%Sources, I)
            CALL LOAD_FIELDS_FOR_SITE_REC(run_data, I, I_n,  mpi, fields)
            E0=C_ZERO
            H0=C_ZERO
            IF (run_data%solver%SPHERICAL) THEN
                CALL CALCULATE_FIELDS_AT_SITE_SPH(x_tmp, y_tmp, z_tmp,  fields, E0, H0)
            ELSE
                CALL CALCULATE_FIELDS_AT_SITE_CART(x_tmp, y_tmp, z_tmp,  fields, E0, H0)
            ENDIF
            PRINT '(A, 7G20.11)','E:', run_data%Sources(I)%freq  , E0
            PRINT '(A, 7G20.11)','H:',  run_data%Sources(I)%freq, H0
            !PRINT *, I,  E0, H0
            CALL DEALLOCATE_FIELDS(fields)
        ENDDO
    ENDIF


    CALL FINALIZE_WORK_WITH_HDF5()
    CALL LOGGER("MAALI")

    CONTAINS
   
        RECURSIVE SUBROUTINE PRINT_FNAMES(run_data_next, mpi)
            TYPE(RUN_INPUT) , INTENT(IN) :: run_data_next
            TYPE(MPI_STATE_TYPE), INTENT(IN):: mpi
            TYPE(RUN_INPUT)  :: run_data
            INTEGER :: I_n, Ns

            Ns=SIZE(run_data_next%Sources)
            DO I=1, Ns 
                PRINT*, I,GET_OUTPUT_NAME_HDF5(run_data_next, I)
            ENDDO
            IF ( ALLOCATED(run_data_next%NIE_DATA)) THEN
                DO I_n=1, SIZE(run_data_next%NIE_DATA)
                    CALL READ_RUN_DATA_FROM_JSON(&
                        run_data_next%NIE_DATA(I_n)%NIE_input_file,&
                        run_data, mpi, .TRUE., run_data_next%solver%OUTPUT_DIR)
                    PRINT*, "Nested:", I_n
                    CALL PRINT_FNAMES(run_data, mpi)
                ENDDO
            ENDIF
        ENDSUBROUTINE 

        RECURSIVE SUBROUTINE LOAD_FIELDS_FOR_SITE_REC(run_data_prev, I, I_n,  mpi, fields)
            TYPE(RUN_INPUT) , INTENT(IN) :: run_data_prev
            INTEGER, INTENT(IN) :: I, I_n(:)
            TYPE(MPI_STATE_TYPE), INTENT(IN):: mpi
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(INOUT)::fields
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER ::p_next
            TYPE(RUN_INPUT)  :: run_data
            LOGICAL :: read_nested
            CHARACTER(len=20):: nm
            ALLOCATE(fields)
            read_nested=SIZE(I_n)>0

            IF (read_nested) THEN
                read_nested=ALLOCATED(run_data_prev%NIE_DATA) .AND. I_n(1)>0
            ENDIF

            IF (  read_nested ) THEN
                WRITE(nm, "(A, I4.4)") "_nested_", I_n(1)
                nm=ADJUSTL(nm)
                    CALL LOAD_RUN_INPUT_FOR_NESTED(run_data_prev, I_n(1), mpi, run_data)

                    CALL LOAD_FIELDS_FOR_SITE_REC(run_data, I, I_n(2:), mpi, fields%next)
                    
                    CALL LOAD_FIELDS(GET_OUTPUT_NAME_HDF5(run_data_prev, I), &
                        run_data_prev%solver%SPHERICAL, &
                        'a', TRIM(nm), mpi, &
                        fields%F%E, fields%F%H,&
                        fields%F%X, fields%F%Y, fields%F%Z)
        
            ELSE
                CALL LOAD_FIELDS(GET_OUTPUT_NAME_HDF5(run_data_prev, I), &
                    run_data_prev%solver%SPHERICAL, &
                    'an', '', mpi, &
                    fields%F%E, fields%F%H,&
                    fields%F%X, fields%F%Y, fields%F%Z)
                fields%next=>NULL()
            ENDIF
        ENDSUBROUTINE

        SUBROUTINE LOAD_COORDINATES_FROM_HDF5_FILE(fname, SPH, mpi, fields)
            CHARACTER(len=*), INTENT(IN) :: fname
            LOGICAL, INTENT(IN) :: SPH
            TYPE(MPI_STATE_TYPE), INTENT(IN):: mpi
            TYPE(FIELDS_AT_OBS_DATA), INTENT(INOUT) :: fields
                    
            CALL LOAD_FIELDS(fname, SPH, '', '', mpi, &
                fields%E, fields%H,   fields%X, fields%Y, fields%Z)
             
            ALLOCATE(fields%E(3, SIZE(fields%X)))
            ALLOCATE(fields%H(3, SIZE(fields%X)))
        ENDSUBROUTINE


        RECURSIVE SUBROUTINE CALCULATE_FIELDS_AT_SITE_CART(x, y, z,  fields, E, H)
            REAL(DP), INTENT(IN) :: x, y, z
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(IN)::fields
            COMPLEX(DP), INTENT(OUT) :: E(:), H(:)
            COMPLEX(DP) :: E_tmp(3), H_tmp(3)
            IF (.NOT. ASSOCIATED (fields)) THEN
                E=C_ZERO
                H=C_ZERO
                RETURN
            ENDIF

            CALL GET_FIELD_AT_SITE_CART_ARRAY(&
                x, y, z, fields%F%X, fields%F%Y, fields%F%Z, &
                fields%F%E, fields%F%H, E_tmp, H_tmp)
            CALL CALCULATE_FIELDS_AT_SITE_CART(x, y, z,  fields%next, E, H)
            E=E+E_tmp 
            H=H+H_tmp 
        ENDSUBROUTINE 

        RECURSIVE SUBROUTINE CALCULATE_FIELDS_AT_SITE_SPH(x, y, z,  fields, E, H)
            REAL(DP), INTENT(IN) :: x, y, z
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(IN)::fields
            COMPLEX(DP), INTENT(OUT) :: E(:), H(:)
            COMPLEX(DP) :: E_tmp(3), H_tmp(3)
            IF (.NOT. ASSOCIATED (fields)) THEN
                E=C_ZERO
                H=C_ZERO
                RETURN
            ENDIF
            CALL GET_FIELD_AT_SITE_SPH_ARRAY(&
                x, y, z, fields%F%X, fields%F%Y, fields%F%Z, &
                fields%F%E, fields%F%H, E_tmp, H_tmp)
            CALL CALCULATE_FIELDS_AT_SITE_SPH(x, y, z,  fields%next, E, H)
            E=E+E_tmp 
            H=H+H_tmp 
        ENDSUBROUTINE 

        
        RECURSIVE SUBROUTINE DEALLOCATE_FIELDS(fields)
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(INOUT)::fields
            IF (.NOT. ASSOCIATED (fields)) RETURN
            CALL DEALLOCATE_FIELDS(fields%next)
            DEALLOCATE(fields%F%E)
            DEALLOCATE(fields%F%H)
            DEALLOCATE(fields%F%X)
            DEALLOCATE(fields%F%Y)
            DEALLOCATE(fields%F%Z)
            fields=>NULL()
        ENDSUBROUTINE

        SUBROUTINE LOAD_AND_INTERPOLATE_FIELDS(run_data, I_src)
            TYPE(RUN_INPUT) , INTENT(IN) :: run_data
            INTEGER, INTENT(IN) :: I_src
            TYPE(MPI_STATE_TYPE) :: mpi
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER::fields_lst
            fields_lst=>NULL()
            CALL LOAD_AND_INTERPOLATE_FIELDS_REC(run_data, I_src,  mpi, fields_lst)
        ENDSUBROUTINE


        RECURSIVE SUBROUTINE LOAD_AND_INTERPOLATE_FIELDS_REC(run_data_prev, I,  mpi, fields_lst)
            TYPE(RUN_INPUT) , INTENT(IN) :: run_data_prev
            INTEGER, INTENT(IN) :: I
            TYPE(MPI_STATE_TYPE), INTENT(IN):: mpi
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(INOUT)::fields_lst
            TYPE(RUN_INPUT)  :: run_data
            TYPE(FIELDS_AT_OBS_DATA) :: fields
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER :: cur_fields
            CHARACTER(len=20):: nm
            INTEGER :: I_n
            IF ( ALLOCATED(run_data_prev%NIE_DATA)  ) THEN
                ALLOCATE(cur_fields)
                cur_fields%next=>fields_lst

                DO I_n=1, SIZE(run_data_prev%NIE_DATA)
                    CALL LOAD_RUN_INPUT_FOR_NESTED(run_data_prev, I_n, mpi, run_data)

                    WRITE(nm, "(A, I4.4)") "_nested_", I_n
                    nm=ADJUSTL(nm)

                    CALL LOAD_FIELDS(GET_OUTPUT_NAME_HDF5(run_data_prev, I), &
                        run_data_prev%solver%SPHERICAL, &
                        'a', TRIM(nm), mpi, &
                        cur_fields%F%E, cur_fields%F%H,&
                        cur_fields%F%X, cur_fields%F%Y, cur_fields%F%Z)


                    CALL LOAD_AND_INTERPOLATE_FIELDS_REC(run_data, I, mpi, cur_fields )

                ENDDO
                DEALLOCATE(cur_fields)
            ELSE
                CALL LOAD_FIELDS(GET_OUTPUT_NAME_HDF5(run_data_prev, I), &
                    run_data_prev%solver%SPHERICAL, &
                    'an', '', mpi, &
                    fields%E, fields%H,&
                    fields%X, fields%Y, fields%Z)
                IF (run_data_prev%solver%SPHERICAL) THEN
                    CALL CALCULATE_ALL_FIELDS_REC_SPH(fields_lst, fields)
                ELSE
                    CALL CALCULATE_ALL_FIELDS_REC_CART(fields_lst, fields)
                ENDIF
                CALL STORE_PROCESSED_EM_FIELDS_HDF5(run_data_prev, I, mpi, fields%E, fields%H ,'')
            ENDIF
        ENDSUBROUTINE

        RECURSIVE SUBROUTINE CALCULATE_ALL_FIELDS_REC_CART(coarse_fields, fields_out)
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(IN)::coarse_fields
            TYPE(FIELDS_AT_OBS_DATA), INTENT(INOUT) :: fields_out
            INTEGER :: I
            COMPLEX(DP) :: E0(3), H0(3)
            IF (.NOT. ASSOCIATED(coarse_fields)) RETURN
            DO I=1, SIZE(fields_out%X)
                CALL GET_FIELD_AT_SITE_CART_ARRAY(&
                    fields_out%X(I),&
                    fields_out%Y(I),&
                    fields_out%Z(I),&
                    coarse_fields%F%X, &
                    coarse_fields%F%Y, &
                    coarse_fields%F%Z, &
                    coarse_fields%F%E, coarse_fields%F%H, E0, H0)
                fields_out%E(:,I)=fields_out%E(:,I)+E0 
                fields_out%H(:,I)=fields_out%H(:,I)+H0 
            ENDDO
            CALL CALCULATE_ALL_FIELDS_REC_CART(coarse_fields%next, fields_out)
        ENDSUBROUTINE

        RECURSIVE SUBROUTINE CALCULATE_ALL_FIELDS_REC_SPH(coarse_fields, fields_out)
            TYPE(FIELDS_AT_OBS_TYPE_LIST), POINTER, INTENT(IN)::coarse_fields
            TYPE(FIELDS_AT_OBS_DATA), INTENT(INOUT) :: fields_out
            INTEGER :: I
            COMPLEX(DP) :: E0(3), H0(3)
            IF (.NOT. ASSOCIATED(coarse_fields)) RETURN

            DO I=1, SIZE(fields_out%X)
                CALL GET_FIELD_AT_SITE_SPH_ARRAY(&
                    fields_out%X(I),&
                    fields_out%Y(I),&
                    fields_out%Z(I),&
                    coarse_fields%F%X, &
                    coarse_fields%F%Y, &
                    coarse_fields%F%Z, &
                    coarse_fields%F%E, coarse_fields%F%H, E0, H0)
                fields_out%E(:,I)=fields_out%E(:,I)+E0 
                fields_out%H(:,I)=fields_out%H(:,I)+H0
            ENDDO
            CALL CALCULATE_ALL_FIELDS_REC_SPH(coarse_fields%next, fields_out)
        ENDSUBROUTINE

ENDPROGRAM
