MODULE SPLINES_M
    USE ISO_C_BINDING
    IMPLICIT NONE
    PRIVATE

    INTEGER, PARAMETER :: SP  = SELECTED_REAL_KIND(5)
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(15)

    COMPLEX(DP), PARAMETER :: C_ZERO =(0.0_DP,0.0_DP) 
    COMPLEX(DP), PARAMETER :: C_ONE=(1.0_DP,0.0_DP) 
    
    REAL(DP), PARAMETER :: R_ZERO =0.0_DP
    REAL(DP), PARAMETER :: R_ONE=1.0_DP

    INTEGER, PARAMETER :: TWO = 2

    INTEGER, PARAMETER :: N_band=7

    INTERFACE GET_SPLINE_COEFFS
        MODULE PROCEDURE SPLINE_COEFFS_CMPLX
        MODULE PROCEDURE SPLINE_COEFFS_REAL
    ENDINTERFACE  

    INTERFACE APPLY_BOTH_PRECONDS
        MODULE PROCEDURE APPLY_BOTH_PRECONDS_CMPLX
        MODULE PROCEDURE APPLY_BOTH_PRECONDS_REAL
    ENDINTERFACE    

    INTERFACE PREPARE_MATRIX_FOR_SPLINE
        MODULE PROCEDURE PREPARE_MATRIX_FOR_SPLINE_CMPLX
        MODULE PROCEDURE PREPARE_MATRIX_FOR_SPLINE_REAL
    ENDINTERFACE

    INTERFACE BINARY_SEARCH
        MODULE PROCEDURE BINARY_SEARCH_PART
        MODULE PROCEDURE BINARY_SEARCH_FULL
    ENDINTERFACE

    INTERFACE POLY_3
        MODULE PROCEDURE POLY3_1
        MODULE PROCEDURE POLY3_2
        MODULE PROCEDURE POLY3_2_CMPLX
    ENDINTERFACE

    PUBLIC :: GET_SPLINE_COEFFS
    PUBLIC :: BINARY_SEARCH, POLY_3
CONTAINS

    SUBROUTINE SPLINE_COEFFS_CMPLX(XI, YI, N_int, C)
        REAL(DP), INTENT(IN):: XI(0:)        

        COMPLEX(DP), INTENT(IN):: YI(0:,:) 
        INTEGER, INTENT(IN) :: N_int       

        COMPLEX(DP), INTENT(OUT):: C(1:N_int, 0:3, SIZE(YI,2))    

        COMPLEX(DP), ALLOCATABLE  ::AS(:,:)
        COMPLEX(DP), ALLOCATABLE  ::BS(:,:)               
        REAL(DP), ALLOCATABLE :: HI(:)
        COMPLEX(DP) :: q
        INTEGER ::N, M                              
        INTEGER ::I 
        
        INTEGER, ALLOCATABLE :: IPIV2(:)
        INTEGER ::INFO

        N=SIZE(XI)-1
        M=SIZE(YI,2)
        ALLOCATE(HI(N))
        DO I=1,N
            HI(I)=XI(I)-XI(I-1)
        ENDDO
        ALLOCATE(AS(N_band,0:N))

        CALL PREPARE_MATRIX_FOR_SPLINE(HI, AS)

        ALLOCATE(BS(0:N, SIZE(YI,2))) 
        q=HI(1)/HI(2)        
        q=q**2
        BS(0,:)=2.0_DP*( (YI(1,:)-YI(0,:) )/HI(1)-(YI(2,:)-YI(1,:))/HI(2)*q)
        DO I=1, N-1
            q=HI(I+1)/HI(I)        
            q=q**2
            BS(I,:)=3.0_DP*((YI(I,:)-YI(I-1,:))*q+ (YI(I+1,:)-YI(I,:)))
        ENDDO
        q=HI(N)/HI(N-1)
        q=q**2        
        BS(N,:)=2.0_DP*( (YI(N-1,:)-YI(N-2,:) )/HI(N-1)*q-(YI(N,:)-YI(N-1,:))/HI(N))

        ALLOCATE(IPIV2(SIZE(BS)))
        CALL ZGBSV(N+1, TWO, TWO, M, AS, N_band, IPIV2, BS, N+1, INFO)

        if (info /= 0) THEN
            PRINT*, "Problem with SLAE for spline coefficients", info
            STOP 22
        ENDIF

        DO I=1, N_int
            q=HI(I)/HI(MIN(I+1, N))
            C(I, 0, : ) = YI(I-1,:)
            C(I, 1, : ) = BS(I-1,:)
            C(I, 2, : ) = (-(BS(I,:)*q+2.0_DP*BS(I-1,:))+3.0_DP*YI(I,:)-3.0_DP*YI(I-1,:))
            C(I, 3, : ) = ((BS(I,:)*q+BS(I-1,:))-2.0_DP*YI(I,:)+2.0_DP*YI(I-1,:))
        ENDDO

        DEALLOCATE(BS)
        DEALLOCATE(AS)
        DEALLOCATE(HI)
    ENDSUBROUTINE

    SUBROUTINE SPLINE_COEFFS_REAL(XI, YI, N_int, C, alpha)
        REAL(DP), INTENT(IN):: XI(0:)        

        REAL(DP), INTENT(IN):: YI(0:,:) 
        INTEGER, INTENT(IN) :: N_int       

        REAL(DP), INTENT(OUT):: C(1:N_int, 0:3, SIZE(YI,2))    
        REAL(DP), OPTIONAL, INTENT(IN) :: alpha

        REAL(DP), ALLOCATABLE  ::AS(:,:)
        REAL(DP), ALLOCATABLE  ::BS(:,:)               
        REAL(DP), ALLOCATABLE :: HI(:)
        REAL(DP) :: q
        INTEGER ::N, M                              
        INTEGER ::I 
        
        INTEGER, ALLOCATABLE :: IPIV2(:)
        INTEGER ::INFO

        N=SIZE(XI)-1
        M=SIZE(YI,2)
        ALLOCATE(HI(N))
        DO I=1,N
            HI(I)=XI(I)-XI(I-1)
        ENDDO
        ALLOCATE(AS(N_band,0:N))

        CALL PREPARE_MATRIX_FOR_SPLINE(HI, AS)
        IF (PRESENT(alpha)) AS(5,:)=AS(5,:)+alpha

        ALLOCATE(BS(0:N, SIZE(YI,2))) 
        q=HI(1)/HI(2)        
        q=q**2
        BS(0,:)=2.0_DP*( (YI(1,:)-YI(0,:) )/HI(1)-(YI(2,:)-YI(1,:))/HI(2)*q)
        DO I=1, N-1
            q=HI(I+1)/HI(I)        
            q=q**2
            BS(I,:)=3.0_DP*((YI(I,:)-YI(I-1,:))*q+ (YI(I+1,:)-YI(I,:)))
        ENDDO
        q=HI(N)/HI(N-1)
        q=q**2        
        BS(N,:)=2.0_DP*( (YI(N-1,:)-YI(N-2,:) )/HI(N-1)*q-(YI(N,:)-YI(N-1,:))/HI(N))

        ALLOCATE(IPIV2(SIZE(BS)))
        CALL DGBSV(N+1, TWO, TWO, M, AS, N_band, IPIV2, BS, N+1, INFO)

        if (info /= 0) THEN
            PRINT*, "Problem with SLAE for spline coefficients", info
            STOP 22
        ENDIF

        DO I=1, N_int
            q=HI(I)/HI(MIN(I+1, N))
            C(I, 0, : ) = YI(I-1,:)
            C(I, 1, : ) = BS(I-1,:)
            C(I, 2, : ) = (-(BS(I,:)*q+2.0_DP*BS(I-1,:))+3.0_DP*YI(I,:)-3.0_DP*YI(I-1,:))
            C(I, 3, : ) = ((BS(I,:)*q+BS(I-1,:))-2.0_DP*YI(I,:)+2.0_DP*YI(I-1,:))
        ENDDO

        DEALLOCATE(BS)
        DEALLOCATE(AS)
        DEALLOCATE(HI)
    ENDSUBROUTINE

    SUBROUTINE PREPARE_MATRIX_FOR_SPLINE_CMPLX(HI, AS)
        REAL(DP), INTENT(IN):: HI(:)
        COMPLEX(DP), INTENT(OUT)  :: AS(1:, 0: )
        INTEGER ::  I, N 
        N=SIZE(HI)

        AS=C_ZERO

        AS(5, 0)=C_ONE/HI(1)**2
        AS(6, 0)=C_ONE/HI(1)

        AS(4, 1)=(C_ONE/HI(1)**2-C_ONE/HI(2)**2)
        AS(5, 1)=2.0_DP*(C_ONE/HI(1)+C_ONE/HI(2))
        AS(6, 1)=C_ONE/HI(2)

        AS(3, 2)=-C_ONE/HI(2)**2
        AS(4, 2)=C_ONE/HI(2)
        AS(5, 2)=2.0_DP*(C_ONE/HI(2)+C_ONE/HI(3))
        AS(6, 2)=C_ONE/HI(3)

        DO I=3, N-3

            AS(4,I) =C_ONE/HI(I)
            AS(5,I) =2.0_DP*(C_ONE/HI(I)+C_ONE/HI(I+1))
            AS(6,I) =C_ONE/HI(I+1)

        ENDDO
        IF (N>3) THEN
            AS(4, N-2) =C_ONE/HI(N-2)
        ENDIF
        AS(5, N-2)=2.0_DP*(C_ONE/HI(N-2)+C_ONE/HI(N-1))
        AS(6, N-2)=C_ONE/HI(N-1)
        AS(7, N-2)=C_ONE/HI(N-1)**2

        AS(4, N-1) =C_ONE/HI(N-1)
        AS(5, N-1)=2.0_DP*(C_ONE/HI(N-1)+C_ONE/HI(N))
        AS(6, N-1)=C_ONE/HI(N-1)**2-C_ONE/HI(N)**2

        AS(4, N)=C_ONE/HI(N)
        AS(5, N)=-C_ONE/HI(N)**2
        
        CALL APPLY_BOTH_PRECONDS(HI, AS)

    ENDSUBROUTINE

    SUBROUTINE PREPARE_MATRIX_FOR_SPLINE_REAL(HI, AS)
        REAL(DP), INTENT(IN):: HI(:)
        REAL(DP), INTENT(OUT)  :: AS(1:, 0: )
        INTEGER ::  I, N 
        N=SIZE(HI)

        AS=C_ZERO

        AS(5, 0)=R_ONE/HI(1)**2
        AS(6, 0)=R_ONE/HI(1)

        AS(4, 1)=(R_ONE/HI(1)**2-R_ONE/HI(2)**2)
        AS(5, 1)=2.0_DP*(R_ONE/HI(1)+R_ONE/HI(2))
        AS(6, 1)=R_ONE/HI(2)

        AS(3, 2)=-R_ONE/HI(2)**2
        AS(4, 2)=R_ONE/HI(2)
        AS(5, 2)=2.0_DP*(R_ONE/HI(2)+R_ONE/HI(3))
        AS(6, 2)=R_ONE/HI(3)

        DO I=3, N-3

            AS(4,I) =R_ONE/HI(I)
            AS(5,I) =2.0_DP*(R_ONE/HI(I)+R_ONE/HI(I+1))
            AS(6,I) =R_ONE/HI(I+1)

        ENDDO
        IF (N>3) THEN
            AS(4, N-2) =R_ONE/HI(N-2)
        ENDIF
        AS(5, N-2)=2.0_DP*(R_ONE/HI(N-2)+R_ONE/HI(N-1))
        AS(6, N-2)=R_ONE/HI(N-1)
        AS(7, N-2)=R_ONE/HI(N-1)**2

        AS(4, N-1) =R_ONE/HI(N-1)
        AS(5, N-1)=2.0_DP*(R_ONE/HI(N-1)+R_ONE/HI(N))
        AS(6, N-1)=R_ONE/HI(N-1)**2-R_ONE/HI(N)**2

        AS(4, N)=R_ONE/HI(N)
        AS(5, N)=-R_ONE/HI(N)**2
        
        CALL APPLY_BOTH_PRECONDS(HI, AS)

    ENDSUBROUTINE

    SUBROUTINE APPLY_BOTH_PRECONDS_CMPLX(HI, AS)
        REAL(DP), INTENT(IN):: HI(:)
        COMPLEX(DP), INTENT(OUT)  :: AS(1:, 0: )
        INTEGER ::  I, N, J, I1, I2, J2
        N=SIZE(HI)

        DO J=1, N+1
            DO I=MAX(1, J-2), MIN(N+1, J+2)
                I1=5+I-J
                I2=MIN(I, N)
                J2=MIN(J, N)
                AS(I1, J-1)=AS(I1, J-1)*HI(I2)**2/HI(J2)
            ENDDO
        ENDDO

    ENDSUBROUTINE

    SUBROUTINE APPLY_BOTH_PRECONDS_REAL(HI, AS)
        REAL(DP), INTENT(IN):: HI(:)
        REAL(DP), INTENT(OUT)  :: AS(1:, 0: )
        INTEGER ::  I, N, J, I1, I2, J2
        N=SIZE(HI)

        DO J=1, N+1
            DO I=MAX(1, J-2), MIN(N+1, J+2)
                I1=5+I-J
                I2=MIN(I, N)
                J2=MIN(J, N)
                AS(I1, J-1)=AS(I1, J-1)*HI(I2)**2/HI(J2)
            ENDDO
        ENDDO

    ENDSUBROUTINE

    FUNCTION POLY3_1(X, X0, C) RESULT(RES)
        REAL(DP), INTENT(IN):: X, X0      
        REAL(DP), INTENT(IN):: C(0:)  
        REAL(DP) :: RES
        REAL(DP) ::  DX
        DX=X-X0
        RES=((C(3)*DX+C(2))*DX+C(1))*DX+C(0)
    ENDFUNCTION

    FUNCTION POLY3_2(X, X0, C) RESULT(RES)
        REAL(DP), INTENT(IN):: X, X0(2)    
        REAL(DP), INTENT(IN):: C(0:)  
        REAL(DP) :: RES
        REAL(DP) ::  DX
        DX=(X-X0(1))/(X0(2)-X0(1))
        RES=((C(3)*DX+C(2))*DX+C(1))*DX+C(0)
    ENDFUNCTION
    
    FUNCTION POLY3_2_CMPLX(X, X0, C) RESULT(RES)
        REAL(DP), INTENT(IN):: X, X0(2)    
        COMPLEX(DP), INTENT(IN):: C(0:)  
        COMPLEX(DP) :: RES
        REAL(DP) ::  DX
        DX=(X-X0(1))/(X0(2)-X0(1))
        RES=((C(3)*DX+C(2))*DX+C(1))*DX+C(0)
    ENDFUNCTION

     FUNCTION BINARY_SEARCH_PART(X, XI, I_MIN) RESULT(IP)
        REAL(DP), INTENT(IN) :: X, XI(:)
        INTEGER, INTENT(IN) :: I_MIN
        INTEGER :: IP
        IF (I_MIN >= 1 .AND. I_MIN <= SIZE(XI)-1) THEN
            IP = BINARY_SEARCH(X, XI(I_MIN:)) + I_MIN - 1
        ELSE
            IP = BINARY_SEARCH(X, XI)
        ENDIF
    ENDFUNCTION


     FUNCTION BINARY_SEARCH_FULL(X, XI) RESULT(I1)
        REAL(DP), INTENT(IN) :: X            
        REAL(DP), INTENT(IN) :: XI(:)        
        INTEGER :: I1
        INTEGER N                            
        INTEGER I2, IC

        N = SIZE(XI)
        I1 = 1
        IF (N < 2) THEN
            PRINT*, "SHORT ARRAY"
            STOP 25
        ELSEIF (N == 2) THEN
            I1 = 1
        ELSEIF (N == 3) THEN
            IF (X <= XI(2)) THEN 
                I1 = 1
            ELSE
                I1 = 2
            ENDIF
        ELSEIF (X <= XI(1)) THEN 
            I1 = 1
        ELSEIF (X <= XI(2)) THEN 
            I1 = 1
        ELSEIF (X <= XI(3)) THEN 
            I1 = 2
        ELSEIF (X >= XI(N)) THEN 
            I1 = N-1
        ELSE
            I1 = 3; I2 = N
            DO
                IF (I2 - I1 == 1) EXIT
                IC = I1 + (I2 - I1)/2
                IF (X >= XI(IC)) THEN
                    I1 = IC
                ELSE
                    I2 = IC
                ENDIF
            ENDDO
        ENDIF
    ENDFUNCTION
ENDMODULE
