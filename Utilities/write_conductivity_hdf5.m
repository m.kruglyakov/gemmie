%function write_conductivity_hdf5(siga, fname)
%siga should be array of following shape siga(Nr, Ntheta, Nphi) in spherical case
%siga(Nz, Nx, Ny) in Cartesian case
function write_conductivity_hdf5(siga, fname)
	if exist(fname, 'file')
		delete(fname);
	end
	if isOctave
		tmp=struct('Cond',siga);
		tmp_file=[fname '.tmp'];
		save('-hdf5', fname, '-struct', 'tmp');
		system(['h5copy   -i ' fname ' -o ' tmp_file ' -s Cond/value -d  Conductivity']);
		system(['h5repack -f GZIP=9  ' tmp_file ' ' fname ]);
		delete(tmp_file);
	else
		h5create(fname,'/Conductivity', size(siga), 'Datatype','double', ...
			'ChunkSize', size(siga),'Deflate',9);
		h5write(fname, '/Conductivity', siga);
	end
end 

function retval = isOctave
  persistent cacheval;  % speeds up repeated calls

  if isempty (cacheval)
    cacheval = (exist ('OCTAVE_VERSION', 'builtin') > 0);
  end

  retval = cacheval;
end
