ELEMENTAL FUNCTION GET_Q_F_COEFFS_LARGE_N_TP_2(z,n) RESULT(RES)
 
        COMPLEX(DP),INTENT(IN)::z
        INTEGER,INTENT(IN)::n
        COMPLEX(DP)::RES
!        INCLUDE 'include/Radial/Single/G_SINGLE_LARGE_N_2.F90'
        COMPLEX(DP)::COEFFS(0:COEFF_N_2)
        COMPLEX(DP)::U,U2
        REAL(DP)::T,A,B
        U=z/n
        U2=U*U
        T=R_ONE/n
        INCLUDE 'include/Radial/Single/call_G_SINGLE_LARGE_N_func_2.F90'
        RES=COMPUTE_POLYNOMIAL(COEFFS,U2)
!        RES=RES*z**(Z_POW_2_FUNC-2)
    ENDFUNCTION

ELEMENTAL FUNCTION GET_Q_DER_COEFFS_LARGE_N_TP_2(z,n) RESULT(RES)
 
        COMPLEX(DP),INTENT(IN)::z
        INTEGER,INTENT(IN)::n
        COMPLEX(DP)::RES
!        INCLUDE 'include/Radial/Single/G_SINGLE_LARGE_N_2.F90'
        COMPLEX(DP)::COEFFS(0:COEFF_N_2)
        COMPLEX(DP)::U,U2
        REAL(DP)::T,A,B
        U=z/n
        U2=U*U
        T=R_ONE/n
        INCLUDE 'include/Radial/Single/call_G_SINGLE_LARGE_N_der_2.F90'
        RES=COMPUTE_POLYNOMIAL(COEFFS,U2)
!        RES=RES*z**(Z_POW_2_DER-3)
    ENDFUNCTION
