    PURE FUNCTION INTEGRATION_FOR_SMALL_SN_G_IE_RR_HERMIT(Layer,n) RESULT(RES)
 
            TYPE (OPERATOR_LAYER_TYPE),INTENT(IN)::Layer
            INTEGER,INTENT(IN)::n
            COMPLEX(DP)::RES
            COMPLEX(DP)::QQ(12),W(12),U2,V,ss
            COMPLEX(DP)::HJ1,HJ2,DH1,DH2,DJ1,DJ2
            ASSOCIATE(    z1=>Layer%Spherical_Layer%z1,&
                    &z2=>Layer%Spherical_Layer%z2,&
                    &r=>Layer%Spherical_Layer%r2,&
                    &s=>Layer%Spherical_Layer%s,&
                    &sn=>Layer%sn,&
                    &Rj1=>Layer%Spherical_Layer%Rj1(n),&
                    &Rh1=>Layer%Spherical_Layer%Rh1(n),&
                    &Rj2=>Layer%Spherical_Layer%Rj2(n),&
                    &Rh2=>Layer%Spherical_Layer%Rh2(n),&
                    &kappa_j=>Layer%kappa_j(n),&
                    &k2=>Layer%Spherical_Layer%k2&
                &)
                U2=s*z2*s*z2
                V=s*n
                ss=s

                DJ1=Rj1*z1*z1
                DJ2=Rj2*z2*z2
            

                DH1=Rh1
                DH2=Rh2

                HJ1=MULT_HJ_M(Rj1,Rh1,z1)
                HJ2=MULT_HJ_M(Rj2,Rh2,z2)



                W(1)=HJ1*s**S_POW_IE_RR_1/sn(S1_POW_IE_RR_1)

                W(2)=HJ1*DH1*s**S_POW_IE_RR_2/sn(S1_POW_IE_RR_2)

                W(3)=HJ1*DJ1*s**S_POW_IE_RR_3/sn(S1_POW_IE_RR_3)

                W(4)=HJ1*DJ1*DH1*s**S_POW_IE_RR_4/sn(S1_POW_IE_RR_4)

                W(5)=HJ2*s**S_POW_IE_RR_5/sn(S1_POW_IE_RR_5)

                W(6)=HJ2*kappa_j*s**S_POW_IE_RR_6/sn(S1_POW_IE_RR_6)

                W(7)=HJ2*kappa_j*DJ1*s**S_POW_IE_RR_7/sn(S1_POW_IE_RR_7)

                W(8)=HJ2*DH2*s**S_POW_IE_RR_8/sn(S1_POW_IE_RR_8)

                W(9)=HJ2*DH2*kappa_j*s**S_POW_IE_RR_9/sn(S1_POW_IE_RR_9)

                W(10)=HJ2*DH2*kappa_j*DJ1*s**S_POW_IE_RR_10/sn(S1_POW_IE_RR_10)

                W(11)=HJ2*DJ2*s**S_POW_IE_RR_11/sn(S1_POW_IE_RR_11)

                W(12)=HJ2*DH2*DJ2*s**S_POW_IE_RR_12/sn(S1_POW_IE_RR_12)


                QQ(1)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A1,ss,V,U2)
                QQ(2)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A2,ss,V,U2)
                QQ(3)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A3,ss,V,U2)
                QQ(4)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A4,ss,V,U2)
                QQ(5)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A5,ss,V,U2)
                QQ(6)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A6,ss,V,U2)
                QQ(7)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A7,ss,V,U2)
                QQ(8)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A8,ss,V,U2)
                QQ(9)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A9,ss,V,U2)
                QQ(10)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A10,ss,V,U2)
                QQ(11)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A11,ss,V,U2)
                QQ(12)=COMPUTE_POLYNOMIAL(EXPAND_S_IE_RR_A12,ss,V,U2)

                RES=SUM(W*QQ)*r**Z_POW_IE_RR_1/C_IONE
                
            END ASSOCIATE
    ENDFUNCTION
