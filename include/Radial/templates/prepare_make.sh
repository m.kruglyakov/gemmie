#!/bin/sh


T_PATH=include/Radial/templates
R_PATH=include/Radial

for NM in IE_LT IE_LP IE_LR IE_RR
do
#	echo  $NM
	echo $R_PATH"/Double/DOUBLE_SMALL_KR_G_"$NM"_INC.F90":  $T_PATH"/template_double_large_n"
	echo -e "\t" 	sed \"s/#NAME#/$NM/g\" '$< >$@'
#	echo -e "\t" 	sed \"s/#NAME#/$NM/g\" $T_PATH"/template_double_large_n" '>' $R_PATH"/Double/DOUBLE_SMALL_KR_G_"$NM"_INC.F90"


	echo $R_PATH"/Double/DOUBLE_SMALL_SN_G_"$NM"_HERMIT_INC.F90":  $T_PATH"/template_double_small_sn2"
	echo -e "\t" 	sed \"s/#NAME#/$NM/g\" '$< >$@'
#	echo -e "\t" 	sed \"s/#NAME#/$NM/g\" $T_PATH"/template_double_small_sn2" '>' $R_PATH"/Double/DOUBLE_SMALL_SN_G_"$NM"_HERMIT_INC.F90"
done


for K in 1 2 3 4 5
do
	echo $R_PATH"/Single/SINGLE_SMALL_SN_"$K"_INC.F90" :  $T_PATH"/template_single_small_sn"
	echo -e "\t" sed \"s/#N#/$K/g\" '$< > $@'

	echo $R_PATH"/Single/SINGLE_LARGE_N_"$K"_INC.F90" :  $T_PATH"/template_single_large_n"
	echo -e "\t" sed \"s/#N#/$K/g\" '$< > $@'

done
