!\eqref{ coeffs_der_theta_DJ } 
B = smu*snu*stheta
DA(PolS) = cmu*cnu*ctheta*stheta
DA(PolD) = cmu*cnu*ctheta
DB(PolS) = ctheta*smu*snu*stheta
DB(PolD) = ctheta*smu*snu
DXI(PolS) = -stheta2
DXI(PolD) = -stheta
