PURE FUNCTION  FINALIZE_G_H_THETA_THETA_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_H) RESULT( G_H_TT ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_H(:, :, :, RC_H_LP:) 
    COMPLEX(DP) :: G_H_TT(SIZE(DER12_F_H,1), SIZE(DER12_F_H,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_H,2) 
        DO I=1,  SIZE(DER12_F_H,1) 
            G_H_TT(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_H(I,Ir,1,RC_H_LP)&
                &*sin(phi_r-phi_s(I))-DER12_F_H(I,Ir,2,RC_H_LT)*sin(phi_r-phi_s(I))*sin(theta_r)**2)&
                &+sin(theta_s(I))*(DER12_F_H(I,Ir,2,RC_H_LT)*cos(phi_r-phi_s(I))*sin(phi_r-phi_s(I))&
                &*cos(theta_r)*sin(theta_r)+DER12_F_H(I,Ir,2,RC_H_LP)&
                &*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(phi_r-phi_s(I))&
                &*sin(theta_r)-DER12_F_H(I,Ir,2,RC_H_LP)*sin(theta_s(I))*sin(phi_r-phi_s(I))&
                &*cos(theta_r))+DER12_F_H(I,Ir,1,RC_H_LT)*sin(phi_r-phi_s(I))*cos(theta_r)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_THETA_PHI_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_H) RESULT( G_H_TP ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_H(:, :, :, RC_H_LP:) 
    COMPLEX(DP) :: G_H_TP(SIZE(DER12_F_H,1), SIZE(DER12_F_H,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_H,2) 
        DO I=1,  SIZE(DER12_F_H,1) 
            G_H_TP(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_H(I,Ir,2,RC_H_LT)*cos(theta_s(I))*cos(phi_r-phi_s(I))&
                &*sin(theta_r)**2-DER12_F_H(I,Ir,1,RC_H_LT)*cos(phi_r-phi_s(I))&
                &*cos(theta_r))+sin(theta_s(I))*(cos(theta_s(I))*((-DER12_F_H(I,Ir,2,RC_H_LT)&
                &*cos(phi_r-phi_s(I))**2)-DER12_F_H(I,Ir,2,RC_H_LT))&
                &*cos(theta_r)*sin(theta_r)+(DER12_F_H(I,Ir,2,RC_H_LP)*sin(phi_r-phi_s(I))&
                &**2-DER12_F_H(I,Ir,1,RC_H_LT))*sin(theta_r)&
                &+DER12_F_H(I,Ir,2,RC_H_LT)*sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r)**2)-DER12_F_H(I,Ir,1,RC_H_LP)&
                &*cos(phi_r-phi_s(I))
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_THETA_R_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_H_LR) RESULT( G_H_TR ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_H_LR(:, :) 
    COMPLEX(DP) :: G_H_TR(SIZE(DER_H_LR,1), SIZE(DER_H_LR,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_H_LR,2) 
        DO I=1,  SIZE(DER_H_LR,1) 
            G_H_TR(I,Ir) = -DER_H_LR(I,Ir)*sin(theta_s(I))&
                &*sin(phi_r-phi_s(I))
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_PHI_THETA_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_H) RESULT( G_H_PT ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_H(:, :, :, RC_H_LP:) 
    COMPLEX(DP) :: G_H_PT(SIZE(DER12_F_H,1), SIZE(DER12_F_H,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_H,2) 
        DO I=1,  SIZE(DER12_F_H,1) 
            G_H_PT(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_H(I,Ir,1,RC_H_LP)&
                &*cos(phi_r-phi_s(I))*cos(theta_r)-DER12_F_H(I,Ir,2,RC_H_LP)*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(theta_r)*&
                &*2)+sin(theta_s(I))*(cos(theta_s(I))*(DER12_F_H(I,Ir,2,RC_H_LP)*cos(phi_r-phi_s(I))&
                &**2+DER12_F_H(I,Ir,2,RC_H_LP))&
                &*cos(theta_r)*sin(theta_r)+(DER12_F_H(I,Ir,1,RC_H_LP)-DER12_F_H(I,Ir,2,RC_H_LT)*sin(phi_r-phi_s(I))&
                &**2)&
                &*sin(theta_r)-DER12_F_H(I,Ir,2,RC_H_LP)*sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r)**2)+&
                DER12_F_H(I,Ir,1,RC_H_LT)*cos(phi_r-phi_s(I))
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_PHI_PHI_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_H) RESULT( G_H_PP ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_H(:, :, :, RC_H_LP:) 
    COMPLEX(DP) :: G_H_PP(SIZE(DER12_F_H,1), SIZE(DER12_F_H,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_H,2) 
        DO I=1,  SIZE(DER12_F_H,1) 
            G_H_PP(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_H(I,Ir,1,RC_H_LT)&
                &*sin(phi_r-phi_s(I))-DER12_F_H(I,Ir,2,RC_H_LP)*sin(phi_r-phi_s(I))*sin(theta_r)**2)&
                &+sin(theta_s(I))*(DER12_F_H(I,Ir,2,RC_H_LP)*cos(phi_r-phi_s(I))*sin(phi_r-phi_s(I))&
                &*cos(theta_r)*sin(theta_r)+DER12_F_H(I,Ir,2,RC_H_LT)&
                &*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(phi_r-phi_s(I))&
                &*sin(theta_r)-DER12_F_H(I,Ir,2,RC_H_LT)*sin(theta_s(I))*sin(phi_r-phi_s(I))&
                &*cos(theta_r))+DER12_F_H(I,Ir,1,RC_H_LP)*sin(phi_r-phi_s(I))*cos(theta_r)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_PHI_R_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_H_LR) RESULT( G_H_PR ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_H_LR(:, :) 
    COMPLEX(DP) :: G_H_PR(SIZE(DER_H_LR,1), SIZE(DER_H_LR,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_H_LR,2) 
        DO I=1,  SIZE(DER_H_LR,1) 
            G_H_PR(I,Ir) = DER_H_LR(I,Ir)&
                &*cos(theta_s(I))*sin(theta_r)-DER_H_LR(I,Ir)*sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_R_THETA_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_H_RL) RESULT( G_H_RT ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_H_RL(:, :) 
    COMPLEX(DP) :: G_H_RT(SIZE(DER_H_RL,1), SIZE(DER_H_RL,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_H_RL,2) 
        DO I=1,  SIZE(DER_H_RL,1) 
            G_H_RT(I,Ir) = DER_H_RL(I,Ir)*sin(phi_r-phi_s(I))*sin(theta_r)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_H_R_PHI_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_H_RL) RESULT( G_H_RP ) 
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_H_RL(:, :) 
    COMPLEX(DP) :: G_H_RP(SIZE(DER_H_RL,1), SIZE(DER_H_RL,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_H_RL,2) 
        DO I=1,  SIZE(DER_H_RL,1) 
            G_H_RP(I,Ir) = DER_H_RL(I,Ir)&
                &*sin(theta_s(I))*cos(theta_r)-DER_H_RL(I,Ir)*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(theta_r)
        ENDDO 
    ENDDO 
ENDFUNCTION 

