!INTEGER, PARAMETER:: G_1_COEFFS_LEN = 17 

PURE FUNCTION  CONVERT_COEFFS_FOR_G_1 (A, y1, y2) RESULT(U) 
COMPLEX(DP),INTENT(IN) :: A(0:MAX_COS_ORDER) 
REAL(DP), INTENT(IN) :: y1, y2 
COMPLEX(DP):: U(1: G_1_COEFFS_LEN ) 
REAL(DP):: l, g 

l = y2-y1
g = (1-y2)/l

U(1) = (A(3)*7.5E+0_DP)/l**3
U(2) = -(A(3)*7.5E+0_DP)/l**3
U(3) = ((g*(A(3)*6.E+1_DP*g+A(3)*6.E+1_DP-A(2)&
	&*1.2E+1_DP)+A(3)*1.2E+1_DP-A(2)*6.E+0_DP+A(1)*2.E+0_DP)*l**2&
	&+A(3)*1.21875E+1_DP)/l**3
U(4) = (A(3)*2.53125E+1_DP)/l**3
U(5) = (9.375E-1_DP*A(3))/l**3
U(6) = -(A(3)*8.4375E+0_DP)/l**3
U(7) = ((-A(3)*3.E+1_DP*g)-A(3)*1.5E+1_DP+A(2)*3.E+0_DP)/l**2
U(8) = (A(3)*3.E+1_DP*g+A(3)*1.5E+1_DP-A(2)*3.E+0_DP)/l**2
U(9) = ((-A(3)*3.E+1_DP*g)-A(3)*1.5E+1_DP+A(2)*3.E+0_DP)/l**2
U(10) = -(A(3)*7.5E+0_DP)/l**3
U(11) = (A(3)*7.5E+0_DP)/l**3
U(12) = (9.375E-1_DP*A(3))/l**3
U(13) = -(A(3)*8.4375E+0_DP)/l**3
U(14) = (A(3)*4.6875E+0_DP)/l**3
U(15) = (2.8125E+0_DP*A(3))/l**3
U(16) = (A(3)*3.E+1_DP*g+A(3)*1.5E+1_DP-A(2)*3.E+0_DP)/l**2
U(17) = ((-A(3)*3.E+1_DP*g)-A(3)*1.5E+1_DP+A(2)*3.E+0_DP)/l**2
ENDFUNCTION 


PURE FUNCTION  CALCULATE_ANALYTICALLY_G_1 (S0_X, S0_Y, S0_Z, SC_X, SC_Y, SC_Z, U)& 
			& RESULT(RES) 
REAL(DP), INTENT(IN)::S0_X, S0_Y, S0_Z 
REAL(DP), INTENT(IN)::SC_X(PMAX,2), SC_Y(PMAX,2), SC_Z(PMAX,2) 
COMPLEX(DP),INTENT(IN)::U(1: G_1_COEFFS_LEN ) 
COMPLEX(DP)::RES 
RES = (SC_Z(2,1)*U(17)*S0_x+SC_X(2,1)*SC_Z(2,1)&
	&*U(8))*S0_y+SC_Y(2,1)*SC_Z(2,1)*U(16)*S0_x+SC_Z(3,1)*SC_X(3,2)&
	&*SC_Y(3,2)*U(15)+SC_Z(1,1)*SC_X(3,2)*SC_Y(3,2)*U(14)+SC_Y(1,2)&
	&*SC_Z(3,1)*SC_X(3,2)*U(13)+SC_Z(1,1)*SC_Y(1,2)*SC_X(3,2)*U(12)&
	&+SC_Z(2,1)*SC_X(3,1)*SC_Y(3,1)*U(11)+SC_Y(1,1)*SC_Z(2,1)*SC_X(3,1)&
	&*U(10)+SC_Z(1,1)*SC_X(2,2)*SC_Y(2,2)*U(9)+SC_X(2,1)*SC_Y(2,1)*SC_Z(2,1)&
	&*U(7)+SC_X(1,2)*SC_Z(3,1)*SC_Y(3,2)*U(6)+SC_Z(1,1)*SC_X(1,2)&
	&*SC_Y(3,2)*U(5)+SC_X(1,2)*SC_Y(1,2)*SC_Z(3,1)*U(4)+SC_X(1,1)*U(2)&
	&*SC_Z(2,1)*SC_Y(3,1)+SC_Z(1,1)*SC_X(1,2)*SC_Y(1,2)*U(3)+U(1)*SC_X(1,1)*SC_Y(1,1)*SC_Z(2,1)
ENDFUNCTION 
