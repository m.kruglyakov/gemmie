!\eqref{ initial_der_1_theta_DI } 
DV0(I0,PolD) = R_ZERO
DV0(I0,PolS) = R_ZERO
DV0(IC,PolD) = R_ZERO
DV0(IC,PolS) = R_ZERO
DV0(ID0,PolD) = R_ZERO
DV0(ID0,PolS) = R_ZERO
DV0(IDC,PolD) = R_ZERO
DV0(IDC,PolS) = R_ZERO
DV1(I0,PolD) = R_FOUR*mu*skappa*stau*stheta+R_FOUR*ckappa*cnu*&
    &ctheta*smu*stau
DV1(I0,PolS) = R_FOUR*cnu*ctau*ctheta*skappa*smu-R_FOUR*ckappa*&
    &ctau*mu*stheta
DV1(IC,PolD) = R_FOUR*cnu*skappa*smu*stau*stheta-R_FOUR*ckappa*&
    &cmu*ctheta*smu*snu2*stau+R_TWO*ckappa*cmu*ctheta*smu*stau+R_TWO*&
    &ckappa*ctheta*mu*stau
DV1(IC,PolS) = (-R_FOUR*ckappa*cnu*ctau*smu*stheta)-R_FOUR*cmu*&
    &ctau*ctheta*skappa*smu*snu2+R_TWO*cmu*ctau*ctheta*skappa*smu+R_TWO*&
    &ctau*ctheta*mu*skappa
DV1(ID0,PolD) = R_ZERO
DV1(ID0,PolS) = R_ZERO
DV1(IDC,PolD) = R_ZERO
DV1(IDC,PolS) = R_ZERO
