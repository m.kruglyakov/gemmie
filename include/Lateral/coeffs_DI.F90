!\eqref{ coeffs_DI } 
A(PolS) = gamma/alpha+ckappa*ctau*ctheta
A(PolD) = -ctheta*skappa*stau
B(PolS) = ctau*skappa*stheta
B(PolD) = ckappa*stau*stheta
ETA(PolS) = -cmu*snu*stheta
ETA(PolD) = cnu*smu
