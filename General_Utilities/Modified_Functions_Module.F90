MODULE  MODIFIED_FUNCTIONS_MODULE
    USE PRECISION_CONTROL_MODULE
!    USE EXPONENTIONAL_INTEGRAL_MODULE
    IMPLICIT NONE
    PRIVATE


    INTERFACE EXP_Z
        MODULE PROCEDURE EXP_Z_CMPLX
        MODULE PROCEDURE EXP_Z_REAL
    ENDINTERFACE

    INTERFACE LOG_SHIFTED
        MODULE PROCEDURE LOG_SHIFTED_REAL
        MODULE PROCEDURE LOG_SHIFTED_CMPLX
    ENDINTERFACE

    PUBLIC :: EXP_Z, LOG_SHIFTED
CONTAINS
    ELEMENTAL FUNCTION LOG_SCALED(z) RESULT(RES)
        COMPLEX(DP),INTENT(IN)::z
        COMPLEX(DP)::RES
        INTEGER::I
        COMPLEX(DP)::A,R
        IF (ABS(z)>0.5_DP) THEN
            RES=(LOG(C_ONE+z)+z*z/C_TWO-z)/z/z
        ELSE
            A=z
            R=C_ZERO
            I=3
            DO 
                R=R+A/I
                A=-z*A
                IF ( ABS(A/R)<ACCURACY) EXIT
                I=I+1
            ENDDO
            RES=R
        ENDIF
    ENDFUNCTION

    ELEMENTAL FUNCTION LOG_SHIFTED_CMPLX(z) RESULT(RES) !log(1+z)
        COMPLEX(DP),INTENT(IN)::z
        COMPLEX(DP)::RES
        INTEGER::I
        COMPLEX(DP)::A,R
        IF (ABS(z)>0.5_DP) THEN
            RES=LOG(C_ONE+z)
        ELSE
            A=z
            R=C_ZERO
            I=1
            DO 
                R=R+A/I
                A=-z*A
                IF ( ABS(A/R)<ACCURACY/R_TWO) EXIT
                I=I+1
            ENDDO
            RES=R
        ENDIF
    ENDFUNCTION

    ELEMENTAL FUNCTION LOG_SHIFTED_REAL(z) RESULT(RES) !log(1+z)
        REAL(DP),INTENT(IN)::z
        REAL(DP)::RES
        INTEGER::I
        REAL(DP)::A,R
        IF (ABS(z)>0.5_DP) THEN
            RES=LOG(R_ONE+z)
        ELSEIF (ABS(z) <TINY(z)) THEN
            RES=R_ZERO
        ELSE
            A=z
            R=R_ZERO
            I=1
            DO 
                R=R+A/I
                A=-z*A
                IF ( ABS(A/R)<ACCURACY/R_TWO) EXIT
                I=I+1
            ENDDO
            RES=R
        ENDIF
    ENDFUNCTION
    
    ELEMENTAL FUNCTION EXP_Z_CMPLX(z) RESULT(RES)
        COMPLEX(DP),INTENT(IN)::z
        COMPLEX(DP)::RES
        IF ((REAL(z, DP)<EXP_ARG_LIMIT) .AND.(REAL(z, DP)>-EXP_ARG_LIMIT)) THEN
            RES=EXP(z)
        ELSEIF (REAL(z, DP)<=-EXP_ARG_LIMIT) THEN
            RES=C_ZERO
        ELSE
            RES=HUGE(EXP_ARG_LIMIT)+1
        ENDIF
    ENDFUNCTION

    ELEMENTAL FUNCTION EXP_Z_REAL(z) RESULT(RES)
        REAL(DP),INTENT(IN)::z
        REAL(DP)::RES
        IF (ABS(z) <TINY(z)) THEN
            RES=R_ONE
            RETURN
        ENDIF
        IF (z<EXP_ARG_LIMIT .AND. z>-EXP_ARG_LIMIT) THEN
            RES=EXP(z)
        ELSEIF (z<=-EXP_ARG_LIMIT) THEN
            RES=R_ZERO
        ELSE
            RES=HUGE(RES)+1
        ENDIF
    ENDFUNCTION

END MODULE  Modified_Functions_Module
