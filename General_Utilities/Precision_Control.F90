MODULE  PRECISION_CONTROL_MODULE
    USE ISO_C_BINDING
    IMPLICIT NONE
    INTEGER, PARAMETER :: SP  = SELECTED_REAL_KIND(5) 
#ifdef SINGLE
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(5) 
    INTEGER, PARAMETER :: R_SIZE =4 
#else
#ifdef QUAD
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(30) 
    INTEGER, PARAMETER :: R_SIZE =16 
#else
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(15) 
    INTEGER, PARAMETER :: R_SIZE =8 
#endif
#endif

    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_DOUBLE=0
    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_REAL=1
    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_NATIVE_INTEGER=2
#ifdef SINGLE
    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_DEFAULT=SAVE_AS_REAL
#else
#ifdef SINGLE_IN_OUTPUT
    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_DEFAULT=SAVE_AS_REAL
#else
    INTEGER, PUBLIC, PARAMETER :: SAVE_AS_DEFAULT=SAVE_AS_DOUBLE
#endif
#endif

    REAL(DP), PUBLIC, PARAMETER :: R_SIZE_GB =REAL(R_SIZE,DP)/1024.0_DP/1024.0_DP/1024.0_DP

    INTEGER, PUBLIC, PARAMETER :: COORDS_KIND=C_INT16_T


    INTEGER,PARAMETER:: L_INT  = SELECTED_INT_KIND(15) 
    INTEGER,PARAMETER:: W_INT  = SELECTED_INT_KIND(4) 

    REAL(DP),PARAMETER:: R_ZERO =0.0_DP 
    REAL(DP),PARAMETER:: R_TWO =2.0_DP 
    REAL(DP),PARAMETER:: R_THREE =3.0_DP 
    REAL(DP),PARAMETER:: R_ONE =1.0_DP 
    REAL(DP),PARAMETER:: R_HALF =0.5_DP 

    REAL(DP),PARAMETER:: ACCURACY =EPSILON(R_ONE) 
    REAL(DP),PARAMETER:: LOG_ACCURACY =LOG(ACCURACY) 
    REAL(DP),PARAMETER:: EXP_ARG_LIMIT =LOG(HUGE(R_ONE))*(1.0_DP-1.0e-1_DP) !!????? 

    COMPLEX(DP),PARAMETER:: C_ZERO =(0.0_DP,0.0_DP) 
    COMPLEX(DP),PARAMETER:: C_ONE =(1.0_DP,0.0_DP) 
    COMPLEX(DP),PARAMETER:: C_IONE =(0.0_DP,1.0_DP) 
    COMPLEX(DP),PARAMETER:: C_TWO =(2.0_DP,0.0_DP) 
    COMPLEX(DP),PARAMETER:: C_THREE =(3.0_DP,0.0_DP) 
    COMPLEX(DP),PARAMETER:: C_HALF =(0.5_DP,0.0_DP) 


    REAL(DP), PARAMETER:: PI =3.1415926535897932384626433832795029_DP 
    REAL(DP), PARAMETER:: HALF_PI =PI/R_TWO 
    REAL(DP), PARAMETER:: PI4 =PI*4.0_DP 

    REAL(DP), PUBLIC, PARAMETER :: MU0 =4.0_DP*PI*1E-7_DP 
    REAL(DP), PUBLIC, PARAMETER :: EPS0 =1.0_DP/(MU0*299792458.0_DP*299792458.0_DP) 


    INTERFACE ZEROISE_MULT
        MODULE PROCEDURE ZEROISE_MULT2_COMPLEX
        MODULE PROCEDURE ZEROISE_MULT2_REAL
        MODULE PROCEDURE ZEROISE_MULT3_COMPLEX
        MODULE PROCEDURE ZEROISE_MULT3_REAL
    ENDINTERFACE

    INTERFACE MULT_OR_ZERO
        MODULE PROCEDURE MULT_OR_ZERO_CMPX
        MODULE PROCEDURE MULT_OR_ZERO_REAL_CMPLX
        MODULE PROCEDURE MULT_OR_ZERO_CMPLX_REAL
        MODULE PROCEDURE MULT_OR_ZERO_REAL
    ENDINTERFACE

    INTERFACE ZEROISE_WRT_ONE
        MODULE PROCEDURE ZEROISE_WRT_ONE_CMPLX
        MODULE PROCEDURE ZEROISE_WRT_ONE_REAL
    ENDINTERFACE

    INTERFACE ZEROISE_IF_SMALL
        MODULE PROCEDURE ZEROISE_IF_SMALL_CMPLX
    ENDINTERFACE
    !    private :: ZEROISE_MULT

CONTAINS

    ELEMENTAL SUBROUTINE ZEROISE_WRT_ONE_CMPLX(c,x)
 
        REAL(DP),INTENT(IN):: c  
        COMPLEX(DP), INTENT(INOUT):: x  
#ifdef trap_errorneous
        REAL(DP) :: TMP (2) 
        INTEGER:: e (2), ec 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-53 
#endif
        !    TMP=TRANSFER(x,TMP)
        TMP(1)=REAL(x, DP)
        TMP(2)=AIMAG(x)
        e=EXPONENT(TMP)
        ec=EXPONENT(c)
        IF (e(1)+ec < EP) TMP(1)=R_ZERO
        IF (e(2)+ec < EP) TMP(2)=R_ZERO
        x=CMPLX(TMP(1), TMP(2), DP)
#endif
    ENDSUBROUTINE

    ELEMENTAL SUBROUTINE ZEROISE_IF_SMALL_CMPLX(c,x)
 
        REAL(DP),INTENT(IN):: c  
        COMPLEX(DP), INTENT(INOUT):: x  
#ifdef trap_errorneous
        REAL(DP) :: TMP (2) 
        INTEGER:: e (2), ec 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-53 
#endif
        TMP(1)=REAL(x, DP)
        TMP(2)=AIMAG(x)
        e=EXPONENT(TMP)
        ec=EXPONENT(c)
        IF (e(1) <ec) TMP(1)=R_ZERO
        IF (e(2) < ec ) TMP(2)=R_ZERO

        x=CMPLX(TMP(1), TMP(2), DP)
#endif
    ENDSUBROUTINE

    ELEMENTAL SUBROUTINE ZEROISE_WRT_ONE_REAL(c,x)
        REAL(DP),INTENT(IN):: c  
        REAL(DP), INTENT(INOUT):: x  
#ifdef trap_errorneous
        INTEGER:: e , ec 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-53 
#endif
        e=EXPONENT(x)
        ec=EXPONENT(c)
        IF (e+ec < EP) x=R_ZERO
#endif
    ENDSUBROUTINE

    ELEMENTAL SUBROUTINE ZEROISE_MULT2_COMPLEX(x,y)
 
        COMPLEX(DP), INTENT(IN):: x  
        COMPLEX(DP), INTENT(INOUT):: y  
#ifdef trap_errorneous
        CALL ZEROISE_MULT2_REAL(ABS(x),y)
#endif
    ENDSUBROUTINE

    ELEMENTAL SUBROUTINE ZEROISE_MULT2_REAL(c,x)
 
        REAL(DP), INTENT(IN):: c  
        COMPLEX(DP), INTENT(INOUT):: x  
#ifdef trap_errorneous
        REAL(DP) :: TMP (2) 
        INTEGER:: e (2), ec 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(TINY(R_ONE))-EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-972 
#endif
        !TMP=TRANSFER(x,TMP)
        TMP(1)=REAL(x, DP)
        TMP(2)=AIMAG(x)
        e=EXPONENT(TMP)
        ec=EXPONENT(c)
        IF (e(1)+ec < EP) TMP(1)=R_ZERO
        IF (e(2)+ec < EP) TMP(2)=R_ZERO
        x=CMPLX(TMP(1), TMP(2), DP)
#endif
    ENDSUBROUTINE

    ELEMENTAL SUBROUTINE ZEROISE_MULT3_COMPLEX(x, y, z)
 
        COMPLEX(DP), INTENT(IN):: x , y 
        COMPLEX(DP), INTENT(INOUT):: z  
#ifdef trap_errorneous
        CALL ZEROISE_MULT3_REAL(ABS(x), ABS(y), z)
#endif
    ENDSUBROUTINE

    PURE SUBROUTINE ZEROISE_MULT3_REAL(x, y, z)
 
        REAL(DP), INTENT(IN):: x , y 
        COMPLEX(DP), INTENT(INOUT):: z  
#ifdef trap_errorneous
        REAL(DP) :: TMP (2) 
        INTEGER:: e (2), ex, ey 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(TINY(R_ONE))-EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-972 
#endif
        !TMP=TRANSFER(x,TMP)
        TMP(1)=REAL(z, DP)
        TMP(2)=AIMAG(z)
        e=EXPONENT(TMP)
        ex=EXPONENT(x)
        ey=EXPONENT(y)
        IF (e(1)+ex+ey < EP) TMP(1)=R_ZERO
        IF (e(2)+ex+ey < EP) TMP(2)=R_ZERO
        z=CMPLX(TMP(1), TMP(2), DP)
#endif
    ENDSUBROUTINE

    ELEMENTAL FUNCTION MULT_OR_ZERO_CMPX(x,y) RESULT(RES)
 
        COMPLEX(DP), INTENT(IN):: x , y 
        COMPLEX(DP) :: RES  
#ifdef trap_errorneous
        REAL(DP) :: TMPX (2) 
        REAL(DP) :: TMPY (2) 
        REAL(DP) :: TMP (2) 
        INTEGER::  ex(2), ey(2) 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(TINY(R_ONE))-EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-972 
#endif
        TMPX(1)=REAL(x, DP)
        TMPX(2)=AIMAG(x)

        TMPY(1)=REAL(y, DP)
        TMPY(2)=AIMAG(y)

        ex=EXPONENT(TMPX)
        ey=EXPONENT(TMPY)

        TMP=R_ZERO

        IF (ex(1)+ey(1) > EP) THEN
            TMP(1)=TMP(1)+TMPX(1)*TMPY(1)
        ENDIF
        IF (ex(2)+ey(2) > EP) THEN
            TMP(1)=TMP(1)-TMPX(2)*TMPY(2)
        ENDIF

        IF (ex(1)+ey(2) > EP) THEN
            TMP(2)=TMP(2)+TMPX(1)*TMPY(2)
        ENDIF
        IF (ex(2)+ey(1) > EP) THEN
            TMP(2)=TMP(2)+TMPX(2)*TMPY(1)
        ENDIF
        RES=CMPLX(TMP(1),TMP(2), DP)

#else
        RES=x*y
#endif
    ENDFUNCTION

    ELEMENTAL FUNCTION MULT_OR_ZERO_CMPLX_REAL(x,y) RESULT(RES)
        COMPLEX(DP), INTENT(IN):: x  
        REAL(DP), INTENT(IN):: y  
        COMPLEX(DP) :: RES  
#ifdef trap_errorneous
        RES=MULT_OR_ZERO(y,x)
#else
        RES=x*y
#endif
    ENDFUNCTION

    ELEMENTAL FUNCTION MULT_OR_ZERO_REAL_CMPLX(x,y) RESULT(RES)
        REAL(DP), INTENT(IN):: x  
        COMPLEX(DP), INTENT(IN):: y  
        COMPLEX(DP) :: RES  
#ifdef trap_errorneous
        RES=MULT_OR_ZERO(CMPLX(x,R_ZERO,DP),y)
#else
        RES=x*y
#endif
    ENDFUNCTION

    ELEMENTAL FUNCTION MULT_OR_ZERO_REAL(x,y) RESULT(RES)
        REAL(DP), INTENT(IN):: x , y 
        REAL(DP) :: RES  
#ifdef trap_errorneous
        INTEGER:: ex , ey 
#ifndef __PGI
        INTEGER, PARAMETER :: EP =EXPONENT(TINY(R_ONE))-EXPONENT(EPSILON(R_ONE))-2 
#else
        INTEGER, PARAMETER :: EP =-972 
#endif

        ex=EXPONENT(x)
        ey=EXPONENT(y)

        RES=R_ZERO

        IF (ex+ey > EP) RES=x*y

#else
        RES=x*y
#endif
    ENDFUNCTION

ENDMODULE

