MODULE OPTIMIZATION_MODULE
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE LOGGER_MODULE
    USE TIMER_MODULE
    USE CHECK_MEMORY
    USE LBFGSB_MODULE
    USE IO_UTILITIES
    USE DISTRIBUTED_LBFGS_MODULE
    IMPLICIT NONE
    PRIVATE

    INTERFACE 
        SUBROUTINE CALC_TARGET_FUNC_TYPE(x, func_data, res )
            IMPORT DP
            REAL(DP), INTENT(IN) :: x(:)
            CLASS(*), POINTER :: func_data
            REAL(DP), INTENT(OUT) :: res
        ENDSUBROUTINE

        SUBROUTINE CALC_GRADIENT_TYPE(x, func_data, res)
            IMPORT DP
            REAL(DP), INTENT(IN) :: x(:)
            CLASS(*), POINTER :: func_data
            REAL(DP), INTENT(INOUT) :: res(:)
        ENDSUBROUTINE

        SUBROUTINE CORRECT_DIRECTION_TYPE(x, func_data, res, same)
            IMPORT DP
            REAL(DP), INTENT(IN) :: x(:)
            CLASS(*), POINTER :: func_data
            REAL(DP), INTENT(INOUT) :: res(:)
            LOGICAL, INTENT(OUT) :: same
        ENDSUBROUTINE

        FUNCTION CHECK_SMALL_FUNC_TYPE(f, func_data) RESULT(RES)
            IMPORT DP
            REAL(DP), INTENT(IN) :: f
            CLASS(*), INTENT(IN), POINTER :: func_data
            LOGICAL  :: res
        ENDFUNCTION

        SUBROUTINE UPDATE_FUNC_DATA_TYPE(func_data, new_data) 
            CLASS(*), INTENT(IN), POINTER :: func_data
            CLASS(*), INTENT(IN):: new_data
        ENDSUBROUTINE

        SUBROUTINE GET_MSG_FROM_FUNC(func_data, msg) 
            CLASS(*), INTENT(IN), POINTER :: func_data
            CHARACTER(len=*), INTENT(INOUT):: msg
        ENDSUBROUTINE
    ENDINTERFACE

    INTERFACE OPT_SOLVER_LOG
        MODULE PROCEDURE OPT_SOLVER_LOG_MSG
        MODULE PROCEDURE OPT_SOLVER_LOG3
        MODULE PROCEDURE OPT_SOLVER_LOG4
    ENDINTERFACE

    TYPE CALLBACKS_FOR_OPT
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, NOPASS :: func_ptr=>NULL()
        PROCEDURE(CALC_GRADIENT_TYPE),    POINTER, NOPASS :: grad_ptr=>NULL()
        PROCEDURE(CORRECT_DIRECTION_TYPE),    POINTER, NOPASS :: s_dir_corr_ptr=>NULL()
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, NOPASS :: chk_ptr=>NULL()
        PROCEDURE(UPDATE_FUNC_DATA_TYPE), POINTER, NOPASS :: upd_ptr=>NULL()
        PROCEDURE(GET_MSG_FROM_FUNC), POINTER, NOPASS :: msg_ptr=>NULL()
    ENDTYPE



    INTEGER, PUBLIC, PARAMETER :: LN_UNDEFINED=-1
    INTEGER, PUBLIC,  PARAMETER :: LN_ARMIJO=2
    INTEGER, PUBLIC,  PARAMETER :: LN_WOLFE=3

    TYPE LBFGS_STATE
        INTEGER  ::  L, L_mem
        REAL(DP) :: f
        CHARACTER(len=60) :: task, csave
        LOGICAL                :: lsave(4)
        INTEGER                :: isave(44)
        REAL(DP)               :: dsave(29)
        REAL(DP), ALLOCATABLE  :: x(:)
        INTEGER,  ALLOCATABLE  :: iwa(:)
        REAL(DP), ALLOCATABLE  :: wa(:)
        REAL(DP), ALLOCATABLE  :: g(:)
    ENDTYPE

    INTERFACE 
        SUBROUTINE STORE_LBFGS_STATE(state, fname )
            IMPORT LBFGS_STATE
            TYPE(LBFGS_STATE), INTENT(IN) :: state
            CHARACTER(len=*), INTENT(IN) :: fname
        ENDSUBROUTINE
    ENDINTERFACE

    TYPE LBFGS_DATA
        INTEGER :: L, L_mem
        INTEGER,  ALLOCATABLE  :: nbd(:)
        REAL(DP), ALLOCATABLE  :: l_bnd(:), u_bnd(:)
        REAL(DP) :: factr, pgtol
        CHARACTER(len=:), ALLOCATABLE :: log_file
        TYPE(LBFGS_STATE), ALLOCATABLE :: state
        PROCEDURE( STORE_LBFGS_STATE), POINTER, NOPASS :: STORE_STATE=>NULL()
        CHARACTER(len=:), ALLOCATABLE :: store_name
    ENDTYPE

    TYPE GRAD_METHOD_DATA
        REAL(DP) :: gtol=1e-10_DP
        REAL(DP) :: tau=1e-4_DP
        REAL(DP) :: tau_curv=0.9_DP
        REAL(DP) :: alpha_step=R_TWO
        REAL(DP) :: max_step=R_ONE
        CHARACTER(len=:), ALLOCATABLE :: log_file
        INTEGER :: LN_TYPE=LN_UNDEFINED
    ENDTYPE

    TYPE LS_PARAMS_TYPE
        REAL(DP) :: gtol=1e-10_DP
        REAL(DP) :: tau=1e-4_DP
        REAL(DP) :: tau_curv=0.9_DP
        REAL(DP) :: alpha_sc=-0.5_DP
        INTEGER  :: LS_max_it=50
        LOGICAL  :: wolfe_weak=.TRUE.

    ENDTYPE

    CHARACTER (len=*), PARAMETER :: FMT_NAIVE_LS='(F12.3, A, A, I5, G15.8, G15.8, A, G15.8)'
    CHARACTER (len=*), PARAMETER :: FMT_GRAD2=   '(F12.3, A, A,  I5, G15.8, G15.8)'
    CHARACTER (len=*), PARAMETER :: LS_msg="  LS It:"
    CHARACTER (len=*), PARAMETER :: M_msg= "Main It:"
    CHARACTER (len=*), PARAMETER :: A_msg= "BB alpha:"
    CHARACTER (len=*), PARAMETER :: LBFGS_msg= "LBFGS Iteration:"

    INTEGER, PARAMETER ::LBFGS_FWD_RUN =1
    INTEGER, PARAMETER ::LBFGS_ZERO_IT =-1
    INTEGER, PARAMETER ::LBFGS_NEW_X =2
    INTEGER, PARAMETER ::LBFGS_ERROR =3
    INTEGER, PARAMETER ::LBFGS_CONVERGED =4
    INTEGER, PARAMETER ::LBFGS_STOP =5

    PUBLIC :: CALC_TARGET_FUNC_TYPE
    PUBLIC :: CALC_GRADIENT_TYPE
    PUBLIC :: CHECK_SMALL_FUNC_TYPE
    PUBLIC :: UPDATE_FUNC_DATA_TYPE
    PUBLIC :: STORE_LBFGS_STATE
    PUBLIC :: CALLBACKS_FOR_OPT

    PUBLIC :: LBFGS_DATA, LBFGS_STATE
    PUBLIC :: LBFGS_METHOD
    PUBLIC :: GRAD_METHOD_DATA, GRADIENT_DESCENT_METHOD
    PUBLIC :: GRADIENT_BB_METHOD, GRADIENT_CG_METHOD

    PUBLIC :: LS_PARAMS_TYPE
    PUBLIC :: LBFGS_2_METHOD
CONTAINS

    SUBROUTINE LBFGS_METHOD(lbfgs_prms, func_ptr, grad_ptr, chk_ptr, upd_ptr, func_data,  x_res)
        TYPE(LBFGS_DATA), INTENT(INOUT) :: lbfgs_prms
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        PROCEDURE(UPDATE_FUNC_DATA_TYPE), POINTER, INTENT(IN) :: upd_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT):: x_res(:)
        INTEGER,  PARAMETER    :: iprint = 1
        INTEGER :: U, Itask
        INTEGER (L_INT) :: L, L_mem
        LOGICAL :: is_small
        INTEGER( L_INT) :: L_alloc

        IF ( .NOT. ALLOCATED(lbfgs_prms%state)) THEN
            ALLOCATE(lbfgs_prms%state)
            L=SIZE(x_res)
            L_mem=lbfgs_prms%l_mem
            lbfgs_prms%state%L=INT(L, KIND=KIND(lbfgs_prms%state%L))
            lbfgs_prms%state%L_mem=INT(L_mem, KIND=KIND(lbfgs_prms%state%L_mem))
            ALLOCATE(lbfgs_prms%state%x(SIZE(x_res)))
            lbfgs_prms%state%x=x_res
            ALLOCATE ( lbfgs_prms%state%g(L)) 
            ALLOCATE ( lbfgs_prms%state%iwa(3*L) )
            L_alloc=2*L_mem*L + 5*L + 11*L_mem**2 + 8*L_mem
            ALLOCATE ( lbfgs_prms%state%wa(L_alloc) )
            lbfgs_prms%state%g=R_ZERO
            lbfgs_prms%state%iwa=0
            lbfgs_prms%state%wa=R_ZERO
            lbfgs_prms%state%task='START'
        ENDIF

        U=OPEN_FOR_WRITE(lbfgs_prms%log_file)
        lbfgs_prms%state%isave(24)=U
        ASSOCIATE( & 
                L=>     lbfgs_prms%state%L, &
                L_mem=> lbfgs_prms%state%L_mem, &
                x=>     lbfgs_prms%state%x,     &
                f=>     lbfgs_prms%state%f,     &
                g=>     lbfgs_prms%state%g,    &
                wa=>    lbfgs_prms%state%wa,    &
                iwa=>   lbfgs_prms%state%iwa,   &
                task=>  lbfgs_prms%state%task,  &
                csave=> lbfgs_prms%state%csave, &
                lsave=> lbfgs_prms%state%lsave, &
                isave=> lbfgs_prms%state%isave, &
                dsave=> lbfgs_prms%state%dsave  &
                )
            LBFGS_LOOP: DO WHILE( task(1:2).eq.'FG' .or. task.eq.'NEW_X' .or. &
                    task.eq.'START') 


                CALL SETULB ( L, L_mem, x, lbfgs_prms%l_bnd, lbfgs_prms%u_bnd, lbfgs_prms%nbd, &
                    f, g, lbfgs_prms%factr, lbfgs_prms%pgtol, &
                    wa, iwa, task, iprint,&
                    csave, lsave, isave, dsave )

                Itask=TASK_STR_2_NUM(task)

                SELECTCASE(Itask)
                    CASE (LBFGS_FWD_RUN, LBFGS_ZERO_IT)
                        CALL func_ptr(x, func_data, f)
                        IF (ASSOCIATED(chk_ptr)) THEN
                            is_small=chk_ptr(f, func_data)
                            IF (is_small) THEN
                                EXIT LBFGS_LOOP
                            ENDIF
                        ENDIF
                        CALL grad_ptr(x, func_data, g)
                        IF (Itask==LBFGS_ZERO_IT) THEN
                            IF (ASSOCIATED(upd_ptr)) THEN
                                CALL upd_ptr(func_data, .TRUE.)
                            ENDIF
                        ENDIF
                    CASE (LBFGS_STOP, LBFGS_CONVERGED, LBFGS_ERROR)
                        IF (ASSOCIATED(lbfgs_prms%STORE_STATE)) THEN
                            CALL lbfgs_prms%STORE_STATE(lbfgs_prms%state, lbfgs_prms%store_name)
                        ENDIF
                        DEALLOCATE(lbfgs_prms%state)
                        EXIT LBFGS_LOOP
                    CASE (LBFGS_NEW_X)
                        IF (ASSOCIATED(lbfgs_prms%STORE_STATE)) THEN
                            CALL lbfgs_prms%STORE_STATE(lbfgs_prms%state, lbfgs_prms%store_name)
                        ENDIF
                        IF (ASSOCIATED(upd_ptr)) THEN
                            CALL upd_ptr(func_data, .TRUE.)
                        ENDIF
                        CYCLE LBFGS_LOOP
                    CASE DEFAULT
                ENDSELECT
            ENDDO LBFGS_LOOP
            x_res=x
        ENDASSOCIATE
        CLOSE (U)

    ENDSUBROUTINE

    FUNCTION  TASK_STR_2_NUM(task) RESULT(Itask)
        CHARACTER(len=60), INTENT(IN) :: task
        INTEGER :: Itask
        Itask=LBFGS_STOP
        IF (task(1:2)=='FG') THEN
            Itask=LBFGS_FWD_RUN
            IF (task(4:5)=='ST') Itask=LBFGS_ZERO_IT
        ELSEIF(task(1:5)=='NEW_X') THEN
            Itask=LBFGS_NEW_X
        ELSEIF(task(1:5)=='ERROR') THEN
            Itask=LBFGS_ERROR
        ELSEIF (task(1:11)=='CONVERGENCE') THEN 
            Itask=LBFGS_CONVERGED
        ENDIF

    ENDFUNCTION

    SUBROUTINE GRADIENT_DESCENT_METHOD(grad_prms,  func_ptr, grad_ptr, &
            chk_ptr, func_data,  x_res)
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT):: x_res(:)
        REAL(DP), ALLOCATABLE :: g(:), x(:), p(:)
        REAL(DP) :: f
        REAL(DP) ::g_norm
        LOGICAL :: is_small
        INTEGER :: U
        INTEGER :: K

        U=OPEN_FOR_WRITE(grad_prms%log_file)
        ALLOCATE(g(SIZE(x_res)),x (SIZE(x_res)), p(SIZE(x_res)) )
        K=1
        CALL func_ptr(x_res, func_data, f)
        is_small=chk_ptr(f, func_data)
        IF (is_small) THEN
            WRITE (U,*) K, f
            CLOSE (U)
            RETURN
        ENDIF
        CALL grad_ptr(x_res, func_data, g)
        g_norm=SQRT(DOT_PRODUCT(g,g))

        CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
        K=2

        DO WHILE (g_norm>grad_prms%gtol)
            x=x_res
            p=-g

            CALL LINE_SEARCH(grad_prms, U, func_ptr, grad_ptr, chk_ptr, &
                func_data,  p, x, x_res, f, g )

            is_small=chk_ptr(f, func_data)
            IF (is_small)  EXIT
            g_norm=SQRT(DOT_PRODUCT(g,g))
            CALL OPT_SOLVER_LOG(U, M_msg,  K, f, g_norm)
            K=K+1
        ENDDO
        CLOSE (U)
    ENDSUBROUTINE


    SUBROUTINE LINE_SEARCH(grad_prms, U,  func_ptr, grad_ptr, chk_ptr, &
            func_data,  p, x, x_res, f0, g)
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        INTEGER, INTENT(IN) :: U
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(IN) :: p(:),  x(:)
        REAL(DP), INTENT(INOUT) :: f0
        REAL(DP), INTENT(INOUT) :: x_res(:), g(:)
        LOGICAL :: is_small, chk_w(2)
        REAL(DP) :: pg0
        SELECTCASE (grad_prms%LN_TYPE)
            CASE (LN_ARMIJO)
                CALL ARMIJO_LINE_SEARCH(grad_prms, U,  func_ptr, chk_ptr, func_data,  p, g, x, x_res, f0 )
                is_small=chk_ptr(f0, func_data)
                IF (is_small)  RETURN
                pg0=DOT_PRODUCT(p,g)
                CALL grad_ptr(x_res, func_data, g)
                CALL CHECK_WOLFE_CONDITIONS(grad_prms, U,  p, pg0, g, chk_w)

            CASE (LN_WOLFE)
                CALL WOLFE_LINE_SEARCH(grad_prms, U,  func_ptr, grad_ptr, chk_ptr, &
                    func_data,  p, x, x_res, f0, g)
            CASE DEFAULT
                PRINT*, "Unsupported line search method"
                STOP 23
        ENDSELECT
    ENDSUBROUTINE

    SUBROUTINE ARMIJO_LINE_SEARCH(grad_prms, U,  func_ptr, chk_ptr, func_data,  p, g, x, x_res, f0 )
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        INTEGER, INTENT(IN) :: U
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(IN) :: p(:), g(:), x(:)
        REAL(DP), INTENT(INOUT) :: f0
        REAL(DP), INTENT(INOUT) :: x_res(:)
        REAL(DP) :: pg, f, alpha, t, p_nrm
        INTEGER :: K 

        pg=DOT_PRODUCT(p,g)
        p_nrm=SQRT(DOT_PRODUCT(p,p))
        t=MIN(grad_prms%max_step/p_nrm, ABS((R_ONE-grad_prms%tau)*ABS(f0)/pg))
        K=1
        f=HUGE(R_ONE)
        alpha=grad_prms%tau
        DO WHILE(f > f0+ alpha*t*pg)
            x_res=x+t*p
            CALL func_ptr(x_res, func_data, f)
            CALL OPT_SOLVER_LOG(U, LS_msg, K, f, t, f0)
            K=K+1
            IF (chk_ptr(f, func_data)) THEN
                f0=f
                RETURN
            ENDIF
            t=t/grad_prms%alpha_step
        ENDDO   
        f0=f
    ENDSUBROUTINE

    SUBROUTINE WOLFE_LINE_SEARCH(grad_prms, U,  func_ptr, grad_ptr, chk_ptr, &
            func_data,  p, x, x_res, f0, g)
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        INTEGER, INTENT(IN) :: U
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(IN) :: p(:), x(:)
        REAL(DP), INTENT(INOUT) :: f0
        REAL(DP), INTENT(INOUT) :: x_res(:), g(:)
        REAL(DP) :: pg, f,  t, pg2, q, p_nrm
        INTEGER :: K
        LOGICAL :: chk_w(2)

        pg=DOT_PRODUCT(p,g)
        p_nrm=SQRT(DOT_PRODUCT(p,p))
        t=MIN(grad_prms%max_step/p_nrm, ABS((R_ONE-grad_prms%tau)*ABS(f0)/pg))
        K=1
        pg2=-HUGE(R_ONE)
        q=R_ONE/grad_prms%alpha_step

        
        chk_w=.FALSE.
#ifdef strong_wolfe
        DO WHILE ( .NOT. chk_w(2))
#else
        DO WHILE ( .NOT. chk_w(1))
#endif
            f=HUGE(R_ONE)
            DO WHILE(f > f0+ grad_prms%tau*t*pg)
                x_res=x+t*p
                CALL func_ptr(x_res, func_data, f)
                CALL OPT_SOLVER_LOG(U, LS_msg, K, f, t, f0)
                K=K+1
                IF (chk_ptr(f, func_data)) THEN
                    f0=f
                    RETURN
                ENDIF
                t=t*q
            ENDDO  
            CALL grad_ptr(x_res, func_data, g)
            CALL CHECK_WOLFE_CONDITIONS(grad_prms, U,  p, pg, g, chk_w) 

            IF ( .NOT. chk_w (1)) THEN
                t=t/q
                q=SQRT(q)
                t=t*(R_ONE+R_ONE/q)/R_TWO/q
            ENDIF

        ENDDO
        f0=f
    ENDSUBROUTINE

    SUBROUTINE CHECK_WOLFE_CONDITIONS(grad_prms, U,  p, pg0, g, chk_w) 
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        INTEGER, INTENT(IN) :: U
        REAL(DP), INTENT(IN) :: p(:), pg0, g(:)
        LOGICAL, INTENT(OUT) :: chk_w(2)
        REAL(DP) :: pg

        pg=DOT_PRODUCT(p, g)
        chk_w=.FALSE.
        IF (ABS(pg) < ABS(grad_prms%tau_curv*pg0)) THEN
            CALL OPT_SOLVER_LOG(U,  "STRONG WOLFE CONDITIONS ARE SATISFIED")
            chk_w=.TRUE.
        ELSEIF (pg >=  grad_prms%tau_curv*pg0) THEN
            CALL OPT_SOLVER_LOG(U,  "WEAK WOLFE CONDITIONS ARE NOT SATISFIED")
        ELSE
            chk_w(1)=.TRUE.
        ENDIF
    ENDSUBROUTINE


    SUBROUTINE GRADIENT_BB_METHOD(grad_prms,  func_ptr, grad_ptr, &
            chk_ptr, func_data,  x_res)
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT):: x_res(:)
        REAL(DP), ALLOCATABLE :: g(:), x(:)
        REAL(DP), ALLOCATABLE :: g0(:)
        REAL(DP) :: f, alpha, f0
        REAL(DP) ::g_norm, step_l, pg0, a_const, m_dx
        LOGICAL :: is_small, chk_w(2)
        INTEGER :: U
        INTEGER :: K

        U=OPEN_FOR_WRITE(grad_prms%log_file)
        ALLOCATE(g(SIZE(x_res)), x(SIZE(x_res)))
        ALLOCATE(g0(SIZE(x_res)))
        K=1
        CALL func_ptr(x_res, func_data, f)
        is_small=chk_ptr(f, func_data)
        IF (is_small) THEN
            WRITE (U,*) K, f
            CLOSE (U)
        ENDIF
        CALL grad_ptr(x_res, func_data, g)

        g_norm=SQRT(DOT_PRODUCT(g,g))

        CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
        IF (g_norm <grad_prms%gtol) THEN
            CLOSE(U)
            RETURN
        ENDIF

        K=2
        g0=g
        x=x_res 

        CALL LINE_SEARCH(grad_prms, U, func_ptr, grad_ptr, chk_ptr, &
            func_data,  -g0, x, x_res, f, g )

        is_small=chk_ptr(f, func_data)
        IF (is_small) THEN
            WRITE (U,*) K, f
            CLOSE(U)
            RETURN
        ENDIF

        g_norm=SQRT(DOT_PRODUCT(g,g))
        CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
        IF (g_norm <grad_prms%gtol) THEN
            CLOSE(U)
            RETURN
        ENDIF

        K=3

        DO WHILE (g_norm>grad_prms%gtol)
            alpha=GET_ALPHA_FOR_BB(x, x_res, g0, g)
            step_l=ABS(alpha)*g_norm

            IF (step_l > grad_prms%max_step ) THEN
                alpha=grad_prms%max_step/g_norm
                step_l=ABS(alpha)*g_norm
            ENDIF
            m_dx=ABS(alpha)*MAXVAL(ABS(g))
            x=x_res
            x_res=x-alpha*g
            CALL OPT_SOLVER_LOG(U, A_msg, K, alpha, step_l, m_dx)
            f0=f
            CALL func_ptr(x_res, func_data, f)
            is_small=chk_ptr(f, func_data)
            IF (is_small) THEN
                WRITE (U,*) K, f
                EXIT
            ENDIF
            g0=g
            pg0=-DOT_PRODUCT(g0,g0)
            a_const=GET_ARMIJO_CONST(f0, f, pg0, alpha)
            IF (a_const<R_ZERO) THEN
                CALL OPT_SOLVER_LOG_MSG(U, "ARMIJO CONDITIONS ARE NOT SATISIFIED")
            ENDIF
            CALL grad_ptr(x_res, func_data, g)
            g_norm=SQRT(DOT_PRODUCT(g,g))
            CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
            CALL CHECK_WOLFE_CONDITIONS(grad_prms, U,  -g0, pg0, g, chk_w)
            K=K+1
        ENDDO
        CLOSE (U)
    ENDSUBROUTINE


    FUNCTION GET_ARMIJO_CONST(f0,f, pg, q) RESULT(RES)
        REAL(DP), INTENT(IN) :: f0, f, pg, q
        REAL(DP) :: RES
        RES=-(f0-f)/pg/q
    ENDFUNCTION

    FUNCTION GET_ALPHA_FOR_BB(x1, x2, g1, g2) RESULT(RES)
        REAL(DP), INTENT(IN) :: x1(:), x2(:)
        REAL(DP), INTENT(IN) :: g1(:), g2(:)
        REAL(DP) :: RES
        REAL(DP) :: s(SIZE(x1)), y(SIZE(g1))
        REAL(DP) :: a, b
        REAL(DP), PARAMETER :: TH=1e-30_DP
        s=x2-x1
        y=g2-g1
        a=DOT_PRODUCT(s,y)
        b=DOT_PRODUCT(y,y)
        IF (b > TH) THEN
            RES=a/b
        ELSEIF( a> TH) THEN
            b=DOT_PRODUCT(s,s)
            RES=b/a
        ELSE
            a=SQRT(DOT_PRODUCT(g2,g2))
            RES=R_ONE/a/100.0_DP
        ENDIF
    ENDFUNCTION

    SUBROUTINE GRADIENT_CG_METHOD(grad_prms,  func_ptr, grad_ptr, &
            chk_ptr, func_data,  x_res)
        TYPE(GRAD_METHOD_DATA), INTENT(IN) :: grad_prms
        PROCEDURE(CALC_TARGET_FUNC_TYPE), POINTER, INTENT(IN) :: func_ptr
        PROCEDURE(CALC_GRADIENT_TYPE), POINTER, INTENT(IN) :: grad_ptr
        PROCEDURE(CHECK_SMALL_FUNC_TYPE), POINTER, INTENT(IN) :: chk_ptr
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT):: x_res(:)
        REAL(DP), ALLOCATABLE :: g(:), g0(:),  x(:)
        REAL(DP), ALLOCATABLE :: p(:)
        REAL(DP) :: f, alpha
        REAL(DP) ::g_norm
        LOGICAL :: is_small
        INTEGER :: U
        INTEGER :: K

        U=OPEN_FOR_WRITE(grad_prms%log_file)
        ALLOCATE(g(SIZE(x_res)), x(SIZE(x_res)))
        ALLOCATE(p(SIZE(x_res)))
        K=1
        CALL func_ptr(x_res, func_data, f)
        is_small=chk_ptr(f, func_data)
        IF (is_small) THEN
            WRITE (U,*) K, f
            CLOSE (U)
            RETURN
        ENDIF
        CALL grad_ptr(x_res, func_data, g)

        g_norm=SQRT(DOT_PRODUCT(g,g))

        CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
        IF (g_norm <grad_prms%gtol) THEN
            CLOSE(U)
            RETURN
        ENDIF

        K=2
        g0=g
        p=-g0
        x=x_res
        CALL LINE_SEARCH(grad_prms, U, func_ptr, grad_ptr, chk_ptr, &
            func_data,  p, x, x_res, f, g )

        is_small=chk_ptr(f, func_data)
        IF (is_small) THEN
            WRITE (U,*) K, f
            CLOSE(U)
            RETURN
        ENDIF

        g_norm=SQRT(DOT_PRODUCT(g,g))
        CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
        IF (g_norm <grad_prms%gtol) THEN
            CLOSE(U)
            RETURN
        ENDIF

        K=3

        DO WHILE (g_norm>grad_prms%gtol)
            alpha=GET_BETA_FOR_CG_FR(g0, g)
            p=-g+alpha*p
            x=x_res
            
            g0=g
            CALL LINE_SEARCH(grad_prms, U, func_ptr, grad_ptr, chk_ptr, &
                func_data,  p, x, x_res, f, g )

            is_small=chk_ptr(f, func_data)
            IF (is_small)  EXIT
            g_norm=SQRT(DOT_PRODUCT(g,g))
            CALL OPT_SOLVER_LOG(U, M_msg, K, f, g_norm)
            K=K+1
        ENDDO

        CLOSE (U)
    ENDSUBROUTINE

    FUNCTION GET_BETA_FOR_CG(x1,x2) RESULT(RES)
        REAL(DP), INTENT(IN) :: x1(:), x2(:)
        REAL(DP) :: RES
        REAL(DP)::  a, b
        a=DOT_PRODUCT(x2,x2-x1)
        b=DOT_PRODUCT(x1,x1)
        RES=a/b
    ENDFUNCTION

    FUNCTION GET_BETA_FOR_CG_FR(x1, x2) RESULT(RES)
        REAL(DP), INTENT(IN) :: x1(:), x2(:)
        REAL(DP) :: RES
        REAL(DP)::  a, b
        a=DOT_PRODUCT(x2,x2)
        b=DOT_PRODUCT(x1,x1)
        RES=a/b
    ENDFUNCTION



    FUNCTION GET_BETA_FOR_CG_DY(x1, x2, s1) RESULT(RES)
        REAL(DP), INTENT(IN) :: x1(:), x2(:), s1(:)
        REAL(DP) :: RES
        REAL(DP)::  a, b
        a=DOT_PRODUCT(x2,x2)
        b=DOT_PRODUCT(s1,x2-x1)
        RES=a/b
    ENDFUNCTION

    SUBROUTINE OPT_SOLVER_LOG3(U, msg, K, f , alpha)
        INTEGER, INTENT(IN) :: U
        CHARACTER(len=*), INTENT(IN) :: msg
        INTEGER , INTENT(IN) :: K
        REAL(DP), INTENT(IN) :: f, alpha
        REAL(DP) :: t
        t=GetTime()
        WRITE (U, FMT_GRAD2)    t, " s: ",  msg,  K, f, alpha
        FLUSH(U)
    ENDSUBROUTINE

    SUBROUTINE OPT_SOLVER_LOG4(U, msg, K, f , alpha, f0)
        INTEGER, INTENT(IN) :: U
        CHARACTER(len=*), INTENT(IN) :: msg
        INTEGER , INTENT(IN) :: K
        REAL(DP), INTENT(IN) :: f, alpha
        REAL(DP), INTENT(IN) :: f0
        REAL(DP) :: t
        t=GetTime()
        WRITE (U, FMT_NAIVE_LS) t, " s: ",  msg,  K, f, alpha,  "|",  f0
        FLUSH(U)
    ENDSUBROUTINE

    SUBROUTINE OPT_SOLVER_LOG_MSG(U, msg)
        INTEGER, INTENT(IN) :: U
        CHARACTER(len=*), INTENT(IN) :: msg
        CALL LOGGER(msg, U)
    ENDSUBROUTINE

    SUBROUTINE LBFGS_2_METHOD(dlbfgs, p_save, ls_prms, cb, func_data,  x_res, conv, log_file)
        TYPE(LBFGS_DATA_TYPE), INTENT(INOUT) :: dlbfgs
        PROCEDURE(STORE_LBFGS_TO_DISK_SUB), POINTER, INTENT(IN) :: p_save
        TYPE(LS_PARAMS_TYPE), INTENT(IN):: ls_prms
        TYPE (CALLBACKS_FOR_OPT), INTENT(IN) :: cb
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT):: x_res(:)
        LOGICAL, INTENT(OUT) :: conv
        CHARACTER(len=*), INTENT(IN), OPTIONAL :: log_file

        REAL(DP), ALLOCATABLE :: g(:)
        REAL(DP), ALLOCATABLE :: p(:)
        REAL(DP) :: f
        REAL(DP) ::g_norm, x_norm
        INTEGER :: U
        INTEGER :: K, K_ln, K2
        LOGICAL :: print_output
        CHARACTER(len=1024) :: msg
        REAL(DP) :: alpha

        REAL(DP), PARAMETER :: TH_SAME=1.0E-10_DP

        conv=.TRUE.
        print_output = PRESENT(log_file)
        IF (print_output)  U=OPEN_FOR_WRITE(log_file)
        ALLOCATE(g(SIZE(x_res)))
        ALLOCATE(p(SIZE(x_res)))

        IF (dlbfgs%K>0) THEN
            p=dlbfgs%xk-x_res
            CALL dlbfgs%CALC_DP(dlbfgs%xk, dlbfgs%xk, x_norm, dlbfgs%A)
            x_norm=SQRT(x_norm)
            CALL dlbfgs%CALC_DP(p, p, g_norm, dlbfgs%A)
            g_norm=SQRT(g_norm)
            g_norm=g_norm/x_norm
            IF (g_norm > TH_SAME) THEN
                CALL FIRST_STEP(U, ls_prms, dlbfgs, cb, func_data,  p, &
                    x_res, f, g, K, K2, K_ln, conv)
            ELSE
                CALL PRINTF_LOG('SAME:', g_norm)
                K=dlbfgs%K+1
                K2=0
                f=dlbfgs%f
            ENDIF
        ELSE
            CALL FIRST_STEP(U, ls_prms, dlbfgs, cb, func_data,  p, x_res, f, g, K, K2, K_ln, conv)
            IF (conv) THEN
                IF (print_output) CLOSE(U)
                RETURN
            ELSEIF (K_ln <0) THEN
                CALL LOGGER('Line search failed for  antigradient direction', U)
                CALL LBFGS2_LS_FAIL(U, ls_prms%LS_max_it, K_ln)
                IF (print_output) CLOSE(U)
                RETURN
            ENDIF
            IF (ASSOCIATED(p_save)) THEN
                CALL p_save(dlbfgs)
            ENDIF
        ENDIF

        DO  
            CALL PLAY_WITH_SEARCH_DIRECTIONS(U, ls_prms, dlbfgs, cb, func_data, &
                p, x_res, f, g, K_ln, alpha)

            IF (K_ln <0) THEN
                CALL LBFGS2_LS_FAIL(U, ls_prms%LS_max_it, K_ln)
                IF (print_output) CLOSE(U)
                conv=.FALSE.
                RETURN
            ENDIF
            dlbfgs%alpha0=R_ONE
            K2=K2+K_ln
            K=K+1

            IF (ASSOCIATED(cb%msg_ptr)) THEN
                CALL cb%msg_ptr(func_data, msg)
            ELSE 
                msg=''
            ENDIF

            IF ( cb%chk_ptr(f, func_data)) EXIT

            CALL UPDATE_LBFGS_MEM(dlbfgs, x_res, g, f)
            CALL LBFGS2_LOGGER(U, K, K2, K_ln,  f, dlbfgs%gk_nrm, dlbfgs%theta, alpha, msg)
            IF (dlbfgs%gk_nrm <=ls_prms%gtol) EXIT

            IF (ASSOCIATED(p_save)) THEN
                CALL p_save(dlbfgs)
            ENDIF
            IF (ASSOCIATED(cb%upd_ptr)) THEN
                CALL cb%upd_ptr(func_data, .TRUE.)
            ENDIF
        ENDDO

        IF (print_output) CLOSE(U)
    ENDSUBROUTINE

    SUBROUTINE PLAY_WITH_SEARCH_DIRECTIONS(U, ls_prms, dlbfgs, cb, func_data,  p, x_res, f, g, K_ln, alpha)
        INTEGER, INTENT(IN) :: U
        TYPE(LS_PARAMS_TYPE), INTENT(IN) :: ls_prms
        TYPE(LBFGS_DATA_TYPE), INTENT(INOUT) :: dlbfgs
        TYPE (CALLBACKS_FOR_OPT), INTENT(IN) :: cb
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT) :: p(:)
        REAL(DP), INTENT(INOUT) :: f
        REAL(DP), INTENT(INOUT) :: x_res(:), g(:)
        REAL(DP), INTENT(INOUT) :: alpha
        INTEGER, INTENT(OUT) :: K_ln
        INTEGER :: I_dir, K_ln0

        K_ln=0
        DO I_dir=1, 3
            SELECT CASE (I_dir)
                CASE(1)
                    CALL CALC_SEARCH_DIRECTION(dlbfgs, p, .FALSE.)
                CASE(2)
                    IF (.NOT. ASSOCIATED(dlbfgs%Mult_H) ) CYCLE
                    CALL CALC_SEARCH_DIRECTION(dlbfgs, p, .TRUE.)
                CASE(3)
                    p=-dlbfgs%gk
            ENDSELECT
            CALL WEAK_WOLFE_LS_LBFGS(ls_prms, dlbfgs, cb, &
                func_data,  x_res, f, g, K_ln0, alpha )
            K_ln=K_ln+ABS(K_ln0)
            IF (K_ln0 >0) THEN
                RETURN
            ELSE
                SELECT CASE (I_dir)
                    CASE(1)
                        CALL LOGGER('Line search failed for "SMART" initial Hessian', U)
                    CASE(2)
                        CALL LOGGER('Line search failed for "CLASICAL" initial Hessian', U)
                    CASE(3)
                        CALL LOGGER('Line search failed for  antigradient direction', U)
                ENDSELECT
            ENDIF
        ENDDO
    ENDSUBROUTINE


    SUBROUTINE FIRST_STEP(U, ls_prms, dlbfgs, cb, func_data,  p, x_res, f, g,&
            K1, K2, K_ln, conv)
        INTEGER, INTENT(IN) :: U
        TYPE(LS_PARAMS_TYPE), INTENT(IN) :: ls_prms
        TYPE(LBFGS_DATA_TYPE), INTENT(INOUT) :: dlbfgs
        TYPE (CALLBACKS_FOR_OPT), INTENT(IN) :: cb
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT) :: p(:)
        REAL(DP), INTENT(INOUT) :: f
        REAL(DP), INTENT(INOUT) :: x_res(:), g(:)
        INTEGER, INTENT(OUT) :: K1, K2, K_ln
        LOGICAL, INTENT(OUT) :: conv
        REAL(DP) :: g_norm, alpha
        CHARACTER(len=1024) :: msg
        conv=.TRUE.
        K_ln=0
        K1=dlbfgs%K+1
        K2=1
        CALL cb%func_ptr(x_res, func_data, f)
        
        IF (cb%chk_ptr(f, func_data))   RETURN

        CALL cb%grad_ptr(x_res, func_data, g)
        CALL dlbfgs%CALC_DP(g, g,  g_norm, dlbfgs%A)
        g_norm=SQRT(g_norm)

        IF (ASSOCIATED(cb%msg_ptr)) THEN
            CALL cb%msg_ptr(func_data, msg)
        ELSE 
            msg=''
        ENDIF
        IF (g_norm <ls_prms%gtol) RETURN
        alpha=R_ZERO
        CALL LBFGS2_LOGGER(U, K1, K2, 0,  f, g_norm, R_ZERO, alpha, msg)
        IF (ASSOCIATED(cb%upd_ptr)) THEN
            CALL cb%upd_ptr(func_data, .TRUE.)
        ENDIF

        IF (K1==1) THEN
            dlbfgs%gk=g
            dlbfgs%xk=x_res
            dlbfgs%gk_nrm=g_norm
            CALL CALC_SEARCH_DIRECTION(dlbfgs, p, .FALSE.)
            CALL WEAK_WOLFE_LS_LBFGS(ls_prms, dlbfgs, cb, &
                func_data,  x_res, f, g, K_ln, alpha )
        ELSE
            CALL UPDATE_LBFGS_MEM(dlbfgs, x_res, g, f)
            CALL PLAY_WITH_SEARCH_DIRECTIONS(U, ls_prms, dlbfgs, cb, &
                func_data,  p, x_res, f, g, K_ln, alpha)
        ENDIF

        IF (K_ln <0 ) THEN
            conv=.FALSE.
            RETURN
        ELSEIF (cb%chk_ptr(f, func_data)) THEN
            RETURN
        ENDIF

        CALL UPDATE_LBFGS_MEM(dlbfgs, x_res, g, f)
        IF (dlbfgs%gk_nrm <ls_prms%gtol)  RETURN
        conv=.FALSE.

        IF (ASSOCIATED(cb%upd_ptr)) THEN
            CALL cb%upd_ptr(func_data, .TRUE.)
        ENDIF

        IF (ASSOCIATED(cb%msg_ptr)) THEN
            CALL cb%msg_ptr(func_data, msg)
        ELSE 
            msg=''
        ENDIF
        K1=K1+1 
        K2=K2+K_ln
        CALL LBFGS2_LOGGER(U, K1, K2, K_ln,  f, dlbfgs%gk_nrm, dlbfgs%theta ,alpha, msg)
    ENDSUBROUTINE

    SUBROUTINE LBFGS2_LOGGER(U, K, K2,  Kl,  f, gnorm, theta, alpha, msg0)
        INTEGER, INTENT(IN) :: U
        INTEGER , INTENT(IN) :: K, K2, Kl
        REAL(DP), INTENT(IN) :: f, gnorm, theta, alpha
        CHARACTER(len=*) :: msg0
        CHARACTER(len=512) :: msg
        CHARACTER (len=*), PARAMETER :: FMT_LBFGS2='(A, I5, I5, I5, A, G15.8,  G15.8, G15.8, G15.8, A,  A)'
        WRITE (msg, FMT_LBFGS2)   LBFGS_msg,  K, K2,   Kl, '|',  alpha, theta,  gnorm, f, '|',  TRIM(msg0)
        CALL LOGGER(msg, U)
    ENDSUBROUTINE

    SUBROUTINE LBFGS2_LS_FAIL(U, K_max, Kl)
        INTEGER, INTENT(IN) :: U
        INTEGER , INTENT(IN) :: K_max, Kl
        INTEGER :: Ndir
        CHARACTER(len=512) :: msg
        Ndir=ABS(Kl/K_max)
        WRITE (msg, '(A, I5.5, A)' ) 'Line search for ', Ndir,  ' directions failed' 
        CALL LOGGER(msg, U)
        WRITE (msg, '(A, I5)' ) 'Max number of steps per line search: ', K_max 
        CALL LOGGER(msg, U)
    ENDSUBROUTINE

    SUBROUTINE WEAK_WOLFE_LS_LBFGS(ls_prms, dlbfgs, cb, func_data,  &
              x_res, f0, g, K_ln, alpha)
        TYPE(LS_PARAMS_TYPE), INTENT(IN) :: ls_prms
        TYPE(LBFGS_DATA_TYPE), INTENT(INOUT) :: dlbfgs
        TYPE (CALLBACKS_FOR_OPT), INTENT(IN) :: cb
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(INOUT) :: f0
        REAL(DP), INTENT(OUT) :: x_res(:)
        REAL(DP), INTENT(OUT) ::  g(:)
        INTEGER, INTENT(OUT) :: K_ln
        REAL(DP), INTENT(OUT) :: alpha
        REAL(DP) :: pg, f,  pg2, p_nrm
        REAL(DP) :: df_lim
        REAL(DP) :: t_min, t_max, tau2
        LOGICAL :: not_wolfe2, not_wolfe1

        CALL CORRECT_LS_DIRECTION(ls_prms, dlbfgs, cb, func_data,  &
            f0, alpha, g)

        p_nrm=dlbfgs%p_nrm
        pg=dlbfgs%pg


        t_min=R_ZERO
        t_max=HUGE(R_ONE) 
        

        K_ln=0
        not_wolfe2=.TRUE.
        WOLFE_LOOP: DO WHILE ( not_wolfe2 )
            not_wolfe1=.TRUE. 
            ARMIJO_LOOP: DO WHILE (not_wolfe1) 

                IF (K_ln>ls_prms%LS_max_it) THEN
                    K_ln=-K_ln
                    RETURN
                ENDIF

                x_res=dlbfgs%xk+alpha*dlbfgs%p
                df_lim=ls_prms%tau*alpha*pg

                CALL cb%func_ptr(x_res, func_data, f)
                K_ln=K_ln+1
                IF (cb%chk_ptr(f, func_data)) THEN
                    f0=f
                    RETURN
                ENDIF
                IF (f <= f0+ df_lim ) THEN
                    not_wolfe1=.FALSE.
                ELSE
                    t_max=alpha
                    alpha=(t_max+t_min)/R_TWO
                    CALL PRINTF_LOG("FIRST WOLFE  CONDITION IS NOT SATISIFIED. New step size:", alpha)
                ENDIF
            ENDDO ARMIJO_LOOP 

            CALL cb%grad_ptr(x_res, func_data, g)
            CALL dlbfgs%CALC_DP(dlbfgs%p, g, pg2, dlbfgs%A)
           
            tau2=pg2/pg
            not_wolfe2=tau2 > R_ONE
            IF (ABS(tau2) < R_ONE) THEN
                CALL PRINTF_LOG("STRONG SECOND WOLFE  CONDITION WITH TAU ", ABS(tau2))
            ELSEIF(tau2 < R_ONE) THEN
                CALL PRINTF_LOG("WEAK SECOND WOLFE  CONDITION WITH TAU ", tau2)
            ELSE
                t_min=alpha 
                IF (t_max<HUGE(R_ONE)-1) THEN
                    alpha=(t_min+t_max)/R_TWO
                ELSE
                    alpha=R_TWO*t_min
                ENDIF
                CALL PRINTF_LOG("SECOND WOLFE  CONDITION IS NOT SATISIFIED. New step size:", alpha)
            ENDIF
        ENDDO WOLFE_LOOP
        f0=f
    ENDSUBROUTINE

    SUBROUTINE CORRECT_LS_DIRECTION(ls_prms, dlbfgs, cb, func_data,  &
              f0, alpha, work)
        TYPE(LS_PARAMS_TYPE), INTENT(IN) :: ls_prms
        TYPE(LBFGS_DATA_TYPE), INTENT(INOUT) :: dlbfgs
        TYPE (CALLBACKS_FOR_OPT), INTENT(IN) :: cb
        CLASS(*), POINTER, INTENT(IN) :: func_data
        REAL(DP), INTENT(IN) :: f0
        REAL(DP), INTENT(OUT) :: alpha
        REAL(DP), INTENT(INOUT) :: work(:)
        REAL(DP) :: pg, p_nrm
        REAL(DP) :: alpha0
        LOGICAL ::  same
        REAL(DP), PARAMETER :: ALPHA_SM=1e-2_DP
        REAL(DP), PARAMETER :: SMOOTH_SM=0.9_DP

        REAL(DP) :: qg, qp, w

        p_nrm=dlbfgs%p_nrm
        pg=dlbfgs%pg


        IF (ls_prms%alpha_sc>R_ZERO) THEN
            alpha=ls_prms%alpha_sc*ABS((R_ONE-ls_prms%tau)*ABS(f0)/pg)
        ELSEIF (dlbfgs%K==0) THEN
            alpha=ABS(ls_prms%alpha_sc)*ABS((R_ONE-ls_prms%tau)*ABS(f0)/pg)
        ELSE
            alpha=dlbfgs%alpha0 
        ENDIF

#ifdef naive
        IF (ASSOCIATED(cb%s_dir_corr_ptr)) THEN
            same=.FALSE.
            alpha0=alpha
            DO WHILE ( .NOT. same .AND. alpha > ALPHA_SM)
                work=dlbfgs%p*alpha
                CALL cb%s_dir_corr_ptr(dlbfgs%xk, func_data, work, same)
                alpha=alpha*SMOOTH_SM
            ENDDO
            IF (.NOT. same ) THEN
                work=dlbfgs%p
                CALL cb%s_dir_corr_ptr(dlbfgs%xk, func_data, work, same)
                CALL dlbfgs%CALC_DP(dlbfgs%p, work, pg2, dlbfgs%A)
                alpha=pg2/p_nrm/p_nrm
                alpha=MIN(alpha0, alpha)
                alpha=MAX(alpha, ALPHA_SM)
            ELSE
                alpha=alpha/SMOOTH_SM
                CALL LOGGER('Naive LS success')
            ENDIF
        ENDIF
#else
        IF (ASSOCIATED(cb%s_dir_corr_ptr)) THEN
            same=.FALSE.
            alpha0=alpha
            DO
                work=dlbfgs%p*alpha

                CALL cb%s_dir_corr_ptr(dlbfgs%xk, func_data, work, same)

                CALL dlbfgs%CALC_DP(dlbfgs%p, work, qp, dlbfgs%A)
                CALL dlbfgs%CALC_DP(dlbfgs%gk, work, qg, dlbfgs%A)

                w=qp/p_nrm/p_nrm
                IF (qg < R_ZERO .AND. w > ALPHA_SM ) THEN
                    CALL CALC_DP_PG(dlbfgs,  work)
                    dlbfgs%p=work
                    p_nrm=dlbfgs%p_nrm
                    pg=dlbfgs%pg
                    alpha=qp/p_nrm/p_nrm
                    alpha=MIN(alpha, R_ONE)
                    CALL PRINTF_LOG ('LS # Modified direction', w)
                    EXIT
                ELSEIF (alpha < ALPHA_SM) THEN
                    work=dlbfgs%p
                    CALL cb%s_dir_corr_ptr(dlbfgs%xk, func_data, work, same)
                    CALL dlbfgs%CALC_DP(dlbfgs%p, work, qp, dlbfgs%A)
                    alpha=qp/p_nrm/p_nrm
                    alpha=MIN(alpha0, alpha)
                    alpha=MAX(alpha, ALPHA_SM)
                    CALL PRINTF_LOG ('LS # Fall back to original direction', w)
                    EXIT
                ENDIF
                alpha=alpha*SMOOTH_SM
            ENDDO
        ENDIF
#endif

    ENDSUBROUTINE
ENDMODULE
