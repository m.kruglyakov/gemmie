#!/bin/sh


rm $1

calc_resp=../../../../../calc_responses

input_files=$( ls MT* )
$calc_resp --add-sites-by-coordinates --output $1 --input $input_files $2
$calc_resp  --extract-by-number $1 $3 tmp_$1

mv tmp_$1 $1
